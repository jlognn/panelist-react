module.exports = {
  purge: [
    './src/**/*.{js,jsx,ts,tsx}',
    './public/index.html',
    './node_modules/@panelist/**/*.{js,jsx,ts,tsx}'
  ],
  darkMode: false, // or 'media' or 'class'
    theme: {
      extend: {
        backgroundColor: {
          'gray-0': '#f9f8fa',
          'gray-1': '#edf2f5',
          'gray-2': '#f6f4ed',
          'gray-3': '#f7f7f4',
          'gray-4': '#707070',
          'gray-5': '#424141',
          'gray-6': '#f8f8f8',
          'gray-7': '#ebebeb',
          white: '#fff',
          'white-1': '#FFF8F8',
          'blue-0': '#edf2f5',
          'blue-1': '#285CB2',
          'blue-3': '#0f4cb0',
          'blue-4': '#4477CC',
          blue: '#203c6e',
          'blue-gray-1': '#7283a4',
          'blue-gray-2': '#244E93',
          'dark-blue': "#1D2A51",
          'orange': '#ffae39',
          'orange-2': '#ff9700',
        },
        borderColor: {
          'gray-1': '#edf2f5',
          'blue-0': '#cddcf2'
        },
        color: {
          'blue-x': '#cddcf2'
        },
        fontFamily: {
          sans: ['"Poppins"']
        },
        fontSize: {
          '2xl': '30px',
          '3xl': ['80px', '5px'],
          xl: ['22px', '24px'],
          xl1: ['40px', '24px'],
          xl2: ['35px', '45px'],
          xl3: ['30px', '40px'],
          xl4: ['36px', '40px'],
          xl5: ['36px', '55px'],
          xl6: ['30px', '46px'],
          xl7: ['30px', '15px'],
          xl7a: ['29px', '43px'],
          xl8: ['28px', '15px'],
          lg4: ['21px', '25px'],
          lg3: ['21px', '31px'],
          lg2: ['20px', '30px'],
          lg1: ['30px', '25px'],
          lg0: ['22px', '24px'],
          lg: ['20px', '25px'],
          md: ['16px', '25px'],
          md0: ['15px', '20px'],
          md1: ['16px', '20px'],
          md2: ['15px', '23px'],
          sm: '13px',
          sm1: ['13px', '20px'],
          sm2: ['13px', '21px'],
          sm3: ['18px', '27px'],
          sm3a: ['18px', '33px'],
          sm4: ['13px', '17px'],
          sm5: ['12px', '17px'],
          'x-sm': '11px',
          '2x-sm': '9px',
          '3x-sm': '5px',
          navlink: '14px',
          h1: '24px',
          h2: '14px',
          h3: '12px',
          md15: '15px',
          lg18: '18px',
          's-10': '10px'
        },
        textColor: {
          'gray-0': '#3e3e3e',
          'gray-1': '#3c3c3c',
          'gray-2': '#E3E3E3',
          'gray-3': '#707070',
          'gray-4': '#2B2B2B',
          'gray-5a': '#18161c',
          'gray-5': '#393939',
          'gray-6': '#797979',
          'orange-1': '#ffcb53',
          'orange-2': '#ffae39',
          'orange-3': '#ff8c27',
          'yellow-0': '#fffdcf',
          'yellow-1': '#fff2a0',
          'blue-0': '#cddcf2',
          'blue-1': '#285cb2',
          'blue-2': '#203c6e',
          'blue-3': '#172746',
          'blue-4': '#0b1221',
          'blue-5': '#2680EB',
          'blue-6': '#3163b5',
          'blue-6a': '#353a3d',
          'blue-7': '#3c4b6c',
          'blue-gray-1': '#7283a4',
          'dark-blue': '#1D2A51',
          'gray-white': '#edf2f5',
          white: '#fff',
          black: '#000',
          red: '#ff1a1a',
          happening: '#B22828'
        },
        borderColor: {
          'blue-0': '#cddcf2',
          'blue-1': '#285cb2',
          'blue-2': '#203c6e',
          'blue-3': '#172746',
          'blue-4': '#1D2A51',
          'orange-2': '#ffae39'
        },
        borderRadius: {
          5: '0.313rem',
          '2.5xl': '1.25rem'
        },
        borderWidth: {
          DEFAULT: '1px',
          0: '0',
          1: '1px',
          2: '2px'
        },
        width: {
          1124: '70.25rem',
          1076: '67.25rem',
          1069: '66.813rem',
          793: '49.563rem',
          630: '39.375rem',
          570: '35.625rem',
          539: '33.713rem',
          535: '33.438rem',
          576: '36rem',
          520: '32.5rem',
          527: '32.938rem',
          483: '30.188rem',
          464: '29rem',
          463: '28.938rem',
          447: '27.938rem',
          438: '27.375',
          420: '26.25rem',
          350: '21.938rem',
          315: '19.688rem',
          283: '17.688rem',
          288: '18rem',
          275: '17.188rem',
          250: '15.375rem',
          242: '15.125rem',
          240: '15rem',
          208: '13rem',
          177: '11.063rem',
          168: '10.5rem',
          169: '10.563rem',
          168: '10.5rem',
          136: '8.5rem',
          130: '8.125rem',
          118: '2.375rem',
          55: '3.438rem',
          38.6: '2.413rem',
          28.6: '1.788rem',
          27: '1.688rem',
          19: '1.188rem',
          15: '0.938rem',
          '6px': '0.375rem'
        },
        minHeight: {
          'full-xl': '1200px',
          371: '23.188rem',
          1182: '73.875rem',
          1630: '106.875rem',
            80: '5rem',

        },
        height: {
          1550: '96.875rem',
          710: '44.375rem',
          575: '35.938rem',
          550: '34.375rem',
          548: '34.25rem',
          506: '31.63rem',
          464: '29rem',
          327: '20.438rem',
          295: '18.438rem',
          284: '17.75rem',
          276: '17.25rem',
          266: '15.625rem',
          238: '14.875rem',
          220: '13.763rem',
          211: '13.188rem',
          210: '13.125rem',
          207: '12.938rem',
          200: '12.556rem',
          193: '12.375rem',
          178: '11.125rem',
          143: '8.938rem',
          138: '8.625rem',
          130: '8.125rem',
          126: '7.875rem',
          123: '7.688rem',
          92: '5.75rem',
          86: '5.375rem',
          80: '5rem',
          69: '4.313rem',
          57: '3.563rem',
          55: '3.438rem',
          51: '3.188rem',
          43: '2.688rem',
          38: '2.375',
          35: '2.188rem',
          26: '1.625rem',
          30: '1.875rem', 
          '3px': '0.188rem'
        },
        margin: {
          101: '28.6em',
          76: '18.8rem',
          100: '6.25rem',
          53: '3.313rem', 
          '-18': '-4.8rem',
          '-432': '-27rem',
          '-419': '-26.2rem',
          '-392': '-24.5rem',
          '-288': '-18rem'
        },
        spacing: {
          138: '8.625rem',
          15: '0.938rem'
        },
        screens: {
          xs: '500px'
        },
        borderRadius: {
          5: '0.313rem'
        },
        padding: {
          5: '0.313rem',
          13: '0.813rem',
          43: '2.688rem'
        },
      },
    },
    variants: {
      extend: {
        backgroundColor: ['active'],
      }
    },
    plugins: [],
};
