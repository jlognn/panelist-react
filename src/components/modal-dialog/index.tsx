import { Modal } from '@panelist/panelist-components-beta';

export interface IModalDialogProps {
  children: React.ReactNode;
  openModalButton?: string;
  buttonImageUrl?: string;
  openModalText?: string;
  modalWidth?: string;
  modalHeight?: string;
  overlayColor?: string;
  primaryButton?: string;
  secondaryButton?: string;
  primaryButtonText?: string;
  secondaryButtonText?: string;
  modalImage?: string;
  classes?: string;
}

export const ModalDialog = (props: IModalDialogProps) => {
  return (
    <div className={`z-50 absolute ${props.classes}`}>
      <Modal
        children={props.children}
        OpenModalbutton={props.openModalButton}
        buttonImageUrl={props.buttonImageUrl}
        openModalText={props.openModalText}
        modalWidth={props.modalWidth}
        modalHeight={props.modalHeight}
        overlayColor={'rgba(70, 70, 70, 0.5)'}
        primaryButton={props.primaryButton}
        SecondaryButton={props.secondaryButton}
        primaryButtonText={props.primaryButtonText}
        SecondaryButtonText={props.secondaryButtonText}
        modalImage={props.modalImage}
      />
    </div>
  );
};

export default ModalDialog;
