import { HyperLink } from '@panelist/panelist-components-beta';

export const Footer = () => (
  <>
    <footer
      className="z-50 inset-x-0 bottom-0 p-4 text-center "
      style={{ backgroundColor: '#edf2f5' }}
    >
      <div className="max-w-5xl 2xl:max-w-4xl mx-auto px-4 sm:px-6 lg:px-12 2xl:px-8">
        <div className="flex items-center justify-between h-1">
          <div className="flex items-center">
            <div className="flex-shrink-0">
              <strong>
                <HyperLink
                  classes="font-semibold"
                  variant="FooterLink"
                  text="Panelist &copy; 2021"
                />
              </strong>
            </div>
          </div>

          <HyperLink classes="font-medium" variant="FooterLink" text="User Agreement" />
          <HyperLink classes="font-medium" variant="FooterLink" text="Privacy Policy" />
          <HyperLink classes="font-medium" variant="FooterLink" text="Cookie Policy" />
          <HyperLink classes="font-medium" variant="FooterLink" text="Copyright Policy" />
          <HyperLink classes="font-medium" variant="FooterLink" text="Coommunity Guidelines" />
          <HyperLink classes="font-medium" variant="FooterLink" text="About Us" />
          <HyperLink classes="font-medium" variant="FooterLink" text="Help" />
        </div>
      </div>
    </footer>
  </>
);

export default Footer;
