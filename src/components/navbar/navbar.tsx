import { LinkButton, HyperLink } from '@panelist/panelist-components-beta';
import logo from '../../assets/images/homepage/logo-panelist.svg';

export const Navbar = () => (
  <>
    <nav className="bg-white z-40 relative shadow-xl">
      <div className="container mx-auto px-4 sm:px-6 lg:px-8 w-10/12">
        <div className="flex items-center justify-between h-51">
          <div className="flex items-center">
            <div className="flex-shrink-0">
              <img src={logo} alt="Panelist" />
            </div>
          </div>
          <div className="flex">
            <HyperLink variant="NavLink" text="Log in" href="/login" className="sans" />
            <LinkButton
              text="Join Now"
              backgroundColor=""
              textColor="#0b1221"
              classes="bg-orange ml-5 text-sm2 font-extralight hover:bg-orange-2 ml-6 leading-6"
              width="88px"
              height="30px"
              href="/join"
            />
          </div>
        </div>
      </div>
    </nav>
  </>
);

export default Navbar;
