import React, { useEffect, useRef, useState } from 'react';

import { Link } from 'react-router-dom';

import { Transition } from '@tailwindui/react';

import { ILoggedInUser } from '../../../models/loggedin-user';

const UserMenu = (user: ILoggedInUser) => {
  const [show, setShow] = useState(false);
  const container = useRef<any>(null);

  useEffect(() => {
    const handleOutsideClick = (event: MouseEvent) => {
      if (!container.current.contains(event.target)) {
        if (!show) return;
        setShow(false);
      }
    };

    window.addEventListener('click', handleOutsideClick);
    return () => window.removeEventListener('click', handleOutsideClick);
  }, [show, container]);

  useEffect(() => {
    const handleEscape = (event: KeyboardEvent) => {
      if (!show) return;

      if (event.key === 'Escape') {
        setShow(false);
      }
    };

    document.addEventListener('keyup', handleEscape);
    return () => document.removeEventListener('keyup', handleEscape);
  }, [show]);

  return (
    <div ref={container} className="relative">
      <button
        className="menu focus:outline-none focus:shadow-solid "
        onClick={() => setShow(!show)}
      >
        <img className="" src={user.avatarImage} alt={user.name} />
      </button>

      <Transition
        show={show}
        enter="transition ease-out duration-100 transform"
        enterFrom="opacity-0 scale-95"
        enterTo="opacity-100 scale-100"
        leave="transition ease-in duration-75 transform"
        leaveFrom="opacity-100 scale-100"
        leaveTo="opacity-0 scale-95"
      >
        <div className="origin-top-right absolute right-0 w-48 py-2 mt-1 bg-white rounded shadow-md">
          <Link to="/profile">
            <div className="block px-4 py-2 hover:bg-blue-1 hover:text-white">Profile</div>
          </Link>
          <Link to="/logout">
            <div className="block px-4 py-2 hover:bg-blue-1 hover:text-white">Logout</div>
          </Link>
        </div>
      </Transition>
    </div>
  );
};

export default UserMenu;
