import logo from '../../../assets/images/panelist-logo-round.svg';

export const Searchbox = () => (
  <div className="flex flex-row">
    <div className="flex-shrink-0 z-20">
      <img src={logo} alt="Panelist" />
    </div>
    <div className="bg-gray-1 px-2 py-1 rounded-2xl w-250 z-10 -ml-8" style={{ height: '38px' }}>
      <input
        type="text"
        placeholder="Search"
        className="px-8 py-1 bg-gray-1 text-md"
        style={{ height: '30px' }}
      />
    </div>
  </div>
);

export default Searchbox;
