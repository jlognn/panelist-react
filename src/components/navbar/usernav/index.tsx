import Midbar from './mid-bar';
import RightNavbar from './right-navbar';
import Searchbox from './searchbox';

export const UserNavbar = () => (
  <nav className="bg-white shadow-md">
    <div className="container mx-auto px-4 sm:px-6 lg:px-8">
      <div className="flex justify-between">
        <div className="flex space-x-7">
          {/* <!-- Website Logo --> */}
          <div className="flex flex-row px-2 py-2">
            <div className="mt-1">
              <Searchbox />
            </div>
            <div className="ml-4">
              <Midbar />
            </div>
            <div className="ml-4">
              <RightNavbar />
            </div>
          </div>
          {/* <!-- Primary Navbar items --> */}
          <div className="hidden md:flex items-center space-x-1">
            {/* <a
                    href=""
                    className="py-4 px-2 text-green-500 border-b-4 border-green-500 font-semibold "
                >Home</a
                >
                <a
                    href=""
                    className="py-4 px-2 text-gray-500 font-semibold hover:text-green-500 transition duration-300"
                >Services</a
                >
                <a
                    href=""
                    className="py-4 px-2 text-gray-500 font-semibold hover:text-green-500 transition duration-300"
                >About</a
                >
                <a
                    href=""
                    className="py-4 px-2 text-gray-500 font-semibold hover:text-green-500 transition duration-300"
                >Contact Us</a
                > */}
          </div>
        </div>
      </div>
    </div>
  </nav>
);

export default UserNavbar;
