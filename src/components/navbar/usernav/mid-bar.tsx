import home from '../../../assets/images/home.svg';
import lounge from '../../../assets/images/lounge.svg';
import techshow from '../../../assets/images/techshow.svg';
import events from '../../../assets/images/events.svg';
import library from '../../../assets/images/library.svg';

export const Midbar = () => (
  <div className="bg-gray-1 px-2 rounded-2xl w-527 z-10">
    <div className="flex flex-row justify-between">
      <div className="px-4 py-0.5 mr-8 mt-0.5">
        <a href="/home">
          <img src={home} alt="Home" />
        </a>
      </div>
      <div className="px-4 py-0.5  mr-8  mt-0.5">
        <a href="/lounge">
          <img src={lounge} alt="Lounge" />
        </a>
      </div>
      <div className="px-4 py-0.5  mr-8  mt-0.5">
        <a href="/tech-show">
          <img src={techshow} alt="Tech Show" />
        </a>
      </div>
      <div className="px-4 py-0.5  mr-8  mt-0.5">
        <a href="/events">
          <img src={events} alt="Events" />
        </a>
      </div>
      <div className="px-4 py-0.5 mt-0.5">
        <a href="/library">
          <img src={library} alt="Library" />
        </a>
      </div>
    </div>
  </div>
);

export default Midbar;
