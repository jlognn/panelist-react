import avatar from '../../../assets/images/avatar-dropdown.svg';
import chat from '../../../assets/images/chat.svg';
import create from '../../../assets/images/create.svg';
import network from '../../../assets/images/Network.svg';
import notifications from '../../../assets/images/notifications.svg';
import UserMenu from './user-menu';

export const RightNavbar = () => {
  const user = {
    avatarImage: avatar,
    name: 'Fahad'
  };
  return (
    <div className="bg-gray-1 px-2 py-0.5 rounded-2xl w-295 z-10">
      <div className="flex flex-row justify-between">
        <div className="px-4 py-0.5 mt-0.5">
          <a href="/network">
            <img src={network} alt="Network" />
          </a>
        </div>
        <div className="px-4 py-0.5 mt-0.5">
          <a href="/chat">
            <img src={chat} alt="Chat" />
          </a>
        </div>
        <div className="px-4 py-0.5  mt-0.5">
          <a href="/notifications">
            <img src={notifications} alt="Notifications" />
          </a>
        </div>
        <div className=" px-4 py-0.5 mt-0.5">
          <UserMenu avatarImage={user.avatarImage} name={user.name} />
        </div>
        <div className="px-4 py-0.5 mt-0.5">
          <a href="/create">
            <img src={create} alt="Create" />
          </a>
        </div>
      </div>
    </div>
  );
};

export default RightNavbar;
