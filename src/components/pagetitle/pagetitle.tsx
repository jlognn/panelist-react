import { Typography } from '@panelist/panelist-components-beta';

interface IPageTitleProps {
  titleText: string;
}

export const PageTitle = (props: IPageTitleProps) => (
  <div className="flex justify-center mr-12">
    <div className="mt-5">
      <Typography variant="SectionTitle" text={props.titleText} />
    </div>
  </div>
);

export default PageTitle;
