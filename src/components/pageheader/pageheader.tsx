import { Typography } from '@panelist/panelist-components-beta';

import logo from '../../assets/images/logo.png';

export const PageHeader = () => (
  <>
    <div className="flex items-start justify-start mt-72">
      <img src={logo} alt="Panelist" />
    </div>
    <div className="flex justify-start ml-5">
      <Typography variant="SubTitle" text="Connecting professionals around the world" />
    </div>
  </>
);

export default PageHeader;
