import { format } from 'date-fns';
import differenceInMonths from 'date-fns/differenceInMonths';
import { createBrowserHistory } from 'history';

const history = createBrowserHistory();

export const emailRegex = /.+@.+\..+/;

export const routeTo = (path: string) => {
  history.push({
    pathname: path
  });

  history.go(0);
};

export const calculatePeriod = (fromDate: string, toDate: string) => {
  if (fromDate) {
    const from = new Date(fromDate);
    const fromFormatted = format(from, 'MMM yyyy');
    let to = new Date(Date.now());
    let toFormatted = '';
    let difference = 0;

    if (toDate === null) toFormatted = 'Present';
    else {
      to = new Date(toDate.split('T')[0]);
      toFormatted = format(to, 'MMM yyyy');
    }

    difference = differenceInMonths(to, from);
    difference++;

    let years = 0;
    if (difference >= 12) years = difference / 12;

    return `${fromFormatted} - ${toFormatted} ・ ${
      difference < 12 ? difference + (difference <= 1 ? ' month' : ' months') : years + ' years'
    }`;
  }
};
