interface IDefaultData {
  title: string;
  placeHolder: string;
  data: string | string[];
  open: boolean;
  keyEx?: string;
  key: string;
  dataKey?: string;
  type: number;
  name?: string;
  selectedItem?: any[];
}

interface IData {
  library: IDefaultData[];
}

export const defaultDataMapping: IData = {
  library: [
    {
      title: 'Format',
      placeHolder: 'Add a Format',
      data: [],
      open: false,
      keyEx: 'where[excludeFormat]',
      key: 'where[format]',
      dataKey: 'format',
      type: 3 // selecting
    },
    {
      title: 'Company',
      placeHolder: 'Add a company',
      data: '',
      open: false,
      keyEx: 'where[excludeCompanyId]',
      key: 'where[companyId]',
      dataKey: 'company',

      type: 2 // selecting
    },
    {
      title: 'Industry',
      placeHolder: 'Add an industry',
      data: '',
      open: false,
      keyEx: 'where[excludeIndustryId]',
      key: 'where[industryId]',
      dataKey: 'industry',

      type: 2 // selecting
    },
    {
      title: 'Job Function',
      placeHolder: 'Add a function',
      data: '',
      open: false,
      keyEx: 'where[excludeJobFunctionId]',
      key: 'where[jobFunctionId]',
      dataKey: 'jobfunction',
      type: 2 // typing
    },
    {
      title: 'Region',
      placeHolder: 'Add a location',
      data: '',
      open: false,
      keyEx: 'where[excludeRegion]',
      key: 'where[region]',
      dataKey: 'region',

      type: 2 // selecting
    }
  ]
};
