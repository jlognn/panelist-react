interface IMap {
  [key: string]: string;
}

export const ONE_TIME_LINK: IMap = {
  WE_JUST_EMAILED_A_LINK: "We've just emailed a one-time link to j*****@sit-down.com.au",
  CLICK_ON_THE_LINK: 'Click on the link to sign in instantly',
  TO_YOUR_PANELIST_ACCOUNT: 'to your Panelist account.'
};

export const EVENT_ABOUT_CARD: IMap = {
  EVENT_NAME: 'Event Name',
  EVENT_THEME: 'Event Theme',
  EVENT_OVERVIEW: 'Event Overview',
  EVENT_KEY_DISCUSSION_POINTS: 'Key Discussion Points',
  EVENT_ATTENDING_JOB_TITLES: 'Attending Job titles',
  EVENT_ATTENDING_INSUSTRIES: 'Attending Job Industries'
};
