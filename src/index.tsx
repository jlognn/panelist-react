import './tailwind.output.css';

import React from 'react';
import ReactDOM from 'react-dom';

import { createBrowserHistory } from 'history';
import { Provider } from 'react-redux';
import { Router } from 'react-router-dom';
import { ToastContainer } from 'react-toastify';

import App from './app/layout/App';
import reportWebVitals from './reportWebVitals';
import configureStore from './store';

const rootElement = document.getElementById('root');
const history = createBrowserHistory();
const store = configureStore();

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <Router history={history}>
        <App />
        <ToastContainer
          hideProgressBar={true}
          autoClose={5000}
          position="bottom-left"
          closeOnClick
          draggable
          pauseOnHover
        />
      </Router>
    </Provider>
  </React.StrictMode>,
  rootElement
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
