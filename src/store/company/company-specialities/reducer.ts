import { Reducer } from 'redux';

import { ICompanySpecialitiesState } from '../../../models/company/CompanySpecialitiesState';
import { CompanySpecialitiesActions } from './actions';
import { CompanySpecialitiesActionTypes } from './types';

const initialState: ICompanySpecialitiesState = {
  value: null,
  loading: false,
  errorMessage: ''
};

export const CompanySpecialitiesReducer: Reducer<
  ICompanySpecialitiesState,
  CompanySpecialitiesActions
> = (state = initialState, action) => {
  switch (action.type) {
    case CompanySpecialitiesActionTypes.COMPANY_SPECIALITIES_LOADING: {
      return {
        ...state,
        loading: state.loading
      };
    }
    case CompanySpecialitiesActionTypes.COMPANY_SPECIALITIES_SUCCESS: {
      return {
        ...state,
        value: action.data
      };
    }

    case CompanySpecialitiesActionTypes.COMPANY_SPECIALITIES_ERROR: {
      return {
        ...state,
        errorMessage: action.errorMessage,
        value: null
      };
    }

    default:
      return state;
  }
};
