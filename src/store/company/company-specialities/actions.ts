import { ActionCreator, Dispatch } from 'redux';
import { ThunkAction } from 'redux-thunk';

import { ICompanySpecialitiesResult } from '../../../models/company/CompanySpecialitiesResult';
import { ICompanySpecialitiesState } from '../../../models/company/CompanySpecialitiesState';
import { CompanyService } from '../../../services';
import { CompanySpecialitiesActionTypes } from './types';

export interface ICompanySpecialitiesLoadingAction {
  type: CompanySpecialitiesActionTypes.COMPANY_SPECIALITIES_LOADING;
  loading: boolean;
}

export interface ICompanySpecialitiesSuccessAction {
  type: CompanySpecialitiesActionTypes.COMPANY_SPECIALITIES_SUCCESS;
  data: ICompanySpecialitiesResult;
}

export interface ICompanySpecialitiesErrorAction {
  type: CompanySpecialitiesActionTypes.COMPANY_SPECIALITIES_ERROR;
  errorMessage: string;
}

export type CompanySpecialitiesActions =
  | ICompanySpecialitiesLoadingAction
  | ICompanySpecialitiesSuccessAction
  | ICompanySpecialitiesErrorAction;

export const getCompanySpecialities: ActionCreator<
  ThunkAction<Promise<any>, ICompanySpecialitiesState, null, ICompanySpecialitiesSuccessAction>
> = () => {
  return async (dispatch: Dispatch) => {
    const companyService: CompanyService = new CompanyService();

    try {
      const result = await companyService.getSpecialities();

      dispatch({
        type: CompanySpecialitiesActionTypes.COMPANY_SPECIALITIES_SUCCESS,
        data: result
      });
    } catch (error: any) {
      console.error(error);
      dispatch({
        type: CompanySpecialitiesActionTypes.COMPANY_SPECIALITIES_ERROR,
        errorMessage: error.message
      });
    } finally {
      dispatch({
        type: CompanySpecialitiesActionTypes.COMPANY_SPECIALITIES_LOADING,
        loading: false
      });
    }
  };
};

export const loadCompanyProfilePreview: ActionCreator<
  ThunkAction<any, ICompanySpecialitiesState, null, ICompanySpecialitiesLoadingAction>
> = (shouldLoad: boolean) => (dispatch: Dispatch) =>
  dispatch({
    type: CompanySpecialitiesActionTypes.COMPANY_SPECIALITIES_LOADING,
    loading: shouldLoad
  });
