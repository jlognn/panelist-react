import { ActionCreator, Dispatch } from 'redux';
import { ThunkAction } from 'redux-thunk';

import { ICompanyCoverPhotoState } from '../../../models/company/CoverPhotoState';
import { CoverPhotoActionTypes } from './types';

export interface ICoverPhotoSuccessAction {
  type: CoverPhotoActionTypes.SET_COVER_PHOTO;
  url: string;
}

export interface ICoverPhotoErrorAction {
  type: CoverPhotoActionTypes.COVER_PHOTO_ERROR;
  errorMessage: string;
}

export type CoverPhotoActions = ICoverPhotoSuccessAction | ICoverPhotoErrorAction;

export const setCoverPhoto: ActionCreator<
  ThunkAction<Promise<any>, ICompanyCoverPhotoState, null, ICoverPhotoSuccessAction>
> = (url: string) => {
  return async (dispatch: Dispatch) => {
    dispatch({
      type: CoverPhotoActionTypes.SET_COVER_PHOTO,
      url
    });
  };
};

export const setCoverPhotoError: ActionCreator<
  ThunkAction<Promise<any>, ICompanyCoverPhotoState, null, ICoverPhotoSuccessAction>
> = (fileName: string) => {
  return async (dispatch: Dispatch) => {
    dispatch({
      type: CoverPhotoActionTypes.COVER_PHOTO_ERROR,
      errorMessage: `Failed to upload file ${fileName}`
    });
  };
};
