import { ActionCreator, Dispatch } from 'redux';
import { ThunkAction } from 'redux-thunk';

import { ICompanyProfile } from '../../../models/company/company-profile';
import { ICompanyProfileState } from '../../../models/company/CompanyProfileState';
import { CompanyService } from '../../../services';
import { CompanyProfilePreviewActionTypes } from './types';

export interface ICompanyProfilePreviewLoadingAction {
  type: CompanyProfilePreviewActionTypes.COMPANY_PROFILE_PREVIEW_LOADING;
  loading: boolean;
}

export interface ICompanyProfilePreviewSuccessAction {
  type: CompanyProfilePreviewActionTypes.COMPANY_PROFILE_PREVIEW_SUCCESS;
  payload: ICompanyProfile;
}

export interface ICompanyProfilePreviewErrorAction {
  type: CompanyProfilePreviewActionTypes.COMPANY_PROFILE_PREVIEW_ERROR;
  errorMessage: string;
}

export type CompanyProfilePreviewActions =
  | ICompanyProfilePreviewLoadingAction
  | ICompanyProfilePreviewSuccessAction
  | ICompanyProfilePreviewErrorAction;

export const getCompanyProfilePreview: ActionCreator<
  ThunkAction<Promise<any>, ICompanyProfileState, null, ICompanyProfilePreviewSuccessAction>
> = (companyIdOrSlug: string) => {
  return async (dispatch: Dispatch) => {
    const companyService: CompanyService = new CompanyService();

    try {
      const result = await companyService.getCompanyProfile(companyIdOrSlug);

      dispatch({
        type: CompanyProfilePreviewActionTypes.COMPANY_PROFILE_PREVIEW_SUCCESS,
        payload: result.data
      });
    } catch (error: any) {
      console.error(error);
      dispatch({
        type: CompanyProfilePreviewActionTypes.COMPANY_PROFILE_PREVIEW_ERROR,
        errorMessage: error.message
      });
    } finally {
      dispatch({
        type: CompanyProfilePreviewActionTypes.COMPANY_PROFILE_PREVIEW_LOADING,
        loading: false
      });
    }
  };
};

export const loadCompanyProfilePreview: ActionCreator<
  ThunkAction<any, ICompanyProfileState, null, ICompanyProfilePreviewLoadingAction>
> = (shouldLoad: boolean) => (dispatch: Dispatch) =>
  dispatch({
    type: CompanyProfilePreviewActionTypes.COMPANY_PROFILE_PREVIEW_LOADING,
    loading: shouldLoad
  });
