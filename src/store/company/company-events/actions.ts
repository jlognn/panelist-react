import { ActionCreator, Dispatch } from 'redux';
import { ThunkAction } from 'redux-thunk';

import { ICompanyEventsResult } from '../../../models/company/CompanyEventsResult';
import { ICompanyEventsState } from '../../../models/company/CompanyEventsState';
import { CompanyService } from '../../../services';
import { CompanyEventsActionTypes } from './types';

export interface ICompanyEventsLoadingAction {
  type: CompanyEventsActionTypes.COMPANY_EVENTS_LOADING;
  loading: boolean;
}

export interface ICompanyEventsSuccessAction {
  type: CompanyEventsActionTypes.COMPANY_EVENTS_SUCCESS;
  payload: ICompanyEventsResult;
}

export interface ICompanyEventsErrorAction {
  type: CompanyEventsActionTypes.COMPANY_EVENTS_ERROR;
  errorMessage: string;
}

export type CompanyEventsActions =
  | ICompanyEventsLoadingAction
  | ICompanyEventsSuccessAction
  | ICompanyEventsErrorAction;

export const getCompanyEvents: ActionCreator<
  ThunkAction<Promise<any>, ICompanyEventsState, null, ICompanyEventsSuccessAction>
> = (companyIdOrSlug: string) => {
  return async (dispatch: Dispatch) => {
    const companyService: CompanyService = new CompanyService();

    try {
      const result = await companyService.getCompanyEvents(companyIdOrSlug);

      dispatch({
        type: CompanyEventsActionTypes.COMPANY_EVENTS_SUCCESS,
        payload: result
      });
    } catch (error: any) {
      console.error(error);
      dispatch({
        type: CompanyEventsActionTypes.COMPANY_EVENTS_ERROR,
        errorMessage: error.message
      });
    } finally {
      dispatch({
        type: CompanyEventsActionTypes.COMPANY_EVENTS_LOADING,
        loading: false
      });
    }
  };
};

export const loadCompanyProfilePreview: ActionCreator<
  ThunkAction<any, ICompanyEventsState, null, ICompanyEventsLoadingAction>
> = (shouldLoad: boolean) => (dispatch: Dispatch) =>
  dispatch({
    type: CompanyEventsActionTypes.COMPANY_EVENTS_LOADING,
    loading: shouldLoad
  });
