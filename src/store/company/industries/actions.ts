import { ActionCreator, Dispatch } from 'redux';
import { ThunkAction } from 'redux-thunk';

import { IIndustriesResult } from '../../../models/company/IndustriesResult';
import { IIndustriesState } from '../../../models/company/IndustriesState';
import { CompanyService } from '../../../services';
import { IndustriesActionTypes } from './types';

export interface IIndustriesLoadingAction {
  type: IndustriesActionTypes.INDUSTRIES_LOADING;
  loading: boolean;
}

export interface IIndustriesSuccessAction {
  type: IndustriesActionTypes.INDUSTRIES_SUCCESS;
  data: IIndustriesResult;
}

export interface IIndustriesErrorAction {
  type: IndustriesActionTypes.INDUSTRIES_ERROR;
  errorMessage: string;
}

export type IndustriesActions =
  | IIndustriesLoadingAction
  | IIndustriesSuccessAction
  | IIndustriesErrorAction;

export const getIndustries: ActionCreator<
  ThunkAction<Promise<any>, IIndustriesState, null, IIndustriesSuccessAction>
> = () => {
  return async (dispatch: Dispatch) => {
    const companyService: CompanyService = new CompanyService();

    try {
      const result = await companyService.getIndustries();

      dispatch({
        type: IndustriesActionTypes.INDUSTRIES_SUCCESS,
        data: result
      });
    } catch (error: any) {
      console.error(error);
      dispatch({
        type: IndustriesActionTypes.INDUSTRIES_ERROR,
        errorMessage: error.message
      });
    } finally {
      dispatch({
        type: IndustriesActionTypes.INDUSTRIES_LOADING,
        loading: false
      });
    }
  };
};

export const loadCompanyProfilePreview: ActionCreator<
  ThunkAction<any, IIndustriesState, null, IIndustriesLoadingAction>
> = (shouldLoad: boolean) => (dispatch: Dispatch) =>
  dispatch({
    type: IndustriesActionTypes.INDUSTRIES_LOADING,
    loading: shouldLoad
  });
