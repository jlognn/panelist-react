import { ActionCreator, Dispatch } from 'redux';
import { ThunkAction } from 'redux-thunk';

import { IEditCompanyProfile } from '../../../models/company/company-profile-edit';
import { ICompanyProfileUpdateResult } from '../../../models/company/CompanyProfileUpdateResult';
import { ICompanyProfileUpdateState } from '../../../models/company/CompanyProfileUpdateState';
import { CompanyService } from '../../../services';
import { UpdateCompanyProfileActionTypes } from './types';

export interface ICompanyProfileUpdateInProgress {
  type: UpdateCompanyProfileActionTypes.UPDATE_COMPANY_PROFILE_IN_PROGRESS;
  inProgress: boolean;
}

export interface ICompanyProfileUpdateSuccess {
  type: UpdateCompanyProfileActionTypes.UPDATE_COMPANY_PROFILE_SUCCESS;
  data: ICompanyProfileUpdateResult;
}

export interface ICompanyProfileUpdateError {
  type: UpdateCompanyProfileActionTypes.UPDATE_COMPANY_PROFILE_ERROR;
  errorMessage: string;
}

export type CompanyProfileUpdateActions =
  | ICompanyProfileUpdateInProgress
  | ICompanyProfileUpdateSuccess
  | ICompanyProfileUpdateError;

export const updateProfile: ActionCreator<
  ThunkAction<Promise<any>, ICompanyProfileUpdateState, null, ICompanyProfileUpdateSuccess>
> = (companyIdOrSlug: string, data: IEditCompanyProfile) => {
  return async (dispatch: Dispatch) => {
    const companyService: CompanyService = new CompanyService();

    try {
      const result = await companyService.updateProfile(companyIdOrSlug, data);
      dispatch({
        type: UpdateCompanyProfileActionTypes.UPDATE_COMPANY_PROFILE_SUCCESS,
        payload: result,
        isLoggedIn: true
      });
    } catch (error: any) {
      console.error(error);
      dispatch({
        type: UpdateCompanyProfileActionTypes.UPDATE_COMPANY_PROFILE_ERROR,
        errorMessage: error.message
      });
    } finally {
      dispatch({
        type: UpdateCompanyProfileActionTypes.UPDATE_COMPANY_PROFILE_IN_PROGRESS,
        inProgress: false
      });
    }
  };
};
