import { Reducer } from 'redux';

import { ICompanyProfileUpdateState } from '../../../models/company/CompanyProfileUpdateState';
import { CompanyProfileUpdateActions } from './actions';
import { UpdateCompanyProfileActionTypes } from './types';

const initialState: ICompanyProfileUpdateState = {
  value: null,
  loading: false,
  errorMessage: ''
};

export const CompanyProfileUpdateReducer: Reducer<
  ICompanyProfileUpdateState,
  CompanyProfileUpdateActions
> = (state = initialState, action) => {
  switch (action.type) {
    case UpdateCompanyProfileActionTypes.UPDATE_COMPANY_PROFILE_IN_PROGRESS: {
      return {
        ...state,
        inProgress: state.loading
      };
    }
    case UpdateCompanyProfileActionTypes.UPDATE_COMPANY_PROFILE_SUCCESS: {
      return {
        ...state,
        value: action.data
      };
    }

    case UpdateCompanyProfileActionTypes.UPDATE_COMPANY_PROFILE_ERROR: {
      return {
        ...state,
        errorMessage: action.errorMessage,
        value: null
      };
    }

    default:
      return state;
  }
};
