import { ActionCreator, Dispatch } from 'redux';
import { ThunkAction } from 'redux-thunk';

import { ICompanyResourcesResult } from '../../../models/company/CompanyResourcesResult';
import { ICompanyResourcesState } from '../../../models/company/CompanyResourcesState';
import { CompanyService } from '../../../services';
import { CompanyResourcesActionTypes } from './types';

export interface ICompanyResourcesLoadingAction {
  type: CompanyResourcesActionTypes.COMPANY_RESOURCES_LOADING;
  loading: boolean;
}

export interface ICompanyResourcesSuccessAction {
  type: CompanyResourcesActionTypes.COMPANY_RESOURCES_SUCCESS;
  payload: ICompanyResourcesResult;
}

export interface ICompanyResourcesErrorAction {
  type: CompanyResourcesActionTypes.COMPANY_RESOURCES_ERROR;
  errorMessage: string;
}

export type CompanyResourcesActions =
  | ICompanyResourcesLoadingAction
  | ICompanyResourcesSuccessAction
  | ICompanyResourcesErrorAction;

export const getCompanyResources: ActionCreator<
  ThunkAction<Promise<any>, ICompanyResourcesState, null, ICompanyResourcesSuccessAction>
> = (companyIdOrSlug: string) => {
  return async (dispatch: Dispatch) => {
    const companyService: CompanyService = new CompanyService();

    try {
      const result = await companyService.getCompanyResources(companyIdOrSlug);

      dispatch({
        type: CompanyResourcesActionTypes.COMPANY_RESOURCES_SUCCESS,
        payload: result
      });
    } catch (error: any) {
      console.error(error);
      dispatch({
        type: CompanyResourcesActionTypes.COMPANY_RESOURCES_ERROR,
        errorMessage: error.message
      });
    } finally {
      dispatch({
        type: CompanyResourcesActionTypes.COMPANY_RESOURCES_LOADING,
        loading: false
      });
    }
  };
};

export const loadCompanyProfilePreview: ActionCreator<
  ThunkAction<any, ICompanyResourcesState, null, ICompanyResourcesLoadingAction>
> = (shouldLoad: boolean) => (dispatch: Dispatch) =>
  dispatch({
    type: CompanyResourcesActionTypes.COMPANY_RESOURCES_LOADING,
    loading: shouldLoad
  });
