import { ActionCreator, Dispatch } from 'redux';
import { ThunkAction } from 'redux-thunk';

import { IUpcomingEventAgendaResult } from '../../../models/upcoming-event/UpcomingEventAgendaResult';
import { IUpcomingEventAgendaState } from '../../../models/upcoming-event/UpcomingEventAgendaState';
import { EventsService } from '../../../services/events';
import { UpcomingEventAgendaActionTypes } from './types';

export interface IUpcomingEventAgendaLoadingAction {
  type: UpcomingEventAgendaActionTypes.EVENT_AGENDA_LOADING;
  loading: boolean;
}

export interface IUpcomingEventAgendaSuccessAction {
  type: UpcomingEventAgendaActionTypes.EVENT_AGENDA_SUCCESS;
  payload: IUpcomingEventAgendaResult;
}
export interface IUpcomingEventAgendaErrorAction {
  type: UpcomingEventAgendaActionTypes.EVENT_AGENDA_ERROR;
  errorMessage: string;
}

export type UpcomingEventAgendaActions =
  | IUpcomingEventAgendaLoadingAction
  | IUpcomingEventAgendaSuccessAction
  | IUpcomingEventAgendaErrorAction;

export const getUpcomingEventAgenda: ActionCreator<
  ThunkAction<Promise<any>, IUpcomingEventAgendaState, null, IUpcomingEventAgendaSuccessAction>
> = (eventIdOrSlug: string) => {
  return async (dispatch: Dispatch) => {
    const eventsService: EventsService = new EventsService();

    try {
      const result = await eventsService.getUpcomingEventAgenda(eventIdOrSlug);

      dispatch({
        type: UpcomingEventAgendaActionTypes.EVENT_AGENDA_SUCCESS,
        payload: result
      });
    } catch (error: any) {
      console.error(error);
      dispatch({
        type: UpcomingEventAgendaActionTypes.EVENT_AGENDA_ERROR,
        errorMessage: error.message
      });
    } finally {
      dispatch({
        type: UpcomingEventAgendaActionTypes.EVENT_AGENDA_LOADING,
        loading: false
      });
    }
  };
};

export const loadUpcomingEvent: ActionCreator<
  ThunkAction<any, IUpcomingEventAgendaState, null, IUpcomingEventAgendaLoadingAction>
> = (shouldLoad: boolean) => (dispatch: Dispatch) =>
  dispatch({
    type: UpcomingEventAgendaActionTypes.EVENT_AGENDA_LOADING,
    loading: shouldLoad
  });
