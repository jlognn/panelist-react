import { Reducer } from 'redux';

import { IUpcomingEventState } from '../../../models/upcoming-event/UpcomingEventState';
import { UpcomingEventActions } from './actions';
import { UpcomingEventActionTypes } from './types';

const initialState: IUpcomingEventState = {
  value: null,
  loading: false,
  errorMessage: ''
};

export const UpcomingEventReducer: Reducer<IUpcomingEventState, UpcomingEventActions> = (
  state = initialState,
  action
) => {
  switch (action.type) {
    case UpcomingEventActionTypes.UPCOMING_EVENT_LOADING: {
      return {
        ...state,
        loading: state.loading
      };
    }
    case UpcomingEventActionTypes.UPCOMING_EVENT_SUCCESS: {
      return {
        ...state,
        value: action.payload
      };
    }

    case UpcomingEventActionTypes.UPCOMING_EVENT_ERROR: {
      return {
        ...state,
        errorMessage: action.errorMessage,
        value: null
      };
    }

    default:
      return state;
  }
};
