import { ActionCreator, Dispatch } from 'redux';
import { ThunkAction } from 'redux-thunk';

import { IUpcomingEventResult } from '../../../models/upcoming-event/UpcomingEventResult';
import { IUpcomingEventState } from '../../../models/upcoming-event/UpcomingEventState';
import { EventsService } from '../../../services/events';
import { UpcomingEventActionTypes } from './types';

export interface IUpcomingEventLoadingAction {
  type: UpcomingEventActionTypes.UPCOMING_EVENT_LOADING;
  loading: boolean;
}

export interface IUpcomingEventSuccessAction {
  type: UpcomingEventActionTypes.UPCOMING_EVENT_SUCCESS;
  payload: IUpcomingEventResult;
}
export interface IUpcomingEventErrorAction {
  type: UpcomingEventActionTypes.UPCOMING_EVENT_ERROR;
  errorMessage: string;
}

export type UpcomingEventActions =
  | IUpcomingEventLoadingAction
  | IUpcomingEventSuccessAction
  | IUpcomingEventErrorAction;

export const getUpcomingEvent: ActionCreator<
  ThunkAction<Promise<any>, IUpcomingEventState, null, IUpcomingEventSuccessAction>
> = (eventIdOrSlug: string) => {
  return async (dispatch: Dispatch) => {
    const eventsService: EventsService = new EventsService();

    try {
      const result = await eventsService.getUpcomingEventDetails(eventIdOrSlug);

      dispatch({
        type: UpcomingEventActionTypes.UPCOMING_EVENT_SUCCESS,
        payload: result
      });
    } catch (error: any) {
      console.error(error);
      dispatch({
        type: UpcomingEventActionTypes.UPCOMING_EVENT_ERROR,
        errorMessage: error.message
      });
    } finally {
      dispatch({
        type: UpcomingEventActionTypes.UPCOMING_EVENT_LOADING,
        loading: false
      });
    }
  };
};

export const loadUpcomingEvent: ActionCreator<
  ThunkAction<any, IUpcomingEventState, null, IUpcomingEventLoadingAction>
> = (shouldLoad: boolean) => (dispatch: Dispatch) =>
  dispatch({
    type: UpcomingEventActionTypes.UPCOMING_EVENT_LOADING,
    loading: shouldLoad
  });
