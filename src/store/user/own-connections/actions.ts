import { ActionCreator, Dispatch } from 'redux';
import { ThunkAction } from 'redux-thunk';

import { IConnectionsResult } from '../../../models/connection';
import { ConnectionsState } from '../../../models/ConnectionsState';
import { UserService } from '../../../services';
import { OwnConnectionsActionTypes } from './types';

export interface IOwnConnectionsLoadingAction {
  type: OwnConnectionsActionTypes.OWN_CONNECTIONS_LOADING;
  loading: boolean;
}

export interface IOwnConnectionsSuccessAction {
  type: OwnConnectionsActionTypes.OWN_CONNECTIONS_SUCCESS;
  payload: IConnectionsResult;
}

export interface IOwnConnectionsErrorAction {
  type: OwnConnectionsActionTypes.OWN_CONNECTIONS_ERROR;
  errorMessage: string;
}

export type OwnConnectionsActions =
  | IOwnConnectionsLoadingAction
  | IOwnConnectionsSuccessAction
  | IOwnConnectionsErrorAction;

export const getOwnConnections: ActionCreator<
  ThunkAction<Promise<any>, ConnectionsState, null, IOwnConnectionsSuccessAction>
> = () => {
  return async (dispatch: Dispatch) => {
    const userService: UserService = new UserService();

    try {
      const result = await userService.getOwnConnections();

      dispatch({
        type: OwnConnectionsActionTypes.OWN_CONNECTIONS_SUCCESS,
        payload: result
      });
    } catch (error: any) {
      console.error(error);
      dispatch({
        type: OwnConnectionsActionTypes.OWN_CONNECTIONS_ERROR,
        errorMessage: error.message
      });
    } finally {
      dispatch({
        type: OwnConnectionsActionTypes.OWN_CONNECTIONS_LOADING,
        loading: false
      });
    }
  };
};

export const loadOwnConnections: ActionCreator<
  ThunkAction<any, ConnectionsState, null, IOwnConnectionsLoadingAction>
> = (shouldLoad: boolean) => (dispatch: Dispatch) =>
  dispatch({
    type: OwnConnectionsActionTypes.OWN_CONNECTIONS_LOADING,
    loading: shouldLoad
  });
