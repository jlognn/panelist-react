import { ActionCreator, Dispatch } from 'redux';
import { ThunkAction } from 'redux-thunk';

import { UserLoginModel } from '../../../models/user-login';
import { UserLoginResult } from '../../../models/user-login-result';
import { IUserLoginState } from '../../../models/UserLoginState';
import { UserService } from '../../../services';
import { UserLoginActionTypes } from './types';

export interface IUserLoginInProgressAction {
  type: UserLoginActionTypes.LOGIN_USER_IN_PROGRESS;
  loggingIn: true;
}

export interface IUserLoginSuccessAction {
  type: UserLoginActionTypes.LOGIN_USER_SUCCESS;
  payload: UserLoginResult;
}

export interface IUserLoginFailureAction {
  type: UserLoginActionTypes.LOGIN_USER_FAILURE;
  errorMessage: string;
}

export interface IUserLoginErrorAction {
  type: UserLoginActionTypes.LOGIN_USER_ERROR;
  errorMessage: string;
}

export interface ILogoutUserInProgressAction {
  type: UserLoginActionTypes.LOGOUT_USER_IN_PROGRESS;
  loggingOut: true;
}

export interface ILogoutUserSuccess {
  type: UserLoginActionTypes.LOGOUT_USER_SUCCESS;
  isLoggedIn: false;
}

export interface ILogoutUserError {
  type: UserLoginActionTypes.LOGOUT_USER_ERROR;
  isLoggedOut: false;
  errorMessage: string;
}

export type LoginUserActions =
  | IUserLoginInProgressAction
  | IUserLoginSuccessAction
  | IUserLoginFailureAction
  | IUserLoginErrorAction
  | ILogoutUserInProgressAction
  | ILogoutUserSuccess
  | ILogoutUserError;

export const login: ActionCreator<
  ThunkAction<Promise<any>, IUserLoginState, null, IUserLoginSuccessAction>
> = (data: UserLoginModel) => {
  return async (dispatch: Dispatch) => {
    const userService: UserService = new UserService();

    try {
      const result = await userService.login(data);

      dispatch({
        type: UserLoginActionTypes.LOGIN_USER_SUCCESS,
        payload: result,
        isLoggedIn: true
      });
    } catch (error: any) {
      console.error(error);

      if (error.errorCode === '401') {
        dispatch({
          type: UserLoginActionTypes.LOGIN_USER_ERROR,
          errorMessage: error.message,
          isLoggedIn: false
        });
      } else {
        dispatch({
          type: UserLoginActionTypes.LOGIN_USER_ERROR,
          errorMessage: error.message,
          isLoggedIn: false
        });
      }
    } finally {
      dispatch({
        type: UserLoginActionTypes.LOGIN_USER_IN_PROGRESS,
        loggingIn: false
      });
    }
  };
};

export const loggingInUser: ActionCreator<
  ThunkAction<any, IUserLoginState, null, IUserLoginInProgressAction>
> = (authenticating: boolean) => (dispatch: Dispatch) =>
  dispatch({
    type: UserLoginActionTypes.LOGIN_USER_IN_PROGRESS,
    loggingIn: authenticating
  });

export const logout: ActionCreator<
  ThunkAction<Promise<any>, IUserLoginState, null, IUserLoginSuccessAction>
> = (data: UserLoginModel) => {
  return async (dispatch: Dispatch) => {
    try {
      sessionStorage.removeItem('auth');
      sessionStorage.removeItem('isAuthenticated');

      dispatch({
        type: UserLoginActionTypes.LOGOUT_USER_SUCCESS,
        payload: null,
        isLoggedIn: false
      });
    } catch (error: any) {
      dispatch({
        type: UserLoginActionTypes.LOGOUT_USER_ERROR,
        errorMessage: error.message,
        isLoggedIn: false
      });
    } finally {
      dispatch({
        type: UserLoginActionTypes.LOGOUT_USER_IN_PROGRESS,
        loggingOut: false
      });
    }
  };
};

export const loggingOutUser: ActionCreator<
  ThunkAction<any, IUserLoginState, null, IUserLoginInProgressAction>
> = (authenticating: boolean) => (dispatch: Dispatch) =>
  dispatch({
    type: UserLoginActionTypes.LOGOUT_USER_IN_PROGRESS,
    loggingOut: authenticating
  });
