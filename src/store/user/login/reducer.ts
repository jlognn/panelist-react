import { Reducer } from 'redux';

import { IUserLoginState } from '../../../models/UserLoginState';
import { LoginUserActions } from './actions';
import { UserLoginActionTypes } from './types';

const initialState: IUserLoginState = {
  isLoggedIn: false,
  value: null,
  loading: false,
  errorMessage: ''
};

export const LoginReducer: Reducer<IUserLoginState, LoginUserActions> = (
  state = initialState,
  action
) => {
  switch (action.type) {
    case UserLoginActionTypes.LOGIN_USER_IN_PROGRESS: {
      return {
        ...state,
        loading: action.loggingIn
      };
    }

    case UserLoginActionTypes.LOGIN_USER_SUCCESS: {
      return {
        ...state,
        isLoggedIn: true,
        value: action.payload
      };
    }

    case UserLoginActionTypes.LOGIN_USER_FAILURE: {
      return {
        ...state,
        isLoggedIn: false,
        errorMessage: action.errorMessage,
        value: null
      };
    }

    case UserLoginActionTypes.LOGIN_USER_ERROR: {
      return {
        ...state,
        isLoggedIn: false,
        errorMessage: action.errorMessage,
        value: null
      };
    }

    case UserLoginActionTypes.LOGOUT_USER_IN_PROGRESS: {
      return {
        ...state,
        loading: action.loggingOut
      };
    }

    case UserLoginActionTypes.LOGOUT_USER_SUCCESS: {
      return {
        ...state,
        isLoggedIn: false,
        value: null
      };
    }

    case UserLoginActionTypes.LOGOUT_USER_ERROR: {
      return {
        ...state,
        errorMessage: action.errorMessage
      };
    }

    default:
      return state;
  }
};
