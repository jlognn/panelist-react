import { Reducer } from 'redux';

import { CheckUserEmailState } from '../../../models/CheckUserEmailState';
import { CheckUserEmailActions } from './actions';
import { CheckUserEmailActionTypes } from './types';

const initialState: CheckUserEmailState = {
  value: null,
  loading: false,
  errorMessage: ''
};

export const CheckUserEmailReducer: Reducer<CheckUserEmailState, CheckUserEmailActions> = (
  state = initialState,
  action
) => {
  switch (action.type) {
    case CheckUserEmailActionTypes.CHECK_USER_EMAIL_LOADING: {
      return {
        ...state,
        loading: action.loading
      };
    }

    case CheckUserEmailActionTypes.CHECK_USER_EMAIL_SUCCESS: {
      return {
        ...state,
        value: action.payload
      };
    }

    case CheckUserEmailActionTypes.CHECK_USER_EMAIL_ERROR: {
      return {
        ...state,
        errorMessage: action.errorMessage,
        value: null
      };
    }

    default:
      return state;
  }
};
