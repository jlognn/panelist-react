import { ActionCreator, Dispatch } from 'redux';
import { ThunkAction } from 'redux-thunk';

import { CheckEmailModel } from '../../../models/check-email';
import { CheckUserEmailResult } from '../../../models/CheckUserEmailResult';
import { CheckUserEmailState } from '../../../models/CheckUserEmailState';
import { UserService } from '../../../services';
import { CheckUserEmailActionTypes } from './types';

export interface ICheckUserEmailSuccessAction {
  type: CheckUserEmailActionTypes.CHECK_USER_EMAIL_SUCCESS;
  payload: CheckUserEmailResult;
}

export interface ICheckUserEmailLoadingAction {
  type: CheckUserEmailActionTypes.CHECK_USER_EMAIL_LOADING;
  loading: boolean;
}

export interface ICheckUserEmailErrorAction {
  type: CheckUserEmailActionTypes.CHECK_USER_EMAIL_ERROR;
  errorMessage: string;
}

export type CheckUserEmailActions =
  | ICheckUserEmailSuccessAction
  | ICheckUserEmailLoadingAction
  | ICheckUserEmailErrorAction;

export const checkUserEmail: ActionCreator<
  ThunkAction<Promise<any>, CheckUserEmailState, null, ICheckUserEmailSuccessAction>
> = (data: CheckEmailModel) => {
  return async (dispatch: Dispatch) => {
    const userService: UserService = new UserService();

    try {
      const result = await userService.checkEmail(data);

      dispatch({
        type: CheckUserEmailActionTypes.CHECK_USER_EMAIL_SUCCESS,
        payload: result.result
      });
    } catch (error: any) {
      console.error(error);
      dispatch({
        type: CheckUserEmailActionTypes.CHECK_USER_EMAIL_ERROR,
        errorMessage: error.message
      });
    } finally {
      dispatch({
        type: CheckUserEmailActionTypes.CHECK_USER_EMAIL_LOADING,
        loading: false
      });
    }
  };
};

export const loadCheckUserEmail: ActionCreator<
  ThunkAction<any, CheckUserEmailState, null, ICheckUserEmailLoadingAction>
> = (shouldLoad: boolean) => (dispatch: Dispatch) =>
  dispatch({
    type: CheckUserEmailActionTypes.CHECK_USER_EMAIL_LOADING,
    loading: shouldLoad
  });
