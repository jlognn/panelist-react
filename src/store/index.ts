import { applyMiddleware, combineReducers, createStore, Store } from 'redux';
import thunk from 'redux-thunk';

import { CheckUserEmailState } from '../models/CheckUserEmailState';
import { ICompanyEmployeesState } from '../models/company/CompanyEmployeesState';
import { ICompanyEventsState } from '../models/company/CompanyEventsState';
import { ICompanyProfileState } from '../models/company/CompanyProfileState';
import { ICompanyResourcesState } from '../models/company/CompanyResourcesState';
import { ICompanySizesState } from '../models/company/CompanySizesState';
import { ICompanySpecialitiesState } from '../models/company/CompanySpecialitiesState';
import { ICompanyCoverPhotoState } from '../models/company/CoverPhotoState';
import { IIndustriesState } from '../models/company/IndustriesState';
import { IJobFunctionState } from '../models/company/JobFunctionsState';
import { IPostsState } from '../models/company/PostsState';
import { IProfilePictureState } from '../models/company/ProfilePictureState';
import { ConnectionsState } from '../models/ConnectionsState';
import { ConnectSuggestionsState } from '../models/ConnectSuggestionsState';
import { IUploadImageState } from '../models/fileupload/UploadImageState';
import { IContentFilterState } from '../models/library-content/ContentFilterState';
import { IContentState } from '../models/library-content/ContentState';
import { IRegionState } from '../models/library-content/RegionState';
import { OwnExperienceState } from '../models/OwnExperienceState';
import { RecommendedEventsState } from '../models/RecommendedEventsState';
import { SelectedPackageState } from '../models/SelectedPackageState';
import { IUpcomingEventAgendaState } from '../models/upcoming-event/UpcomingEventAgendaState';
import { IUpcomingEventSpeakersState } from '../models/upcoming-event/UpcomingEventSpeakersState';
import { IUpcomingEventState } from '../models/upcoming-event/UpcomingEventState';
import { UserEventsState } from '../models/UserEventsState';
import { IUserLoginState } from '../models/UserLoginState';
import { UserProfileState } from '../models/UserProfileState';
import { CompanyEmployeesReducer } from './company/company-employees/reducer';
import { CompanyEventsReducer } from './company/company-events/reducer';
import { CompanyPostsReducer } from './company/company-posts/reducer';
import { CompanyResourcesReducer } from './company/company-resources/reducer';
import { CompanySizesReducer } from './company/company-sizes/reducer';
import { CompanySpecialitiesReducer } from './company/company-specialities/reducer';
import { SetCoverPhotoReducer } from './company/cover-photo/reducer';
import { IndustriesReducer } from './company/industries/reducer';
import { JobFunctionsReducer } from './company/job-functions/reducer';
import { SetProfilePictureReducer } from './company/profile-picture/reducer';
import { CompanyProfilePreviewReducer } from './company/profile-preview/reducer';
import { SetContentFilterReducer } from './content-filter/reducer';
import { UpcomingEventAgendaReducer } from './events/upcoming-event-agenda/reducer';
import { UpcomingEventReducer } from './events/upcoming-event/reducer';
import { UpcomingEventSpeakersReducer } from './events/upcominn-event-speakers/reducer';
import { ImageUploadReducer } from './fileupload/imageupload/reducer';
import { ContentReducer } from './library-content/reducer';
import { SetSelectedPackageReducer } from './packages/reducer';
import { RegionsReducer } from './regions/reducer';
import { ConnectSuggestionsReducer } from './user/connect-suggestions/reducer';
import { LoginReducer } from './user/login/reducer';
import { CheckUserEmailReducer } from './user/loginorjoin/reducer';
import { OwnConnectionsReducer } from './user/own-connections/reducer';
import { OwnExperienceReducer } from './user/own-experience/reducer';
import { RecommendedEventsReducer } from './user/recommended-events/reducer';
import { UserEventsReducer } from './user/user-events/reducer';
import { UserProfileReducer } from './user/user-profile/reducer';

export interface IAppState {
  checkUserEmail: CheckUserEmailState;
  authenticateUser: IUserLoginState;
  setSelectedPackage: SelectedPackageState;
  userProfile: UserProfileState;
  ownExperience: OwnExperienceState;
  connections: ConnectionsState;
  connectSuggestions: ConnectSuggestionsState;
  userEvents: UserEventsState;
  recommendedEvents: RecommendedEventsState;
  companySpecialities: ICompanySpecialitiesState;
  companyEmployees: ICompanyEmployeesState;
  companySizes: ICompanySizesState;
  industries: IIndustriesState;
  jobFunctions: IJobFunctionState;
  regions: IRegionState;
  companyProfilePreview: ICompanyProfileState;
  companyCoverPhoto: ICompanyCoverPhotoState;
  companyProfilePicture: IProfilePictureState;
  imageUpload: IUploadImageState;
  companyEvents: ICompanyEventsState;
  companyResources: ICompanyResourcesState;
  posts: IPostsState;
  libraryContent: IContentState;
  contentFilter: IContentFilterState;
  upcomingEvent: IUpcomingEventState;
  upcomingEventSpeakers: IUpcomingEventSpeakersState;
  upcomingEventAgenda: IUpcomingEventAgendaState;
}

const rootReducer = combineReducers<IAppState>({
  checkUserEmail: CheckUserEmailReducer,
  authenticateUser: LoginReducer,
  setSelectedPackage: SetSelectedPackageReducer,
  userProfile: UserProfileReducer,
  ownExperience: OwnExperienceReducer,
  connections: OwnConnectionsReducer,
  connectSuggestions: ConnectSuggestionsReducer,
  userEvents: UserEventsReducer,
  recommendedEvents: RecommendedEventsReducer,
  companySpecialities: CompanySpecialitiesReducer,
  companyEmployees: CompanyEmployeesReducer,
  companySizes: CompanySizesReducer,
  industries: IndustriesReducer,
  jobFunctions: JobFunctionsReducer,
  regions: RegionsReducer,
  companyProfilePreview: CompanyProfilePreviewReducer,
  companyCoverPhoto: SetCoverPhotoReducer,
  companyProfilePicture: SetProfilePictureReducer,
  imageUpload: ImageUploadReducer,
  companyEvents: CompanyEventsReducer,
  companyResources: CompanyResourcesReducer,
  posts: CompanyPostsReducer,
  libraryContent: ContentReducer,
  contentFilter: SetContentFilterReducer,
  upcomingEvent: UpcomingEventReducer,
  upcomingEventSpeakers: UpcomingEventSpeakersReducer,
  upcomingEventAgenda: UpcomingEventAgendaReducer
});

export default function configureStore(): Store<IAppState, any> {
  return createStore(rootReducer, undefined, applyMiddleware(thunk));
}
