import { ActionCreator, Dispatch } from 'redux';
import { ThunkAction } from 'redux-thunk';

import { IContentFilterState } from '../../models/library-content/ContentFilterState';
import { ContentFilterActionTypes } from './types';

export interface IContentFilterSuccessAction {
  type: ContentFilterActionTypes.SET_FILTER_VALUE;
  filterString: string;
}

export interface IContentFilterErrorAction {
  type: ContentFilterActionTypes.FILTER_VALUE_ERROR;
  errorMessage: string;
}

export type ContentFilterActions = IContentFilterSuccessAction | IContentFilterErrorAction;

export const setContentFilter: ActionCreator<
  ThunkAction<Promise<any>, IContentFilterState, null, IContentFilterSuccessAction>
> = (filterString: string) => {
  return async (dispatch: Dispatch) => {
    dispatch({
      type: ContentFilterActionTypes.SET_FILTER_VALUE,
      filterString
    });
  };
};

export const setContentFilterError: ActionCreator<
  ThunkAction<Promise<any>, IContentFilterState, null, IContentFilterSuccessAction>
> = (fileName: string) => {
  return async (dispatch: Dispatch) => {
    dispatch({
      type: ContentFilterActionTypes.FILTER_VALUE_ERROR,
      errorMessage: `Failed to upload file ${fileName}`
    });
  };
};
