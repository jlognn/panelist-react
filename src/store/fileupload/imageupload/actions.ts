import { ActionCreator, Dispatch } from 'redux';
import { ThunkAction } from 'redux-thunk';

import { IUploadImageResult } from '../../../models/fileupload/UploadImageResult';
import { IUploadImageState } from '../../../models/fileupload/UploadImageState';
import { FilUploadService } from '../../../services/FileUpload';
import { ImageUploadActionTypes } from './types';

export interface IImageUploadInProgressAction {
  type: ImageUploadActionTypes.IMAGE_UPLOAD_IN_PROGRESS;
  uploading: boolean;
}

export interface IImageUploadSuccessAction {
  type: ImageUploadActionTypes.IMAGE_UPLOAD_SUCCESS;
  payload: IUploadImageResult;
}

export interface IImageUploadError {
  type: ImageUploadActionTypes.IMAGE_UPLOAD_ERROR;
  errorMessage: string;
}

export type ImageUploadActions =
  | IImageUploadInProgressAction
  | IImageUploadSuccessAction
  | IImageUploadError;

export const uploadImage: ActionCreator<
  ThunkAction<Promise<any>, IUploadImageState, null, IImageUploadSuccessAction>
> = (imageFile: File) => {
  return async (dispatch: Dispatch) => {
    const fileuploadService = new FilUploadService();

    try {
      const result = await fileuploadService.uploadImage(imageFile);

      dispatch({
        type: ImageUploadActionTypes.IMAGE_UPLOAD_SUCCESS,
        payload: result.data
      });
    } catch (error: any) {
      console.error(error);
      dispatch({
        type: ImageUploadActionTypes.IMAGE_UPLOAD_ERROR,
        errorMessage: error.message
      });
    } finally {
      dispatch({
        type: ImageUploadActionTypes.IMAGE_UPLOAD_IN_PROGRESS,
        loading: false
      });
    }
  };
};

export const setImageUploadProgress: ActionCreator<
  ThunkAction<any, IUploadImageState, null, IImageUploadInProgressAction>
> = (isUploading: boolean) => (dispatch: Dispatch) =>
  dispatch({
    type: ImageUploadActionTypes.IMAGE_UPLOAD_IN_PROGRESS,
    uploading: isUploading
  });
