import { IUpcomingEventAgendaResult } from '../../models/upcoming-event/UpcomingEventAgendaResult';
import { IUpcomingEventResult } from '../../models/upcoming-event/UpcomingEventResult';
import { IUpcomingEventsSpeakerResult } from '../../models/upcoming-event/UpcomingEventSpeakersResult';
import BaseService from '../BaseService';

export class EventsService extends BaseService {
  async getUpcomingEventDetails(eventIdOrSlug: string) {
    const extraOptions = this.getAuthToken();

    return this.get<IUpcomingEventResult>(`/events/${eventIdOrSlug}`, extraOptions);
  }

  async getUpcomingEventSpeakers(eventIdOrSlug: string) {
    const extraOptions = this.getAuthToken();

    return this.get<IUpcomingEventsSpeakerResult>(
      `/events/${eventIdOrSlug}/speakers?page=1&limit=10`,
      extraOptions
    );
  }

  async getUpcomingEventAgenda(eventIdOrSlug: string) {
    const extraOptions = this.getAuthToken();

    return this.get<IUpcomingEventAgendaResult>(`/events/${eventIdOrSlug}/agenda`, extraOptions);
  }
}
