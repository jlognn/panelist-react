import { CheckEmailModel } from '../../models/check-email';
import { CheckUserEmailResult } from '../../models/CheckUserEmailResult';
import { IConnectionsResult } from '../../models/connection';
import { ConnectSuggestionsResult } from '../../models/ConnectSuggestionsResult';
import { IOwnExperienceResult } from '../../models/own-experience';
import { RecommendedEventsResult } from '../../models/RecommendedEventsResult';
import { UserLoginModel } from '../../models/user-login';
import { UserLoginResult } from '../../models/user-login-result';
import { IUserProfileResult } from '../../models/user-profile';
import { UserEventsResult } from '../../models/UserEventsResult';
import BaseService from '../BaseService';

export class UserService extends BaseService {
  async checkEmail(data: CheckEmailModel) {
    return this.post<CheckUserEmailResult>('/auth/email-check', data);
  }

  async login(data: UserLoginModel) {
    return this.post<UserLoginResult>('/auth/login', data);
  }

  async getOwnProfile() {
    const extraOptions = this.getAuthToken();

    return this.get<IUserProfileResult>('/users/me/profile', extraOptions);
  }

  async getOwnExperience() {
    const extraOptions = this.getAuthToken();

    return this.get<IOwnExperienceResult>('/users/me/experiences', extraOptions);
  }

  async getOwnConnections() {
    const extraOptions = this.getAuthToken();

    return this.get<IConnectionsResult>('/users/me/connections', extraOptions);
  }

  async getConnectSuggestions() {
    const extraOptions = this.getAuthToken();

    return this.get<ConnectSuggestionsResult>(
      '/users/me/connection-suggests?page=1&limit=5',
      extraOptions
    );
  }

  async getOwnEvents() {
    const extraOptions = this.getAuthToken();

    return this.get<UserEventsResult>(
      '/events/user?page=1&limit=3&where[isHosting]=1&where[isSpeaking]=1&where[isSponsoring]=1&where[isAttending]=1',
      extraOptions
    );
  }

  async getRecommendedEvents() {
    const extraOptions = this.getAuthToken();

    return this.get<RecommendedEventsResult>('/events/recommend?param=1', extraOptions);
  }
}
