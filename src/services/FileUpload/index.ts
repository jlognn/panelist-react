import { IUploadImageResult } from '../../models/fileupload/UploadImageResult';
import BaseService from '../BaseService';

export class FilUploadService extends BaseService {
  async uploadImage(imageFile: any) {
    const extraOptions = this.getAuthToken();

    return this.postFile<IUploadImageResult>('/upload/image', imageFile, extraOptions);
  }
}
