import { ICommentsResult } from '../../models/company/CommentsResult';
import { IEditCompanyProfile } from '../../models/company/company-profile-edit';
import { ICompanyEmployeesResult } from '../../models/company/CompanyEmployeesResult';
import { ICompanyEventsResult } from '../../models/company/CompanyEventsResult';
import { ICompanyProfileResult } from '../../models/company/CompanyProfileResult';
import { ICompanyProfileUpdateResult } from '../../models/company/CompanyProfileUpdateResult';
import { ICompanyResourcesResult } from '../../models/company/CompanyResourcesResult';
import { ICompanySizesResult } from '../../models/company/CompanySizesResult';
import { ICompanySpecialitiesResult } from '../../models/company/CompanySpecialitiesResult';
import { IIndustriesResult } from '../../models/company/IndustriesResult';
import { IJobFunctionsResult } from '../../models/company/JobFunctionsResult';
import { IPost, IPostComment } from '../../models/company/post';
import { IPostsResult } from '../../models/company/PostsResult';
import BaseService from '../BaseService';

export class CompanyService extends BaseService {
  async getSpecialities() {
    const extraOptions = this.getAuthToken();

    return this.get<ICompanySpecialitiesResult>(
      '/company-specialities/short?page=1&limit=10&search=',
      extraOptions
    );
  }

  async getCompanySizes() {
    const extraOptions = this.getAuthToken();

    return this.get<ICompanySizesResult>('/company-sizes?page=1&limit=10&search=', extraOptions);
  }

  async getIndustries() {
    const extraOptions = this.getAuthToken();

    return this.get<IIndustriesResult>('/industries/short?page=1&limit=150&search=', extraOptions);
  }

  async getJobFunctions() {
    const extraOptions = this.getAuthToken();

    return this.get<IJobFunctionsResult>('/job-functions/short', extraOptions);
  }

  async getCompanyProfile(companyIdOrSlug: string) {
    const extraOptions = this.getAuthToken();

    return this.get<ICompanyProfileResult>(`/companies/${companyIdOrSlug}`, extraOptions);
  }

  async getCompanyEmployees(companyIdOrSlug: string) {
    const extraOptions = this.getAuthToken();

    return this.get<ICompanyEmployeesResult>(
      `/companies/${companyIdOrSlug}/employees?page=1&limit=10`,
      extraOptions
    );
  }

  async getCompanyEvents(companyIdOrSlug: string) {
    const extraOptions = this.getAuthToken();

    return this.get<ICompanyEventsResult>(
      `/events/company?page=1&limit=10&companyId=${companyIdOrSlug}&where[isHosting]=1&where[isSponsoring]=1`,
      extraOptions
    );
  }

  async getCompanyResources(companyIdOrSlug: string) {
    const extraOptions = this.getAuthToken();

    return this.get<ICompanyResourcesResult>(
      `/resources?page=1&limit=10&companyId=${companyIdOrSlug}`,
      extraOptions
    );
  }

  async getCompanyPosts(companyIdOrSlug: string) {
    const extraOptions = this.getAuthToken();

    const postsResult = await this.get<IPostsResult>(
      `/companies/${companyIdOrSlug}/posts?page=1&limit=10&order[createdAt]=DESC`,
      extraOptions
    );

    let comments: IPostComment[] = [];
    let updatedPosts: IPost[] = [];

    const postsWithComments = postsResult.data.map(async (post) => {
      const commentsResult = await this.getPostComments(post.id);

      commentsResult.data.forEach((comment) =>
        comments.push({
          avatarUrl: comment.commentedAsUser.avatar
            ? `${process.env.REACT_APP_IMAGE_URL_PREFIX_STAGING}/${comment.commentedAsUser.avatar}`
            : `${process.env.REACT_APP_IMAGE_URL_PREFIX_STAGING}/images/20211129/blank-profile-picture-g7b0695023640-udq004b.png`,
          name: `${comment.commentedAsUser.firstName} ${comment.commentedAsUser.lastName}`,
          jobTitle: '',
          companyName: '',
          timeAgo: '',
          commentText: comment.content,
          reactionsCount:
            comment.totalClaps +
            comment.totalHahas +
            comment.totalLikes +
            comment.totalLoves +
            comment.totalSmiles
        })
      );

      post.comments = comments;
      comments = [];

      return post;
    });

    updatedPosts = await Promise.all(postsWithComments);

    return { ...postsResult, data: updatedPosts };
  }

  async getPostComments(postId: string) {
    const extraOptions = this.getAuthToken();

    return this.get<ICommentsResult>(`/posts/${postId}/comments?page=1&limit=10`, extraOptions);
  }

  async updateProfile(companyIdOrSlug: string, data: IEditCompanyProfile) {
    const extraOptions = this.getAuthToken();

    return this.put<ICompanyProfileUpdateResult>(
      `/companies/${companyIdOrSlug}`,
      data,
      extraOptions
    );
  }
}
