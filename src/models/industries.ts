export type IIndustries = {
  id?: any;
  name?: string;
  description?: string;
  slug?: string;
};
