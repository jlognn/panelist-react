export interface ILoggedInUser {
  avatarImage: string;
  name: string;
}
