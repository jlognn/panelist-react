import { ConnectSuggestion } from './connect-suggestions';

export interface ConnectSuggestionsResult {
  page: number;
  limit: number;
  totalRow: number;
  totalPage: number;
  data: ConnectSuggestion[];
}
