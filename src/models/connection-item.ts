export interface IConnectionItem {
  avatarUrl: string;
  name: string;
}
