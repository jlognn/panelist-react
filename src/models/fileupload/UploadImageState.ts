import { IUploadImageResult } from './UploadImageResult';

type Nullable<T> = T | null;

export interface IUploadImageState {
  value: Nullable<IUploadImageResult>;
  loading: boolean;
  errorMessage: string;
}
