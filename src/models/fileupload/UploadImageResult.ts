export interface IUploadImageResultAttributes {
  ETag: string;
  Location: string;
  key: string;
  Key: string;
  Bucket: string;
}

export interface IUploadImageResult {
  data: IUploadImageResultAttributes;
}
