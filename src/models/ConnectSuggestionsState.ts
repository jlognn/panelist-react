import { ConnectSuggestionsResult } from './ConnectSuggestionsResult';

type Nullable<T> = T | null;

export interface ConnectSuggestionsState {
  value: Nullable<ConnectSuggestionsResult>;
  loading: boolean;
  errorMessage: string;
}
