type Nullable<T> = T | null;

export interface IBaseState<T> {
  value: Nullable<T>;
  loading: boolean;
  errorMessage: string;
}
