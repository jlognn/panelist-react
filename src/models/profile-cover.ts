export interface IProfileCoverProps {
  name: string;
  coverPhotoUrl: string;
  avatarUrl: string;
  jobTitle: string;
  companyName: string;
  companyLogoUrl: string;
  location: string;
}
