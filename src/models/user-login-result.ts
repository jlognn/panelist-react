export interface UserLoginResult {
  token: string;
  refreshToken: string;
  janusToken: string;
}
