export interface Connection {
  fullName: string;
  id: string;
  firstName: string;
  lastName: string;
  email: string;
  slug: string;
  avatar: string;
  jobTitle: string;
  companyId: string;
  createdAt: string;
  connectionId: string;
}

export interface IConnectionsResult {
  data: Connection[];
}
