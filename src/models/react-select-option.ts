export interface IReactSelectOption {
  key: string;
  value: string;
  label: string;
}
