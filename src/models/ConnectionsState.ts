import { IConnectionsResult } from './connection';

type Nullable<T> = T | null;

export interface ConnectionsState {
  value: Nullable<IConnectionsResult>;
  loading: boolean;
  errorMessage: string;
}
