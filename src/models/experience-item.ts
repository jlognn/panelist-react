export interface IExperienceItem {
  logoUrl: string;
  jobTitle: string;
  companyName: string;
  period: string;
  country: string;
}
