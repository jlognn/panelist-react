import { Company } from './company';

export interface ConnectSuggestion {
  id: string;
  firstName: string;
  lastName: string;
  slug: string;
  avatar: string;
  cover: string;
  jobTitle: string;
  companyId: string;
  city: string;
  country: string;
  location: string;
  company: Company;
}
