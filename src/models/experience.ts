import { Company } from './company';

export interface Experience {
  id: string;
  jobTitle: string;
  jobFunctionId?: string;
  companyId: string;
  employmentType: string;
  location: string;
  startDate: string;
  endDate?: string;
  current: boolean;
  jobFunction?: any;
  company: Company;
}
