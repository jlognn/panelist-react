export interface Company {
  id: string;
  name: string;
  logo?: string;
  website?: string;
  slug: string;
  isPage: boolean;
}
