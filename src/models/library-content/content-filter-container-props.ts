import { Industry } from '../industry';
import { IJobFunction } from '../job-function';
import { IContentFormat } from './content-format';

export interface IContentFilterContainerProps {
  formats?: IContentFormat[];
  jobFunctions?: IJobFunction[];
  industries?: Industry[];
  regions?: any[];
}
