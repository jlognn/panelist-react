export interface IContentItemCardProps {
  logoUrl: string;
  companyName: string;
  industryName: string;
  readTime: string;
  coverPhotoUrl: string;
  title: string;
  summary: string;
  format: string;
}
