import { IRegion } from './region';

export interface IRegionResult {
  data: IRegion[];
}
