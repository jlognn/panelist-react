export interface IFilter {
  title: string;
  placeHolder: string;
  data: string | string[];
  open: boolean;
  keyEx?: string;
  key: string;
  dataKey?: string;
  type: number;
  name?: string;
  selectedItem?: any[];
}
