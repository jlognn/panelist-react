import { IBaseResult } from '../base-result';
import { ICompanyProfile } from '../company/company-profile';
import { IUserProfile } from '../user-profile';
import { IContent } from './content';

export interface IContentResult extends IBaseResult<IContent> {
  company?: ICompanyProfile;
  user?: IUserProfile;
}
