import { IBaseState } from '../base-state';
import { IRegionResult } from './RegionResult';

export interface IRegionState extends IBaseState<IRegionResult> {}
