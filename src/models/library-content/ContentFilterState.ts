import { IBaseState } from '../base-state';

export interface IContentFilterState extends IBaseState<string> {}
