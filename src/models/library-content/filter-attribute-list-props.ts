export interface IFilterAttributesListItem {
  id: string;
  name: string;
}

export interface IFilterAttributeListProps {
  title: string;
  placeHolderText?: string;
  items: IFilterAttributesListItem[];
}
