import { IBaseState } from '../base-state';
import { IContentResult } from './ContentResult';

export interface IContentState extends IBaseState<IContentResult> {}
