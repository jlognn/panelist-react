import { Industry } from '../industry';

interface ICompany {
  id: string;
  name: string;
  logo: string;
  cover: string;
  headquarter: string;
  slug: string;
  isPage: boolean;
  isFollowing: number;
  isCompanyAdmin: number;
  industry: Industry;
}

interface IUser {
  id: string;
  firstName: string;
  lastName: string;
  email: string;
  slug: string;
  avatar: string;
  cover?: string;
  jobTitle: string;
  companyId: string;
  connectionId: string;
  sentRequest: number;
  hasRequest: number;
  company: ICompany;
}

interface IPost {
  id: string;
  totalClaps: number;
  totalHahas: number;
  totalLikes: number;
  totalLoves: number;
  totalSmiles: number;
  totalComments: number;
  totalCommentsWithoutReplies: number;
}

export interface IContent {
  id: string;
  title: string;
  summary: string;
  cover: string;
  format: string;
  type: string;
  status: string;
  length?: string;
  slug: string;
  userId: string;
  companyId: string;
  eventId: string;
  postId: string;
  readTime: number;
  privacy: string;
  createdAt: string;
  updatedAt: string;
  user: IUser;
  company: ICompany;
  post: IPost;
}
