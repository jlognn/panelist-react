export interface IContentFormat {
  id: string;
  name: string;
}
