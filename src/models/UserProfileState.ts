import { IUserProfile } from './user-profile';

type Nullable<T> = T | null;

export interface UserProfileState {
  value: Nullable<IUserProfile>;
  loading: boolean;
  errorMessage: string;
}
