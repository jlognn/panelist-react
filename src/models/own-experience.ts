import { Experience } from './experience';

export interface OwnExperience {
  true: Experience[];
  false: Experience[];
}

export interface IOwnExperienceResult {
  data: OwnExperience;
}
