import { Company } from './company';
import { IIndustries } from './industries';
import { IJobFunction, JJobTitle } from './job-function';
import { IProfile } from './profile';
import { IUserProfile } from './user-profile';

export interface IEventInfo {
  attendeeStatus?: IAttendeeStatus;
  event?: IEvent;
  role?: IRole;
}

export interface IRole {
  admin?: boolean;
  host?: boolean;
  speaker?: boolean;
  sponsor?: boolean;
  audience?: IAttendeeStatus;
  audienceStatus?: any;
}

export interface IAttendeeStatus {
  audienceStatus?: string;
  status?: string;
  totalAttendees?: number;
  hasInvitation?: number;
}

export interface IEvent {
  id?: string;
  name?: string;
  slug?: string;
  duration?: string;
  region?: string;
  startTime?: any;
  endTime?: any;
  timezone?: string;
  cover?: string;
  logo?: string;
  status?: string;
  stage?: string;
  creator?: IUserProfile;
  company?: Company;
  eventIndustries?: IIndustries[];
  eventJobFunctions?: IJobFunction[];
  eventRegions?: any[];
  eventProgress?: number;
  title?: string; // for event library
  format?: string; // for event library
  summary?: string; // for event library
  length?: string; // for event library
  source?: string; // for event library
  createdAt?: string;
  type?: string;
  theme?: string;
  overview?: string;
  totalConnectionAttending?: number;
  eventKeyDiscussionPoints?: any[];
  eventMetrics?: any[];
  jobTitles?: JJobTitle[];
  jobFunctions?: IJobFunction[];
  industries?: IIndustries[];
  speakers?: IProfile[];
  eventSponsors?: IProfile[];
  rooms?: IRoom[];
  companySizes?: ICompanySize[];
  eventInvitations?: IEventInvitation[];
  audienceSize?: number;
  postId?: string;
  eventLinks?: IEventLink[];
  colorStyle?: IColorStyle;
  userId?: any;
  isShow?: any;
  totalAttendees?: any;
  relatedAttendees?: IProfile[];
}

export interface IColorStyle {
  id?: string;
  name?: string;
  ordering?: number;
  values?: {
    background?: string;
    backgroundButton?: string;
    colorButton?: string;
    heading?: string;
  };
}

export interface IEventLink {
  id?: string;
  key?: string;
  eventId?: string;
  createdAt?: string;
  metadata?: IMetaData[];
}

interface IMetaData {
  title?: string;
  url?: string;
  isShow?: boolean;
  icon?: any;
  body?: string;
}

export interface IRoom {
  id?: string;
  name?: string;
  type?: string;
  eventId?: string;
  ordering?: number;
  createdAt?: string;
  updatedAt?: string;
  sessions?: ISession[];
  isSelect?: boolean;
  isExpand?: boolean;
  timezone?: string;
}

export interface ISession {
  id?: string;
  title?: string;
  summary?: string;
  duration?: number;
  startTime?: string;
  endTime?: string;
  type?: string;
  format?: string;
  ordering?: number;
  eventSpeakers?: IEventSpeaker[];
  eventSponsors?: IEventSponsor[];
  isSelect?: boolean;
  isExpand?: boolean;
  isRewatch?: boolean;
}

export interface ICompanySize {
  id?: string;
  name?: string;
}

export interface IEventInvitation {
  id?: string;
  eventId?: string;
  inviteUserId?: string;
  status?: string;
  createdAt?: string;
  updateAt?: string;
}

export interface IAudience {
  id?: string;
  eventId?: string;
  userId?: string;
  status?: string;
  createdAt?: string;
  updateAt?: string;
}

export interface IEventSponsor {
  id?: string;
  company?: Company;
  sponsorship?: string; // another company name
  user?: IProfile;
}

export interface IEventSpeaker {
  id?: string;
  bio?: string;
  company: Company;
  ordering?: number;
  user?: IProfile;

  eventSessionSpeaker?: {
    id?: string;
    main?: boolean;
  };
}

export interface IRegistrationField {
  id?: string;
  eventId?: string;
  fieldName?: string;
  fieldType?: string;
  isRequired?: boolean; // 0 or 1
  ordering?: number;
  prefix?: string;
  value?: any;
  existed?: boolean;
  registrationOptionFields?: IRegistrationOptionField[];
}

export interface IRegistrationOptionField {
  id?: string;
  optionName?: string;
  registrationFieldId?: string;
}
