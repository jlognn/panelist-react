import { CheckUserEmailResult } from './CheckUserEmailResult';

type Nullable<T> = T | null;

export interface CheckUserEmailState {
  value: Nullable<CheckUserEmailResult>;
  loading: boolean;
  errorMessage: string;
}
