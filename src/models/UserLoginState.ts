import { UserLoginResult } from './user-login-result';

type Nullable<T> = T | null;

export interface IUserLoginState {
  isLoggedIn: boolean;
  value: Nullable<UserLoginResult>;
  loading: boolean;
  errorMessage: string;
}
