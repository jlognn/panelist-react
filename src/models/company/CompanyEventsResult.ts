import { IBaseResult } from '../base-result';
import { ICompanyEvent } from './company-event';

export interface ICompanyEventsResult extends IBaseResult<ICompanyEvent> {}
