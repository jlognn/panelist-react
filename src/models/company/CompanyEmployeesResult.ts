import { ICompanyEmployee } from './company-employee';

export interface ICompanyEmployeesResult {
  page: number;
  limit: number;
  totalRow: number;
  totalPage: number;
  data: ICompanyEmployee[];
}
