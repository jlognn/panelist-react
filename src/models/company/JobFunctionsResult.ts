import { IJobFunction } from '../job-function';

export interface IJobFunctionsResult {
  data: IJobFunction[];
}
