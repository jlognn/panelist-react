export interface ICompnayProfileUpdate {
  name: string;
  tagline: string;
  slug: string;
  description: string;
  specialities: string[];
  website: string;
  industryId: string;
  companySizeId: string;
  headquarter: string;
  logo: string;
  cover: string;
}
