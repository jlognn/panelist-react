interface ICreator {
  id: string;
  companyId: string;
}

export interface ICompanyResource {
  id: string;
  title: string;
  summary: string;
  cover?: string;
  region?: string;
  latitude?: string;
  longitude?: string;
  format: string;
  source: string;
  slug?: string;
  createdAt: string;
  userId: string;
  length: string;
  creator: ICreator;
  industries: any[];
  jobFunctions: any[];
}
