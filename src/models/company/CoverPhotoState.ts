type Nullable<T> = T | null;

export interface ICompanyCoverPhotoState {
  value: Nullable<string>;
  loading: boolean;
  errorMessage: string;
}
