import { IBaseResult } from '../base-result';
import { IPost } from './post';

export interface IPostsResult extends IBaseResult<IPost> {}
