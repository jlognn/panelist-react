interface ICreator {
  id: string;
  name: string;
  slug: string;
  avatar: string;
}

export interface ICommentedAsUser {
  id: string;
  firstName: string;
  lastName: string;
  avatar: string;
  cover: string;
  location: string;
  slug: string;
  jobTitle: string;
  companyId: string;
  connectionId?: string;
  sentRequest: number;
  hasRequest: number;
  company: any;
  sendConnectionRequests: any[];
}

export interface IComment {
  id: string;
  content: string;
  image: string;
  parentId: string;
  postId: string;
  userId: string;
  type: string;
  commentedAsId?: string;
  userMentioned?: string;
  createdAt: string;
  updatedAt: string;
  totalClaps: number;
  totalHahas: number;
  totalLikes: number;
  totalLoves: number;
  totalSmiles: number;
  totalComments: number;
  totalCommentsWithoutReplies: number;
  likes: any[];
  creator?: ICreator;
  commentedAsUser: ICommentedAsUser;
}
