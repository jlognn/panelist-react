import { Industry } from '../industry';

export interface IIndustriesResult {
  page: number;
  limit: number;
  totalRow: number;
  totalPage: number;
  data: Industry[];
}
