import { IIndustriesResult } from './IndustriesResult';

type Nullable<T> = T | null;

export interface IIndustriesState {
  value: Nullable<IIndustriesResult>;
  loading: boolean;
  errorMessage: string;
}
