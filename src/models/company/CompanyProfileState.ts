import { ICompanyProfile } from './company-profile';

type Nullable<T> = T | null;

export interface ICompanyProfileState {
  value: Nullable<ICompanyProfile>;
  loading: boolean;
  errorMessage: string;
}
