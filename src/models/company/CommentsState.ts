import { IBaseState } from '../base-state';
import { ICommentsResult } from './CommentsResult';

export interface ICommentsState extends IBaseState<ICommentsResult> {}
