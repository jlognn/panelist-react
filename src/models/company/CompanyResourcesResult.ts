import { IBaseResult } from '../base-result';
import { ICompanyResource } from './company-resource';

export interface ICompanyResourcesResult extends IBaseResult<ICompanyResource> {}
