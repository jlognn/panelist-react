import { ICompanyProfileUpdateResult } from './CompanyProfileUpdateResult';

type Nullable<T> = T | null;

export interface ICompanyProfileUpdateState {
  value: Nullable<ICompanyProfileUpdateResult>;
  loading: boolean;
  errorMessage: string;
}
