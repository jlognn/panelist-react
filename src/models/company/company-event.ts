import { ICompanyProfile } from './company-profile';

interface ICreator {
  id: string;
  firstName: string;
  lastName: string;
}

export interface ICompanyEvent {
  id: string;
  name: string;
  slug: string;
  theme: string;
  duration: string;
  startTime: string;
  endTime: string;
  timezone: string;
  cover: string;
  logo: string;
  status: string;
  stage: string;
  format: string;
  totalAttendees: number;
  totalConnectionAttending: number;
  isHosting: number;
  isponsoring: number;
  creator: ICreator;
  company: ICompanyProfile;
  relatedAttendess: any[];
  connectionAttendedIds: string[];
}
