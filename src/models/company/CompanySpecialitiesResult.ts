import { ICompanySpecialities } from './company-specialities';

export interface ICompanySpecialitiesResult {
  page: number;
  limit: number;
  totalRow: number;
  totalPage: number;
  data: ICompanySpecialities[];
}
