import { IBaseState } from '../base-state';
import { ICompanyEventsResult } from './CompanyEventsResult';

export interface ICompanyEventsState extends IBaseState<ICompanyEventsResult> {}
