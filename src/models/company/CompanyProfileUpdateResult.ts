import { ICompanyProfile } from './company-profile';

export interface ICompanyProfileUpdateResult {
  data: ICompanyProfile;
}
