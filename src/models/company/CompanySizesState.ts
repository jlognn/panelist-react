import { ICompanySizesResult } from './CompanySizesResult';

type Nullable<T> = T | null;

export interface ICompanySizesState {
  value: Nullable<ICompanySizesResult>;
  loading: boolean;
  errorMessage: string;
}
