import { ICompanyEmployeesResult } from './CompanyEmployeesResult';

type Nullable<T> = T | null;

export interface ICompanyEmployeesState {
  value: Nullable<ICompanyEmployeesResult>;
  loading: boolean;
  errorMessage: string;
}
