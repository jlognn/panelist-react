import { ICompanySize } from './company-size';

export interface ICompanySizesResult {
  page: number;
  limit: number;
  totalRow: number;
  totalPage: number;
  data: ICompanySize[];
}
