export interface ICompanyEmployee {
  fullName?: string;
  id: string;
  firstName: string;
  lastName: string;
  slug: string;
  avatar: string;
  jobTitle: string;
  companyId: string;
  createdAt: string;
}
