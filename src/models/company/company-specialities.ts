export interface ICompanySpecialities {
  id: string;
  name: string;
}
