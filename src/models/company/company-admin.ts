export interface ICompanyAdminType {
  type: string;
}

export interface ICompanyAdmin {
  id: string;
  firstName: string;
  lastName: string;
  email: string;
  avatar: string;
  slug: string;
  companyAdmin: ICompanyAdminType;
}
