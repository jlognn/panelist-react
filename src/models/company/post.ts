import { ICompanyProfile } from './company-profile';

interface ICreator {
  id: string;
  name: string;
  slug: string;
  avatar: string;
}

interface IWall {
  id: string;
  name: string;
  logo: string;
  slug: string;
}

export interface IPostComment {
  avatarUrl: string;
  name: string;
  jobTitle: string;
  companyName: string;
  timeAgo: string;
  commentText: string;
  groupIcons?: string;
  reactionsCount?: number;
}

export interface IPost {
  id: string;
  content: string;
  format: string;
  location: string;
  wallId: string;
  source: string;
  creatorId: string;
  totalShares: number;
  type: string;
  postedAsId: string;
  privacy: string;
  userMentioned?: string;
  createdAt: string;
  updatedAt: string;
  totalClaps: number;
  totalHahas: number;
  totalLikes: number;
  totalLoves: number;
  totalSmiles: number;
  totalComments: number;
  totalCommentsWithoutReplies: number;
  rank: number;
  likes: any[];
  creator?: ICreator;
  profileWall?: IWall;
  companyWall?: IWall;
  postedAsUser?: any;
  postedAsCompany: ICompanyProfile;
  comments: IPostComment[];
  poll?: any;
  event?: any;
  shareEvent?: any;
  libraryContent?: any;
  sharePost?: any;
}
