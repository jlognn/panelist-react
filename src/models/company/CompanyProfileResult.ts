import { ICompanyProfile } from './company-profile';

export interface ICompanyProfileResult {
  data: ICompanyProfile[];
}
