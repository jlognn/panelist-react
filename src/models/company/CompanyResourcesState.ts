import { IBaseState } from '../base-state';
import { ICompanyResourcesResult } from './CompanyResourcesResult';

export interface ICompanyResourcesState extends IBaseState<ICompanyResourcesResult> {}
