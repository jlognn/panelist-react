import { Industry } from '../industry';
import { ICompanyAdmin } from './company-admin';
import { ICompanySize } from './company-size';
import { ICompanySpecialities } from './company-specialities';

export interface ICompanyProfile {
  id: string;
  name: string;
  tagline?: string;
  description?: string;
  logo?: string;
  cover?: string;
  website?: string;
  industryId: string;
  companySizeId: string;
  headquarter: string;
  city?: string;
  suburb?: string;
  state?: string;
  country?: string;
  postCode?: string;
  slug: string;
  isPage: boolean;
  userId: string;
  latitude: string;
  longitude: string;
  createdAt: string;
  updatedAt: string;
  isFollowing: number;
  isAdmin: number;
  admins: ICompanyAdmin[];
  companySpecialities: ICompanySpecialities[];
  industry: Industry;
  companySize: ICompanySize;
}
