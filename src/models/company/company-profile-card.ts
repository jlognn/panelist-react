import { ICompanySpecialities } from './company-specialities';

export interface ICompanyProfileCardProps {
  logoUrl: string;
  companyName: string;
  industryName: string;
  headquarter: string;
  about: string;
  specialities: ICompanySpecialities[];
  size: string;
  employeesOnPanelist: string;
  website: string;
}
