import { IBaseState } from '../base-state';
import { IPostsResult } from './PostsResult';

export interface IPostsState extends IBaseState<IPostsResult> {}
