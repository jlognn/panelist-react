import { IBaseState } from '../base-state';
import { IJobFunctionsResult } from './JobFunctionsResult';

export interface IJobFunctionState extends IBaseState<IJobFunctionsResult> {}
