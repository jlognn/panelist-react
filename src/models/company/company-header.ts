export interface ICompanyHeader {
  id: string;
  cover: string;
  logo: string;
  name: string;
  isVerified: boolean;
  showPosts: boolean;
  showTechShows: boolean;
  showEvents: boolean;
  showResources: boolean;
  isViewingAsMember?: boolean;
}
