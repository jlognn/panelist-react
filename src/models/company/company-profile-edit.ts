import { ICompanyAdmin } from './company-admin';

export interface IEditCompanyProfile {
  name: string;
  tagline: string;
  slug: string;
  description: string;
  specialities: string[];
  website: string;
  industryId: string;
  companySizeId: string;
  headquarter: string;
  logo: string;
  cover: string;
  companyAdmin: ICompanyAdmin[];
}
