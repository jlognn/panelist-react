import { IBaseResult } from '../base-result';
import { IComment } from './comment';

export interface ICommentsResult extends IBaseResult<IComment> {}
