import { ICompanySpecialitiesResult } from './CompanySpecialitiesResult';

type Nullable<T> = T | null;

export interface ICompanySpecialitiesState {
  value: Nullable<ICompanySpecialitiesResult>;
  loading: boolean;
  errorMessage: string;
}
