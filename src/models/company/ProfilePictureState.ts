type Nullable<T> = T | null;

export interface IProfilePictureState {
  value: Nullable<string>;
  loading: boolean;
  errorMessage: string;
}
