import { IEventAgenda } from './event-agenda';

export interface IUpcomingEventAgendaResult {
  data: IEventAgenda[];
}
