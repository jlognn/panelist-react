import { ICompanyProfile } from '../company/company-profile';
import { IUserProfile } from '../user-profile';

export interface IEventSessionSpeaker {
  id: string;
  main: boolean;
  eventSpeaker: IEventSpeaker;
  createdAt: string;
}

export interface IEventSpeaker {
  id: string;
  userId: string;
  bio?: string;
  firstName: string;
  lastName: string;
  jobTitle: string;
  photo: string;
  companyId: string;
  eventId: string;
  ordering: number;
  createdAt: string;
  updatedAt: string;
  eventSessionSpeaker: IEventSessionSpeaker;
  user: IUserProfile;
  company: ICompanyProfile;
}
