export interface IEventHeader {
  id: string;
  logo?: string;
  cover?: string;
  eventName: string;
  theme: string;
  companyName: string;
  industryName: string;
  startDateTime: string;
  endDateTime: string;
  format: string;
  type: string;
  location: string;
}
