export interface IEventRegion {
  id: string;
  name: string;
}
