import { IBaseState } from '../base-state';
import { IUpcomingEventsSpeakerResult } from './UpcomingEventSpeakersResult';

export interface IUpcomingEventSpeakersState extends IBaseState<IUpcomingEventsSpeakerResult> {}
