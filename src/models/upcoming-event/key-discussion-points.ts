export interface IKeyDiscussionPoints {
  id: string;
  key: string;
  summary: string;
  ordering: number;
  eventId: string;
  createdAt: string;
  updatedAt: string;
}
