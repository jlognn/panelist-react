import { IAttendeeStatus } from './attendee-status';
import { IEvent } from './event';

interface IUpcomingEventData {
  event: IEvent;
  attendeesStatus: IAttendeeStatus;
}
export interface IUpcomingEventResult {
  data: IUpcomingEventData;
}
