export interface IEventCreator {
  id: string;
  firstName: string;
  lastName: string;
  avatar?: string;
  slug: string;
  email: string;
}
