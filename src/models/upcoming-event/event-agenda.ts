import { IEventSession } from './event-session';

export interface IEventAgenda {
  id: string;
  name: string;
  verticalOrdering: number;
  ordering: number;
  minTime: string;
  maxTime: string;
  sessions: IEventSession[];
}
