import { IBaseResult } from '../base-result';
import { IEventSpeakersData } from './event-speakers-data';

export interface IUpcomingEventsSpeakerResult extends IBaseResult<IEventSpeakersData> {}
