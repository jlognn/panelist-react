import { IBaseState } from '../base-state';
import { IUpcomingEventAgendaResult } from './UpcomingEventAgendaResult';

export interface IUpcomingEventAgendaState extends IBaseState<IUpcomingEventAgendaResult> {}
