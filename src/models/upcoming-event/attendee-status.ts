export interface IAttendeeStatus {
  totalAttendees: number;
  audienceStatus: string;
  hasInvitation: string;
}
