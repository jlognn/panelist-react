import { IEventSponsor } from '../user-events';
import { IEventSession } from './event-session';

export interface IRoom {
  id: string;
  name: string;
  type?: any;
  eventId: string;
  ordering: number;
  verticalOrdering: number;
  createdAt: string;
  updatedAt: string;
  sessions: IEventSession[];
  eventSponsors: IEventSponsor[];
}
