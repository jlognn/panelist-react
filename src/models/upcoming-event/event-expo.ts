export interface IEventExpo {
  id: string;
  expo: boolean;
  eventId: string;
  background?: any;
}
