export interface IEventLocationProps {
  latitude: string;
  longitude: string;
  title: string;
  description: string;
  address: string;
  notes: string;
}
