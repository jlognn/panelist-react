export interface IEventMetric {
  id: string;
  name: string;
  value: string;
  eventId: string;
  createdAt: string;
  updatedAt: string;
}
