import { ICompanyProfile } from '../company/company-profile';
import { IAttendeeStatus } from './attendee-status';
import { IEventCreator } from './event-creator';
import { IEventExpo } from './event-expo';
import { IEventLink } from './event-link';
import { IEventMetric } from './event-metric';
import { IEventPost } from './event-post';
import { IEventRegion } from './event-region';
import { IEventSponsor } from './event-sponsor';
import { IKeyDiscussionPoints } from './key-discussion-points';
import { IRoom } from './room';

export interface IEvent {
  completedAspects: string[];
  missingAspects: string[];
  eventProgress: string;
  id: string;
  name: string;
  slug: string;
  duration: string;
  audienceSize: number;
  region?: any;
  startTime: string;
  endTime: string;
  timezone: string;
  type: string;
  format: string;
  companyId: string;
  cover?: string;
  logo?: string;
  styleId?: string;
  theme: string;
  overview: string;
  keyDiscussionPoints?: string;
  attendeeCompanySizeId?: string;
  userId: string;
  location?: string;
  latitude: string;
  longitude: string;
  status: string;
  stage: string;
  postId: string;
  createdAt: string;
  updatedAt: string;
  eventExpo: IEventExpo;
  company: ICompanyProfile;
  post: IEventPost;
  creator: IEventCreator;
  eventSponsors: IEventSponsor[];
  rooms: IRoom[];
  eventRegions: IEventRegion[];
  eventMetrics: IEventMetric[];
  eventKeyDiscussionPoints: IKeyDiscussionPoints[];
  eventLinks: IEventLink[];
  attendeeStatus: IAttendeeStatus;
}
