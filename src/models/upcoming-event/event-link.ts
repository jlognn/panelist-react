export interface IMetadata {
  title: string;
  body: string;
  isShow: string;
}

export interface IEventLink {
  metadata: IMetadata[];
  id: string;
  eventId: string;
  key: string;
  createdAt: string;
  updatedAt: string;
}
