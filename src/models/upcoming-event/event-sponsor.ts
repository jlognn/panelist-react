import { ICompanyProfile } from '../company/company-profile';
import { IUserProfile } from '../user-profile';

export interface ISponsorVisibility {
  attendees: boolean;
  ads_report: boolean;
  sessions: boolean;
  content_report: boolean;
  expo_report: boolean;
}

export interface IEventSponsor {
  visibility: ISponsorVisibility;
  id: string;
  userId?: string;
  bio: string;
  logo?: string;
  companyId: string;
  sponsorship?: any;
  eventId: string;
  ordering: number;
  createdBy: string;
  createdAt: string;
  updatedAt: string;
  users: IUserProfile[];
  company: ICompanyProfile;
}
