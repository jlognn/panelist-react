import { IKeyDiscussionPoints } from './key-discussion-points';

export interface IAboutEventProps {
  eventName: string;
  eventTheme: string;
  overview: string;
  keyDiscussionPoints: IKeyDiscussionPoints[];
  attendingJobTitles?: string[];
  attendingJobIndustries?: string[];
}
