import { Company } from '../company';
import { IEventSpeaker } from './event-speaker';

export interface IEventSpeakersData {
  id: string;
  firstName: string;
  lastName: string;
  email: string;
  avatar?: string;
  cover?: string;
  jobTitle: string;
  companyId: string;
  city: string;
  state: string;
  country: string;
  location: string;
  status: string;
  slug: string;
  connectionId?: string;
  sentRequest: number;
  hasRequest: number;
  sendConnectionRequests: string[];
  company: Company;
  eventSpeakers: IEventSpeaker[];
}

export interface IEventSpeakers {
  data: IEventSpeakersData[];
  eventSpeakers: IEventSpeaker[];
}
