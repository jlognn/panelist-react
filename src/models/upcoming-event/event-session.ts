import { IEventSessionSpeaker } from './event-speaker';
import { IEventSponsor } from './event-sponsor';

export interface IEventSession {
  id: string;
  title: string;
  summary: string;
  duration: number;
  startTime: string;
  endTime: string;
  type: string;
  format: string;
  ordering: number;
  edited?: any;
  uploaded?: any;
  eventSessionSpeakers: IEventSessionSpeaker[];
  eventSessionSponsors: IEventSponsor[];
  eventSessionVideos: any[];
}
