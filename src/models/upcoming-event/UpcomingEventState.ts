import { IBaseState } from '../base-state';
import { IUpcomingEventResult } from './UpcomingEventResult';

export interface IUpcomingEventState extends IBaseState<IUpcomingEventResult> {}
