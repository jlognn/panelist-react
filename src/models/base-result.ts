export interface IBaseResult<T> {
  page: number;
  limit: number;
  totalRow: number;
  totalPage: number;
  data: T[];
}
