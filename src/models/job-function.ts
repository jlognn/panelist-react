export interface IJobFunction {
  id: string;
  name: string;
}

export interface JJobTitle {
  id?: string;
  name?: string;
}
