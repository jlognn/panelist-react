import { Company } from './company';
import { Industry } from './industry';
import { OffersAndInterests } from './offers-and-interests';

export interface IUserProfile {
  fullName: string;
  id: string;
  email: string;
  phone?: string;
  type: string;
  status: string;
  firstName: string;
  lastName: string;
  slug: string;
  avatar?: string;
  cover?: string;
  about?: string;
  city: string;
  state: string;
  country: string;
  location: string;
  latitude: string;
  longitude: string;
  postCode: string;
  isEmailVerified: boolean;
  jobTitle: string;
  companyId: string;
  freeAttempts: number;
  hostAttempts: number;
  limitHostAttempts: boolean;
  hasSubscription: boolean;
  currentSubscriptionId?: string;
  jobFunctionId?: string;
  industryId: string;
  lastLoggedIn?: string;
  joinedAt: string;
  createdAt: string;
  updatedAt: string;
  connectionId?: string;
  sentRequest: number;
  hasRequest: number;
  company: Company;
  interestJobFunctions: OffersAndInterests[];
  interestIndustries: OffersAndInterests[];
  companyAdmins: string[];
  jobFunction?: string;
  industry: Industry;
  sendConnectionRequests: string[];
}

export interface IUserProfileResult {
  data: IUserProfile;
}
