export interface IPeopleInYourNetworkProp {
  name: string;
  jobTitle: string;
  companyName: string;
  avatarImage: string;
}
