import { IEvent } from './user-events';

export interface UserEventsResult {
  page: number;
  limit: number;
  totalRow: number;
  totalPage: number;
  data: IEvent[];
}
