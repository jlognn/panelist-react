import { IEvent } from './user-events';

export interface RecommendedEventsResult {
  page: number;
  limit: number;
  totalRow: number;
  totalPage: number;
  data: IEvent[];
}
