import { UserEventsResult } from './UserEventsResult';

type Nullable<T> = T | null;

export interface UserEventsState {
  value: Nullable<UserEventsResult>;
  loading: boolean;
  errorMessage: string;
}
