import { OwnExperience } from './own-experience';

type Nullable<T> = T | null;

export interface OwnExperienceState {
  value: Nullable<OwnExperience>;
  loading: boolean;
  errorMessage: string;
}
