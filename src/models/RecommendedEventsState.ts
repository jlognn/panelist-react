import { RecommendedEventsResult } from './RecommendedEventsResult';

type Nullable<T> = T | null;

export interface RecommendedEventsState {
  value: Nullable<RecommendedEventsResult>;
  loading: boolean;
  errorMessage: string;
}
