import { Company } from './company';
import { IIndustries } from './industries';
import { IJobFunction } from './job-function';

export interface IProfile {
  fullName?: string;
  id?: string;
  email?: string;
  phone?: string;
  type?: string;
  status?: string;
  firstName?: string;
  lastName?: string;
  slug?: string;
  avatar?: string;
  cover?: string;
  about?: string;
  location?: string;
  city?: string;
  state?: string;
  country?: string;
  postCode?: string;
  latitude?: string;
  longitude?: string;
  isEmailVerified?: boolean;
  jobTitle?: string;
  companyId?: string;
  freeAttempts?: number;
  hasSubscription?: boolean;
  jobFunctionId?: string;
  lastLoggedIn?: string;
  joinedAt?: string;
  createdAt?: string;
  updatedAt?: string;
  company?: Company;
  interestIndustries?: IIndustries;
  industryId?: string;
  industry?: any;
  companyAdmins?: Company[];
  logo?: string;
  password?: string;
  confirmPassword?: string;
  phoneCode?: {}; // fe custom
  isConnection?: number;
  sentRequest?: number;
  hasRequest?: number;
  connectionId?: any;
  privacySettings?: any;
  hasConnection?: any;
  jobFunction?: any;
  eventSpeakers?: any;
  sponsorship?: any;
  sendConnectionRequests?: any;
  send?: boolean;
  accept?: boolean;
  reject?: boolean;
}

export interface IExperirence {
  id?: string;
  jobTitle?: string;
  jobFunctionId?: string;
  companyId?: string;
  employmentType?: any;
  location?: string;
  startDate?: any;
  endDate?: any;
  current?: boolean;
  createdAt?: string;
  updatedAt?: string;
  userId?: string;
  jobFunction?: IJobFunction;
  company?: Company;
}

export interface INotificationSettings {
  id?: string;
  activity?: string;
  permit?: boolean;
  userId?: string;
}

export interface IPrivacySettings {
  id?: string;
  defaultValues?: any[];
  can?: string;
  permit?: string;
  userId?: string;
}
