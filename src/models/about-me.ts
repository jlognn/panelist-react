import { OffersAndInterests } from './offers-and-interests';

export interface IAboutMeProps {
  location: string;
  industry: string;
  offers: OffersAndInterests[];
  interests: OffersAndInterests[];
}
