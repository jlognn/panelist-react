import { useEffect } from 'react';

import { useDispatch, useSelector } from 'react-redux';

import defaultImage from '../../../assets/images/default-image.svg';
import { IAppState } from '../../../store';
import { getContent, loadContent } from '../../../store/library-content/actions';
import { ContentItem } from './content-item';

export const ContentContainer = () => {
  const { libraryContent, contentFilter } = useSelector((state: IAppState) => state);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(loadContent(true));
    dispatch(getContent(contentFilter.value));
  }, [dispatch, contentFilter.value]);

  const stripQuotesAndBraces = (value: string) => value.replace(/[&\\#,+()[\]$~%.'":*?<>{}]/g, ' ');

  return (
    <div className="bg-white rounded-lg w-858 px-4 py-4">
      <div className="flex flex-col">
        <div className="text-md2 font-medium leading-5">All content</div>
        <div className="flex flex-row mt-1">
          <div>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="654.187"
              height="1"
              viewBox="0 0 654.187 1"
            >
              <path
                id="Path_10882"
                data-name="Path 10882"
                d="M0,0H654.187"
                transform="translate(0 0.5)"
                fill="none"
                stroke="#707070"
                strokeWidth="1"
                opacity="0.28"
              />
            </svg>
          </div>
          {/* Put the sort by dropdown here */}
        </div>
        <div className="flex flex-row flex-wrap mt-1">
          {libraryContent.value &&
            libraryContent.value.data.map(
              (content, index) =>
                content.company && (
                  <div key={`content-${index}`}>
                    <ContentItem
                      logoUrl={
                        (content.company !== null
                          ? `${process.env.REACT_APP_IMAGE_URL_PREFIX_STAGING}/${content.company?.logo}`
                          : process.env.REACT_APP_NO_COMPANY_LOGO_URL) ?? ''
                      }
                      companyName={content.company ? content.company.name : ''}
                      industryName={
                        content.company && content.company.industry
                          ? content.company.industry.name
                          : ''
                      }
                      readTime={content.readTime ? content.readTime.toString() : ''}
                      coverPhotoUrl={content.cover ?? defaultImage}
                      title={content.title ?? ''}
                      summary={content.summary}
                      format={stripQuotesAndBraces(content.format)}
                    />
                  </div>
                )
            )}
        </div>
      </div>
    </div>
  );
};
