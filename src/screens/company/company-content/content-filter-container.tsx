import { useDispatch, useSelector } from 'react-redux';

import { IContentFilterContainerProps } from '../../../models/library-content/content-filter-container-props';
import { IAppState } from '../../../store';
import { setContentFilter } from '../../../store/content-filter/action';
import { FilterAttributeList } from './filter-attribute-list';

export const ContentFilterContainer = (props: IContentFilterContainerProps) => {
  const dispatch = useDispatch();
  const { contentFilter } = useSelector((state: IAppState) => state);

  const search = (key: string, value: string) => {
    let currentFilter = contentFilter.value;

    if (currentFilter === '' || currentFilter === null) currentFilter = 'page=1&limit=10';

    currentFilter += `&${key}=${value}`;

    dispatch(setContentFilter(currentFilter));
  };

  return (
    <div
      className="flex flex-col w-250 rounded-lg px-4 py-4 bg-white ml-8"
      style={{
        boxShadow: '1px 1px 4px 0 rgba(0, 0, 0, 0.1)'
      }}
    >
      <div className="flex flex-row justify-between">
        <div className="text-md text-blue-2 font-semibold">Filter your search</div>
        <div className="text-md text-blue-1 font-light">Clear all</div>
      </div>
      <div className="border-1 border-blue-0 bg-blue-0 rounded-full mt-2">
        <div className="relative">
          <span className="absolute inset-y-0 left-0 flex items-center pl-2">
            <svg
              fill="none"
              stroke="currentColor"
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth="2"
              viewBox="0 0 24 24"
              className="w-6 h-6"
            >
              <path d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"></path>
            </svg>
          </span>
          <input
            type="search"
            name="q"
            className="py-2 text-sm bg-blue-0 rounded-full pl-10 focus:outline-none"
            placeholder="Enter a keyword..."
            onChange={(e) => search('search', e.target.value)}
          />
        </div>
      </div>
      <div className="mt-4">
        <svg xmlns="http://www.w3.org/2000/svg" width="212.51" height="1" viewBox="0 0 212.51 1">
          <line
            id="Line_146"
            data-name="Line 146"
            x2="211.51"
            transform="translate(0.5 0.5)"
            fill="none"
            stroke="#cddcf2"
            strokeLinecap="round"
            strokeWidth="1"
          />
        </svg>
      </div>
      <div className="mt-4">
        <FilterAttributeList
          title="Format"
          placeHolderText="Select format"
          items={props.formats ?? []}
        />
      </div>
      <div className="mt-2">
        <FilterAttributeList
          title="Function"
          placeHolderText="Select job functions"
          items={props.jobFunctions ?? []}
        />
      </div>
      <div className="mt-2">
        <FilterAttributeList
          title="Industry"
          placeHolderText="Search for an industry"
          items={props.industries ?? []}
        />
      </div>
      <div className="mt-2">
        <FilterAttributeList
          title="Regions"
          placeHolderText="Add locations"
          items={props.regions ?? []}
        />
      </div>
    </div>
  );
};

export default ContentFilterContainer;
