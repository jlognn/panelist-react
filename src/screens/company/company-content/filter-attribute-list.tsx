import { useEffect, useState } from 'react';

import { useDispatch, useSelector } from 'react-redux';

import { Industry } from '../../../models/industry';
import { IJobFunction } from '../../../models/job-function';
import { IContentFormat } from '../../../models/library-content/content-format';
import { IFilterAttributeListProps } from '../../../models/library-content/filter-attribute-list-props';
import { IRegion } from '../../../models/library-content/region';
import { IAppState } from '../../../store';
import { setContentFilter } from '../../../store/content-filter/action';

export const FilterAttributeList = (props: IFilterAttributeListProps) => {
  const [selectedFormats, setSelectedFormats] = useState<IContentFormat[]>([]);
  const [selectedJobFunctions, setSelectedJobFunctions] = useState<IJobFunction[]>([]);
  const [selectedIndustries, setSelectedIndustries] = useState<Industry[]>([]);
  const [selectedRegions, setSelectedRegions] = useState<IRegion[]>([]);

  const [formats, setFormats] = useState<IContentFormat[]>([]);
  const [jobFunctions, setJobFunctions] = useState<IJobFunction[]>([]);
  const [industries, setIndustries] = useState<Industry[]>([]);
  const [regions, setRegions] = useState<IRegion[]>([]);

  const dispatch = useDispatch();
  const { contentFilter } = useSelector((state: IAppState) => state);

  useEffect(() => {
    props.title === 'Format' && setFormats(props.items);
    props.title === 'Function' && setJobFunctions(props.items);
    props.title === 'Industry' && setIndustries(props.items);
    props.title === 'Regions' && setRegions(props.items);
  }, [setFormats, setJobFunctions, setIndustries, setRegions, props]);

  const filterFormats = (searchString: string) => {
    if (searchString.trim() !== '') {
      const filteredFormats = formats.filter((f) => {
        return f.name.toLowerCase().startsWith(searchString.toLowerCase());
      });
      setFormats(filteredFormats);
    } else {
      setFormats(props.items);
    }
  };

  const filterJobFunctions = (searchString: string) => {
    if (searchString.trim() !== '') {
      const filteredJobFunctions = jobFunctions.filter((f) => {
        return f.name.toLowerCase().startsWith(searchString.toLowerCase());
      });
      setJobFunctions(filteredJobFunctions);
    } else {
      setJobFunctions(props.items);
    }
  };

  const filterIndustries = (searchString: string) => {
    if (searchString.trim() !== '') {
      const filteredIndustries = industries.filter((f) => {
        return f.name.toLowerCase().startsWith(searchString.toLowerCase());
      });
      setIndustries(filteredIndustries);
    } else {
      setIndustries(props.items);
    }
  };

  const filterRegions = (searchString: string) => {
    if (searchString.trim() !== '') {
      const filteredRegions = regions.filter((f) => {
        return f.name.toLowerCase().startsWith(searchString.toLowerCase());
      });
      setRegions(filteredRegions);
    } else {
      setRegions(props.items);
    }
  };

  const search = (key: string, value: string) => {
    let currentFilter = contentFilter.value;

    if (currentFilter === '' || currentFilter === null) currentFilter = 'page=1&limit=10';

    currentFilter += `&${key}=${value}`;

    dispatch(setContentFilter(currentFilter));
  };

  return (
    <div className="flex flex-col mb-2">
      <div className="flex flex-row justify-between mt-2">
        <div className="text-h2 text-blue-1 font-medium">{props.title}</div>
        <div className="text-h2 text-blue-1 font-medium mr-1">➖</div>
      </div>
      <div className="">
        <input
          type="text"
          className="border-b-1 border-blue-0 w-full text-sm text-gray-7 focus:outline-none focus:text-gray-7"
          placeholder={props.placeHolderText}
          onChange={(e) => {
            props.title === 'Format' && filterFormats(e.target.value);
            props.title === 'Function' && filterJobFunctions(e.target.value);
            props.title === 'Industry' && filterIndustries(e.target.value);
            props.title === 'Regions' && filterRegions(e.target.value);
          }}
        />
      </div>
      <div className="flex flex-row flex-wrap mt-1">
        {selectedFormats &&
          selectedFormats.map((format) => (
            <div
              className="flex flex-row justify-between bg-blue-1 text-white text-sm4 font-extralight rounded-lg px-4 mr-1 mb-1 p-0.5 w-122"
              key={format.id}
            >
              <div className="">{format.name}</div>
              <div
                className="ml-3 font-bold cursor-pointer"
                onClick={() => {
                  setSelectedFormats(selectedFormats.filter((f) => f.name !== format.name));
                }}
              >
                X
              </div>
            </div>
          ))}
        {selectedJobFunctions &&
          selectedJobFunctions.map((jobFunction) => (
            <div
              className="flex flex-row justify-between bg-blue-1 text-white text-sm4 font-extralight rounded-lg px-4 mr-1 mb-1 p-0.5 w-122"
              key={jobFunction.id}
            >
              <div className="">{jobFunction.name}</div>
              <div
                className="ml-3 font-bold cursor-pointer"
                onClick={() => {
                  setSelectedJobFunctions(
                    selectedJobFunctions.filter((f) => f.name !== jobFunction.name)
                  );
                }}
              >
                X
              </div>
            </div>
          ))}
        {selectedIndustries &&
          selectedIndustries.map((industry) => (
            <div
              className="flex flex-row justify-between bg-blue-1 text-white text-sm4 font-extralight rounded-lg px-4 mr-1 mb-1 p-0.5 w-122"
              key={industry.id}
            >
              <div className="">{industry.name}</div>
              <div
                className="ml-3 font-bold cursor-pointer"
                onClick={() => {
                  setSelectedIndustries(selectedIndustries.filter((f) => f.name !== industry.name));
                }}
              >
                X
              </div>
            </div>
          ))}
        {selectedRegions &&
          selectedRegions.map((region) => (
            <div
              className="flex flex-row justify-between bg-blue-1 text-white text-sm4 font-extralight rounded-lg px-4 mr-1 mb-1 p-0.5 w-122"
              key={region.name}
            >
              <div className="">{region.name}</div>
              <div
                className="ml-3 font-bold cursor-pointer"
                onClick={() => {
                  setSelectedRegions(selectedRegions.filter((f) => f.name !== region.name));
                }}
              >
                X
              </div>
            </div>
          ))}
      </div>
      <div className="mt-2">
        <div className="flex flex-col overflow-y-auto h-130">
          {formats &&
            formats.map((item, index) => (
              <div
                key={`${props.title}-${index}`}
                className="text-sm2 text-blue-1 cursor-pointer"
                onClick={() => {
                  props.title === 'Format' &&
                    !selectedFormats.includes(item) &&
                    setSelectedFormats((oldValue) => [...oldValue, item]);
                }}
              >
                {item.name}
              </div>
            ))}
          {jobFunctions &&
            jobFunctions.map((item, index) => (
              <div
                key={`${props.title}-${index}`}
                className="text-sm2 text-blue-1 cursor-pointer"
                onClick={() => {
                  props.title === 'Function' &&
                    !selectedJobFunctions.includes(item) &&
                    setSelectedJobFunctions((oldValue) => [...oldValue, item]);
                  // dispatch(setContentFilter(`&where[industryId]=${item.id}`));
                  // console.log(contentFilter);
                  search('where[jobFunctionId]', item.id);
                }}
              >
                {item.name}
              </div>
            ))}
          {industries &&
            industries.map((item, index) => (
              <div
                key={`${props.title}-${index}`}
                className="text-sm2 text-blue-1 cursor-pointer"
                onClick={() => {
                  props.title === 'Industry' &&
                    !selectedIndustries.includes(item) &&
                    setSelectedIndustries((oldValue) => [...oldValue, item]);
                  search('where[industryId]', item.id);
                }}
              >
                {item.name}
              </div>
            ))}
          {regions &&
            regions.map((item, index) => (
              <div
                key={`${props.title}-${index}`}
                className="text-sm2 text-blue-1 cursor-pointer"
                onClick={() => {
                  props.title === 'Regions' &&
                    !selectedRegions.includes(item) &&
                    setSelectedRegions((oldValue) => [...oldValue, item]);
                  search('where[region]', item.name);
                }}
              >
                {item.name}
              </div>
            ))}
        </div>
      </div>
      <div className="mt-1">
        <svg xmlns="http://www.w3.org/2000/svg" width="212.51" height="1" viewBox="0 0 212.51 1">
          <line
            id="Line_146"
            data-name="Line 146"
            x2="211.51"
            transform="translate(0.5 0.5)"
            fill="none"
            stroke="#cddcf2"
            strokeLinecap="round"
            strokeWidth="1"
          />
        </svg>
      </div>
    </div>
  );
};

export default FilterAttributeList;
