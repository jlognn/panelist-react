import { IContentItemCardProps } from '../../../models/library-content/content-item-card-props';

export const ContentItem = (content: IContentItemCardProps) => {
  return (
    <div className="bg-blue-0 w-182 rounded-lg px-2 py-2 m-2">
      <div className="flex flex-col p-1">
        <div className="flex flex-row">
          <div className="mr-2">
            <img src={content.logoUrl} className="w-26 h-26" alt="" />
          </div>
          <div className="flex flex-col">
            <div className="text-h3 text-gray-8 font-medium">{content.companyName}</div>
            <div className="text-x-sm text-gray-8 font-light overflow-hidden text-ellipsis">
              {content.industryName}
            </div>
            <div className="text-x-sm text-gray-8 font-light">{content.readTime} min</div>
          </div>
        </div>
        <div className="m-1">
          <img src={content.coverPhotoUrl} className="rounded-lg w-169 h-86" alt="" />
        </div>
        <div className="flex flex-row">
          <div className="text-h3 text-gray-8 font-semibold text-ellipsis overflow-hidden">
            {content.title}
          </div>
          <div className="w-26 h-26 bg-white rounded-full">
            <span className="left-0 top-0">...</span>
          </div>
        </div>
        <div className="text-x-sm font-light text-gray-8">{content.summary}</div>
        <div className="text-2x-sm font-light text-gray-8">
          {`${content.format} . ${content.readTime} min`}
        </div>
      </div>
    </div>
  );
};
