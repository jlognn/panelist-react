import { useEffect, useState } from 'react';

import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';

import { UserNavbar } from '../../../components';
import { IContentFormat } from '../../../models/library-content/content-format';
import { IAppState } from '../../../store';
import { getIndustries } from '../../../store/company/industries/actions';
import { getJobFunction } from '../../../store/company/job-functions/actions';
import { getCompanyProfilePreview } from '../../../store/company/profile-preview/actions';
import { getRegions } from '../../../store/regions/actions';
import CompanyHeader from '../company-header';
import { ContentContainer } from './content-container';
import ContentFilterContainer from './content-filter-container';

export const CompanyContent = () => {
  const formats: IContentFormat[] = [
    {
      id: 'case-study',
      name: 'Case Study'
    },
    {
      id: 'ebook',
      name: 'eBook'
    },
    {
      id: 'whitepaper',
      name: 'White Paper'
    },
    {
      id: 'webinar',
      name: 'Webinar'
    },
    {
      id: 'article',
      name: 'Article'
    },
    {
      id: 'video',
      name: 'Video'
    }
  ];

  const params = useParams();
  const companyId = Object.values(params)[0];
  const { jobFunctions, industries, regions, companyProfilePreview } = useSelector(
    (state: IAppState) => state
  );
  const dispatch = useDispatch();
  const [regionsList, setRegionsList] = useState<any[]>([]);

  useEffect(() => {
    dispatch(getCompanyProfilePreview(companyId));
    dispatch(getJobFunction());
    dispatch(getIndustries());
    dispatch(getRegions());

    const contentRegions: any[] = [];
    regions.value?.data.map((r) => contentRegions.push({ id: r.name, name: r.name }));
    setRegionsList(contentRegions);
  }, [dispatch, companyId, regions]);

  return (
    <div className="bg-gray-1">
      <UserNavbar />
      <div className="container mx-auto">
        <div className="flex flex-col">
          <div className="ml-2">
            {/* header goes here */}
            <CompanyHeader
              id={companyProfilePreview.value?.id!}
              cover={companyProfilePreview.value?.cover!}
              logo={companyProfilePreview.value?.logo!}
              name={companyProfilePreview.value?.name!}
              isVerified={true}
              showPosts={true}
              showTechShows={false}
              showEvents={true}
              showResources={true}
            />
          </div>
          <div className="flex flex-row px-4 py-4">
            <div>
              <ContentFilterContainer
                formats={formats}
                jobFunctions={jobFunctions.value?.data}
                industries={industries.value?.data}
                regions={regionsList}
              />
            </div>
            <div className="ml-4">
              <ContentContainer />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default CompanyContent;
