import { useEffect, useState } from 'react';

import { format } from 'date-fns';
import { parseISO } from 'date-fns/fp';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';

import { CardMainPost, CardYourEvents } from '@panelist/panelist-components-beta';

import commentIcon from '../../../assets/images/comment-icon.png';
import emojiIcon from '../../../assets/images/emoji-smile-small-icon.png';
import groupIcon from '../../../assets/images/group-icon.png';
import likeIcon from '../../../assets/images/like-icon.png';
import imageIcon from '../../../assets/images/post-small-icon.png';
import shareIcon from '../../../assets/images/share-icon.png';
import { UserNavbar } from '../../../components';
import { Connection } from '../../../models/connection';
import { IAppState } from '../../../store';
import { getCompanyEmployees } from '../../../store/company/company-employees/actions';
import { getCompanyEvents } from '../../../store/company/company-events/actions';
import { getCompanyPosts } from '../../../store/company/company-posts/actions';
import { getCompanyResources } from '../../../store/company/company-resources/actions';
import { getCompanyProfilePreview } from '../../../store/company/profile-preview/actions';
import Connections from '../../user/user-profile/connections';
import CompanyHeader from '../company-header';
import CompanyProfileCard from '../company-profile/company-profile-card';

const CompanyResource = (props: {
  thumbnailUrl: string;
  title: string;
  description: string;
  footerText: string;
}) => (
  <div className="flex flex-row px-2 py-1 mb-2">
    <div className="">
      <img
        className="w-55 h-55"
        src={`${process.env.REACT_APP_IMAGE_URL_PREFIX_STAGING}/${props.thumbnailUrl}`}
        alt=""
      />
    </div>
    <div className="flex flex-col ml-2 -mt-2">
      <div className="text-x-sm text-blue-3 font-semibold leading-6">{props.title}</div>
      <div className="text-2x-sm text-gray-1 font-light leading-5">{props.description}</div>
      <div className="text-2x-sm text-gray-1 font-medium leading-5">{props.footerText}</div>
    </div>
  </div>
);

export const CompanyViewAsVisitorPage = (_companyIdOrSlug: any) => {
  const companyId = Object.values(_companyIdOrSlug)[0];

  const dispatch = useDispatch();
  const { companyProfilePreview, companyEmployees, posts, companyEvents, companyResources } =
    useSelector((state: IAppState) => state);

  const [companyEmployeesData, setCompanyEmployeesData] = useState<Connection[]>();

  useEffect(() => {
    const connections: Connection[] = [];
    dispatch(getCompanyProfilePreview(companyId));
    dispatch(getCompanyEmployees(companyId));

    companyEmployees.value?.data &&
      companyEmployees.value?.data.map((emp) =>
        connections.push({
          fullName: emp.fullName || `${emp.firstName} ${emp.lastName}`,
          email: '',
          id: emp.id,
          firstName: emp.firstName,
          lastName: emp.lastName,
          slug: emp.slug,
          avatar: emp.avatar,
          jobTitle: emp.jobTitle,
          companyId: emp.companyId,
          createdAt: emp.createdAt,
          connectionId: ''
        })
      );

    setCompanyEmployeesData(connections);

    dispatch(getCompanyPosts(companyId));
    dispatch(getCompanyEvents(companyId));
    dispatch(getCompanyResources(companyId));
  }, [dispatch, companyEmployees, companyId]);

  return (
    <div className="bg-gray-1">
      <UserNavbar />
      <div className="container mx-auto">
        <div className="flex flex-col">
          <div>
            <CompanyHeader
              id={companyProfilePreview.value?.id!}
              cover={companyProfilePreview.value?.cover!}
              logo={companyProfilePreview.value?.logo!}
              name={companyProfilePreview.value?.name!}
              isVerified={true}
              showPosts={true}
              showTechShows={true}
              showEvents={true}
              showResources={true}
            />
          </div>
        </div>
        <div className="flex flex-row px-4 py-4">
          <div className="flex flex-col">
            <div className="bg-white w-315 h-575 ml-6 rounded-lg">
              <CompanyProfileCard
                logoUrl={companyProfilePreview.value?.logo!}
                companyName={companyProfilePreview.value?.name!}
                industryName={companyProfilePreview.value?.industry.name!}
                headquarter={companyProfilePreview.value?.headquarter!}
                about={companyProfilePreview.value?.description!}
                specialities={companyProfilePreview.value?.companySpecialities!}
                size={companyProfilePreview.value?.companySize.name!}
                employeesOnPanelist={'47'}
                website={companyProfilePreview.value?.website!}
              />
            </div>
            <div className="bg-white w-315 ml-6 rounded-lg mt-4">
              <Connections connections={companyEmployeesData!} title="Employees" />
            </div>
          </div>

          {/* cards */}
          <div className="ml-2">
            {posts.value?.data &&
              posts.value?.data.map((post, index) => (
                <div key={`event-${index}`} className="mb-4 bg-white rounded-lg">
                  <CardMainPost
                    imageUrl={post.companyWall?.logo!}
                    avatarUrl={post.creator?.avatar!}
                    groupIcons={groupIcon}
                    likeIcon={likeIcon}
                    commentIcon={commentIcon}
                    shareIcon={shareIcon}
                    title={post.companyWall?.name!}
                    subTitle={''}
                    dateAndTime={format(parseISO(post.createdAt), 'dd MMM yyyy HH:mm aaaa')}
                    eventType={post.type}
                    placeHolder={post.content}
                    commentsTally={post.totalComments.toString()}
                    detailsHref={''}
                    comments={post.comments}
                    emojiIcon={emojiIcon}
                    imageIcon={imageIcon}
                  />
                </div>
              ))}
          </div>
          {/* events */}
          <div className="ml-1 flex flex-col">
            <div className="bg-white rounded-lg w-288 px-2 py-2">
              <div className="text-h2 text-blue-3 px-2 py-1">
                {`${companyProfilePreview.value?.name!}'s events`}
              </div>
              <div className="bg-white mt-1">
                {companyEvents.value?.data &&
                  companyEvents.value?.data.map((event, index) => (
                    <div key={`your-event-${index}`} className="mb-2">
                      <CardYourEvents
                        avatarUrl={event.logo!}
                        dotIconUrl={''}
                        groupIconSmallUrl={event.logo!}
                        title={event.name!}
                        subTitle={''}
                        date={format(parseISO(event.startTime!), 'dd MMM yyyy')}
                        time={format(parseISO(event.startTime!), 'HH:mm aaaa')}
                        contactsGoing={event.totalAttendees.toString()}
                        eventType={event.format}
                        detailsHref={`/event-details/${event.slug!}`}
                      />
                    </div>
                  ))}
              </div>
            </div>
            <div className="bg-white rounded-lg w-288 px-2 py-2 mt-2">
              <div className="text-h2 text-blue-3 px-2 py-1">
                {`${companyProfilePreview.value?.name!}'s resources`}
              </div>
              <div className="bg-white mt-1">
                {companyResources.value?.data &&
                  companyResources.value.data.map((resource, index) => (
                    <div key={`resource-${index}`}>
                      <CompanyResource
                        thumbnailUrl={resource?.cover!}
                        title={resource?.title!}
                        description={resource?.summary}
                        footerText={`${resource.format} ${resource.length ?? ''}`}
                      />
                    </div>
                  ))}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export const CompanyViewAsVisitor = () => {
  const params = useParams();
  const companyId = String(Object.values(params)[0]);

  return <CompanyViewAsVisitorPage _companyIdOrSlug={companyId} />;
};

export default CompanyViewAsVisitor;
