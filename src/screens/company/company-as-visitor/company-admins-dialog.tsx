import { useEffect, useState } from 'react';

import Select from 'react-select';

import { CardCompanyAdmins } from '@panelist/panelist-components-beta';

import { ICompanyAdmin } from '../../../models/company/company-admin';
import { Connection } from '../../../models/connection';
import { IReactSelectOption } from '../../../models/react-select-option';

interface ICompanyAdminsDialogProps {
  currentAdmins: ICompanyAdmin[];
  adminRequests: ICompanyAdmin[];
  companyEmployees: Connection[];
}

export const CompanyAdminsDialog = (props: ICompanyAdminsDialogProps) => {
  const [admins, setAdmins] = useState<IReactSelectOption[]>();
  const [selectedAdmins, setSelectedAdmins] = useState<IReactSelectOption[]>();

  const handleAdminsSelected = (selectedOptions: any) => {
    selectedOptions.map(
      (option: IReactSelectOption) =>
        selectedAdmins?.indexOf(option) === -1 && setSelectedAdmins([...selectedAdmins, option])
    );
  };

  useEffect(() => {
    const adminsArray: IReactSelectOption[] = [];

    props.companyEmployees.map((item) =>
      adminsArray.push({
        key: item.id,
        value: item.id,
        label: item.fullName
      })
    );

    setAdmins(adminsArray);
  }, [props.companyEmployees]);

  return (
    <div className="flex flex-col">
      <div className="text-blue-4 text-md0">Company Admins</div>
      <div className="flex flex-col px-4 py-3">
        <div className="text-md15 text-gray-1 leading-4 font-semibold mt-1 ml-1">
          Admins<span className="text-happening">*</span>
        </div>
        <div className="mt-2">
          <Select options={admins} isMulti onChange={handleAdminsSelected} />
        </div>
        <div className="text-blue-2 text-sm font-light mt-1">
          Add a name or another admins email address if they are not a member on Panelist
        </div>
        <div className="flex flex-row mt-4">
          <div className="bg-white border-2 rounded border-blue-0 w-6 h-6 flex flex-shrink-0 justify-center items-center mr-2 focus-within:border-blue-0">
            <input type="checkbox" className="opacity-0 absolute" />
            <svg
              className="fill-current hidden w-4 h-4 text-blue-3 pointer-events-none"
              viewBox="0 0 20 20"
            >
              <path d="M0 11l2-2 5 5L18 3l2 2L7 18z" />
            </svg>
          </div>
          <div className="text-sm text-blue-2 font-light leading-5 w-full ml-1">
            I verify that I am an authorised representative of this organisation and have the right
            to act on its behalf in the creation and management of this page.
          </div>
        </div>
        <div className="flex flex-row mt-2">
          <div className="text-black text-sm3 font-semibold">Current admins</div>
          <div className="text-black text-sm3 font-light ml-3">Admin requests</div>
        </div>
        <div className="mt-1">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="559.742"
            height="1"
            viewBox="0 0 559.742 1"
          >
            <line
              id="Line_374"
              data-name="Line 374"
              x2="559.742"
              transform="translate(0 0.5)"
              fill="none"
              stroke="#707070"
              strokeWidth="1"
              opacity="0.28"
            />
          </svg>
        </div>
        <div className="flex flex-row">
          {props.currentAdmins.length > 0 &&
            props.currentAdmins.map((item: ICompanyAdmin) => (
              <div className="mt-2 w-full">
                <CardCompanyAdmins
                  containerClass="w-full h-57"
                  avatarUrl={item.avatar ?? process.env.REACT_APP_DEFAULT_AVATAR_IMAGE_URL}
                  avatarClass={'w-12 h-12 rounded-full'}
                  title={`${item.firstName} ${item.lastName}`}
                  subTitle={item.companyAdmin.type}
                  location={''}
                  locationClass={''}
                  secondaryButtonText={''}
                  detailsHref={''}
                  primaryButton="rounded-2xl h-8 w-24 px-5 py-1.5 bg-blue text-gray-2 text-sm absolute right-0"
                  primaryButtonText="Remove"
                />
              </div>
            ))}
        </div>
        <div className="flex flex-row">
          {/* TODO: Implement admin requests feature when API endpoint is developed
                        Jira ticket ID: PB-27
                     */}
        </div>
      </div>
    </div>
  );
};

export default CompanyAdminsDialog;
