import { useEffect, useState } from 'react';

import { useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';

import { Button, TextInput, Typography } from '@panelist/panelist-components-beta';

import { IEditCompanyProfile } from '../../../models/company/company-profile-edit';
import { IReactSelectOption } from '../../../models/react-select-option';
import { IAppState } from '../../../store';
import { getCompanySizes } from '../../../store/company/company-sizes/actions';
import { getIndustries } from '../../../store/company/industries/actions';
import { updateProfile } from '../../../store/company/profile-update/actions';
import { SetProfilePicture } from './set-profile-picture';

interface IEditCompanyProps {
  id: string;
  name: string;
  logo: string;
  slug: string;
  tagline: string;
  description: string;
  industryId: string;
  companySizeId: string;
  website: string;
  headquarters: string;
  companyAdmins: any[];
}

export function EditCompany(props: IEditCompanyProps) {
  const {
    register,
    setValue,
    handleSubmit,
    formState: { errors }
  } = useForm<IEditCompanyProfile>();

  const companyId = props.id;

  const [industryValues, setIndustryValues] = useState<IReactSelectOption[]>();

  const dispatch = useDispatch();
  const { companySizes, industries, companyProfilePicture } = useSelector(
    (state: IAppState) => state
  );

  const onSubmit = (data: IEditCompanyProfile) => {
    const logo = companyProfilePicture.value;
    const dataToSubmit = { ...data, logo };

    dispatch(updateProfile(props.id, dataToSubmit));
  };

  useEffect(() => {
    const populateIndustry = async () => {
      dispatch(getIndustries(companyId));

      const industriesData: IReactSelectOption[] = [];

      industries.value?.data &&
        industries.value?.data.map((industryItem, index) =>
          industriesData.push({
            key: `employee-option-${index}`,
            value: industryItem.id,
            label: industryItem.name ?? `${industryItem.name}`
          })
        );

      setIndustryValues(industriesData);
    };
    populateIndustry();

    const populateCompanySizes = async () => dispatch(getCompanySizes());
    populateCompanySizes();

    setValue('name', props.name, { shouldDirty: true });
    setValue('tagline', props.tagline, { shouldDirty: true });
    setValue('slug', props.slug, { shouldDirty: true });
    setValue('website', props.website, { shouldDirty: true });
    setValue('description', props.description, { shouldDirty: true });
    setValue('headquarter', props.headquarters, { shouldDirty: true });
  }, [dispatch, setValue, companyId, industries, props]);

  return (
    <div>
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className="bg-white shadow-md w-567 h-450 rounded-lg">
          {/* Company Name */}
          <div className="flex justify-start">
            <div className="blue-4 text-md15 -ml-1 mb-3 -mt-6">Edit company information</div>
          </div>
          <div className="flex flex-row mt-1 mb-2">
            <div className="flex flex-row justify-start z-20 mt-2">
              <div className="mb-2 mr-2">
                <div className="text-x-sm text-gray-1 mb-1 ml-3">
                  Company name<span className="text-happening">*</span>
                  <TextInput
                    textinputsize="small"
                    type="text"
                    style={{
                      width: '127px',
                      height: '27px',
                      border: '1px solid #e3e3e3',
                      borderRadius: '20px'
                    }}
                    {...register('name', {
                      required: { value: true, message: 'Company name is required' }
                    })}
                    classes="company-name-field"
                  />
                  {errors.name && (
                    <Typography
                      variant="ValidationMessage"
                      text={errors.name?.message!.toString()}
                      classes="flex content-start text-x-sm name-validation-message"
                    />
                  )}
                </div>
              </div>
            </div>
            {/* Company Logo */}
            <div className="flex flex-row justify-start">
              <SetProfilePicture />
            </div>
            <div className="flex flex-row justify-start z-20 mt-2">
              <div className="mb-2 mr-6">
                <div className="text-x-sm text-gray-1">
                  URL<span className="text-happening">*</span>
                </div>
                <div className=" text-gray-1 text-2x-sm rounded-2xl w-36 h-6 pl-12 py-2 -ml-10">
                  panelist.com/company/
                </div>
              </div>
            </div>
            <div className="flex flex-row justify-start z-20 mt-5">
              <TextInput
                type="text"
                textinputsize="small"
                placeholder="Type here..."
                style={{
                  width: '117px',
                  height: '27px',
                  border: '1px solid #e3e3e3',
                  borderRadius: '20px'
                }}
                {...register('slug', {
                  required: { value: true, message: 'slug is required' }
                })}
                classes="slug-field"
              />
            </div>
            {errors.slug && (
              <Typography
                variant="ValidationMessage"
                text={errors.slug?.message!.toString()}
                classes="flex content-start text-x-sm name-validation-message"
              />
            )}
          </div>
          {/* second line, tagline  */}
          <div className="flex flex-row justify-start z-20 -mt-2">
            <div className="mb-2 mr-8">
              <div className="text-x-sm text-gray-1 ml-3 -mb-1">Tagline</div>
            </div>
          </div>
          <div className="ml-4">
            <TextInput
              textinputsize="extraLarge"
              type="text"
              style={{
                width: '530px',
                height: '27px',
                border: '1px solid #e3e3e3',
                borderRadius: '20px'
              }}
              {...register('tagline')}
              classes="tagline-field"
            />
          </div>
          <div className="flex justify-end mr-5 mt-1  text-x-sm text-blue-2">0/100</div>

          {/* third line, company description */}
          <div className="flex flex-row justify-start z-20 -mt-1">
            <div className="mb-2 mr-6">
              <div className="text-x-sm text-gray-1 ml-3 -mt-1 -mb-1">Company description</div>
            </div>
          </div>
          <textarea
            className="text-sm text-gray-1 px-2 py-2 ml-4 description-field"
            style={{
              width: '530px',
              height: '68px',
              border: '1px solid #e3e3e3',
              borderRadius: '20px'
            }}
            {...register('description')}
          ></textarea>
          <div className="flex justify-end mr-5 -mt-2 text-x-sm text-blue-2">0/2000</div>
          {/* industry  */}
          <div className="flex flex-row mt-1">
            <div className="flex flex-row justify-start z-20 mt-2">
              <div className="mb-2 mr-5">
                <div className="text-x-sm text-gray-1 mb-1 ml-3">
                  Industry<span className="text-happening">*</span>
                </div>
                <div className="flex md:flex-col md:ml-8">
                  <div className="relative inline-flex md:w-6/12 lg:w-full ">
                    <select
                      className="z-10 border border-gray-1 rounded-2xl text-h2 text-gray-1 h-8 pl-3 pr-10 -ml-5 mr-6 bg-white hover:border-gray-400 focus:outline-none appearance-none industry-field"
                      style={{ width: '134.2px', height: '27px' }}
                      {...register('industryId')}
                    >
                      {industryValues &&
                        industryValues.map((industryItem: IReactSelectOption, index) => (
                          <option
                            key={`size-${index}`}
                            value={industryItem.value}
                            selected={industryItem.value === props.industryId}
                          >
                            {industryItem.label}
                          </option>
                        ))}
                    </select>
                  </div>
                  {errors.industryId && (
                    <Typography
                      variant="ValidationMessage"
                      text={errors.industryId?.message!.toString()}
                      classes="flex content-start text-x-sm name-validation-message"
                    />
                  )}
                </div>
              </div>
            </div>
            {/* Company Size */}
            <div className="flex flex-row justify-start z-20 mt-2">
              <div className="mb-2 mr-6">
                <div className="text-x-sm text-gray-1 -ml-8 mb-1">
                  Company size<span className="text-happening">*</span>
                </div>
                <div className="flex md:flex-col md:ml-8">
                  <div className="relative inline-flex md:w-6/12 lg:w-full ">
                    <svg
                      className="w-2 h-2 absolute top-0 right-2 m-2 pointer-events-none"
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 412 232"
                    >
                      <path
                        d="M206 171.144L42.678 7.822c-9.763-9.763-25.592-9.763-35.355 0-9.763 9.764-9.763 25.592 0 35.355l181 181c4.88 4.882 11.279 7.323 17.677 7.323s12.796-2.441 17.678-7.322l181-181c9.763-9.764 9.763-25.592 0-35.355-9.763-9.763-25.592-9.763-35.355 0L206 171.144z"
                        fill="#648299"
                        fillRule="nonzero"
                      />
                    </svg>
                    <select
                      className="border border-gray-1 rounded-2xl text-h2 text-gray-1 h-8 pl-3 pr-10 -ml-16 bg-white hover:border-gray-400 focus:outline-none appearance-none company-sizes-field"
                      style={{ width: '134.2px', height: '27px' }}
                      {...register('companySizeId')}
                    >
                      {companySizes &&
                        companySizes.value?.data.map((size, index) => (
                          <option
                            key={`size-${index}`}
                            value={size.id}
                            selected={size.id === props.companySizeId}
                          >
                            {size.name}
                          </option>
                        ))}
                    </select>
                  </div>
                  {errors.companySizeId && (
                    <Typography
                      variant="ValidationMessage"
                      text={errors.companySizeId?.message!.toString()}
                      classes="flex content-start text-x-sm name-validation-message"
                    />
                  )}
                </div>
              </div>
            </div>
            {/* Website */}
            <div className="flex flex-row justify-start z-20 mt-2">
              <div className="mb-2 mr-6">
                <div className="text-x-sm text-gray-1 mb-1 -ml-2">
                  Website<span className="text-happening">*</span>
                </div>
              </div>
            </div>
            <div className="mt-7 -ml-16">
              <TextInput
                textinputsize="large"
                type="text"
                placeholder={"Link to your company's external website"}
                style={{
                  width: '227px',
                  height: '27px',
                  border: '1px solid #e3e3e3',
                  borderRadius: '20px',
                  marginRight: '18px'
                }}
                {...register('website')}
                classes="website-field"
              />
              {errors.website && (
                <Typography
                  variant="ValidationMessage"
                  text={errors.website.message!.toString()}
                  classes="flex content-start text-x-sm name-validation-message"
                />
              )}
            </div>
          </div>
          {/* Headquaters */}
          <div className="flex flex-row mt-5">
            <div className="flex flex-row justify-start z-20 mt-1">
              <div className="mb-2 mr-6">
                <div className="text-x-sm text-gray-1 ml-3 mb-1">
                  Headquaters<span className="text-happening">*</span>
                </div>
              </div>
            </div>
            <div className="mt-6 -ml-28 mr-14 pl-2">
              <TextInput
                textinputsize="large"
                type="text"
                style={{
                  width: '202px',
                  height: '27px',
                  border: '1px solid #e3e3e3',
                  borderRadius: '20px'
                }}
                {...register('headquarter', {
                  required: { value: true, message: 'Headquarter is required' }
                })}
                classes="headquarters-field"
              />
            </div>
            {/* Company admin */}
            <div className="flex flex-row justify-start z-20 mt-1">
              <div className="mb-2 mr-6">
                <div className="text-x-sm text-gray-1 -ml-6 mb-2">
                  Company admin<span className="text-happening">*</span>
                </div>
                <div className="-ml-10">
                  {/* <div className="border-gray-1 border-solid border-1 text-blue-2 text-2x-sm rounded-2xl w-318 h-24 py-2 ml-22"> */}
                  <TextInput
                    textinputsize="extraLarge"
                    type="text"
                    style={{
                      width: '318px',
                      height: '96px',
                      border: '1px solid #e3e3e3',
                      borderRadius: '20px'
                    }}
                    {...register('tagline')}
                    classes="company-admins-field"
                  />
                </div>
                {/* </div> */}
                <div className="text-x-sm text-blue-2 font-light -ml-4">
                  Enter a name or email address
                </div>
              </div>
            </div>
          </div>
          <div className="flex flex-row -mt-24 ml-3">
            <div className="bg-white border-2 rounded border-blue-0 w-3 h-3 flex flex-shrink-0 mt-5 mr-1 focus-within:border-blue-0">
              <input type="checkbox" className="opacity-0 absolute verification-checkbox" />
              <svg
                className="fill-current hidden w-4 h-4 text-blue-3 pointer-events-none"
                viewBox="0 0 20 20"
              >
                <path d="M0 11l2-2 5 5L18 3l2 2L7 18z" />
              </svg>
            </div>
            <div className="text-x-sm text-blue-2 font-light w-48 mt-4">
              I verify that I am an authorised representative of this organisation and have the
              right to act on its behalf in the creation and management of this page.
            </div>
          </div>
        </div>
        <div className="flex justify-end mt-2">
          <Button
            type="submit"
            size="x-large"
            text="Save &amp; Publish"
            classes="bg-blue-1 text-white"
          />
        </div>
      </form>
    </div>
  );
}
