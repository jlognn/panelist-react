import { useRef } from 'react';

import { useDispatch, useSelector } from 'react-redux';

import changeProfilePicture from '../../../assets/images/change-profile-picture.png';
import { IAppState } from '../../../store';
import { setProfilePicture } from '../../../store/company/profile-picture/actions';
import { uploadImage } from '../../../store/fileupload/imageupload/actions';

export function SetProfilePicture() {
  const profilePictureRef = useRef<HTMLInputElement | null>(null);
  const { imageUpload } = useSelector((state: IAppState) => state);

  const dispatch = useDispatch();

  function handleProfilePictureSelected(files: FileList) {
    const data = new FormData();
    data.append(files[0].name, files[0]);
    dispatch(uploadImage(data));

    const uploadedFile = imageUpload.value?.data;
    dispatch(setProfilePicture(uploadedFile?.key));
  }

  return (
    <div className="flex flex-row justify-start z-20 mt-2">
      <div className="mb-2 mr-2">
        <div className="text-x-sm text-gray-1 mb-1 ml-1">Company logo</div>
        <div className="relative bg-gray-1 text-gray-1 font-medium text-2x-sm rounded-2xl w-36 h-6 pl-9 py-2">
          PNG recommended
          <div className="absolute z-30 -mt-7 -ml-11">
            <img
              src={changeProfilePicture}
              alt=""
              className="cursor-pointer w-10"
              onClick={() => profilePictureRef?.current?.click()}
            />
            <input
              type="file"
              id="profilePicture"
              ref={profilePictureRef}
              className="hidden"
              onChange={(e) => handleProfilePictureSelected(e.target?.files!)}
            />
          </div>
        </div>
      </div>
    </div>
  );
}
