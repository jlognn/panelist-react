import Enzyme, { mount } from 'enzyme';
import * as reactRedux from 'react-redux';

import Adapter from '@wojtekmaj/enzyme-adapter-react-17';

import { ICompanyProfileState } from '../../../models/company/CompanyProfileState';
import { CompanyViewAsVisitorPage } from '../company-as-visitor';
import { EditCompany } from './';

Enzyme.configure({ adapter: new Adapter() });

const useSelectorMock = jest.spyOn(reactRedux, 'useSelector');
const useDispatchMock = jest.spyOn(reactRedux, 'useDispatch');

const mockCompanyProfileState: ICompanyProfileState = {
  value: {
    id: '15933b75-52fa-404d-bfb1-cf2f08f23626',
    name: 'A-Z Project Management',
    tagline: '',
    description: 'ssdefe',
    logo: 'images/f60f2de0-1de9-45d4-9fce-205ca492463c.jpg',
    cover: 'images/c0ceede4-536c-48b5-b86b-2b7a77682f0a.png',
    website: 'abc.com',
    industryId: '1a28e486-21e1-45e0-ad3b-7d5d04608a91',
    companySizeId: '26b548e8-99fe-41d4-b033-6a95998fd391',
    headquarter: 'Atlanta, GA, USA',
    city: '',
    suburb: '',
    state: '',
    country: '',
    postCode: '',
    slug: 'a-z-project-management-8c503fl',
    isPage: true,
    userId: '4185dd94-9155-49b6-943e-724167966cee',
    latitude: '33.74899540',
    longitude: '-84.38798240',
    createdAt: '2021-02-21T02:30:06.000Z',
    updatedAt: '2021-07-22T02:51:36.000Z',
    isFollowing: 0,
    isAdmin: 0,
    admins: [],
    companySpecialities: [
      {
        id: 'e459f04c-f6c7-454c-b352-70fda8b9421e',
        name: 'Information Technology'
      },
      {
        id: 'acc3ce4c-39cf-4883-a2b4-35068493e5f9',
        name: 'Entertainment'
      }
    ],
    industry: {
      id: '1a28e486-21e1-45e0-ad3b-7d5d04608a91',
      name: 'Alternative Dispute Resolution'
    },
    companySize: {
      id: '26b548e8-99fe-41d4-b033-6a95998fd391',
      name: '1-10'
    }
  },
  loading: false,
  errorMessage: ''
};

const mockSelectorValues = {
  companySizes: {},
  industries: {},
  companyProfilePreview: mockCompanyProfileState,
  companyProfilePicture: {},
  companyEmployees: {},
  posts: {},
  companyEvents: {},
  companyResources: {}
};

jest.mock('react-redux', () => ({
  ...jest.requireActual('react-redux'),
  useSelector: jest.fn()
}));

jest.mock('react', () => ({
  ...jest.requireActual('react'),
  useEffect: jest.fn()
}));

describe('Top level', () => {
  beforeEach(() => {
    useSelectorMock.mockReturnValue(mockSelectorValues);

    const mockDispatch = jest.fn().mockReturnValue(mockCompanyProfileState);
    useDispatchMock.mockImplementation(mockDispatch);
  });

  afterEach(() => {
    useSelectorMock.mockClear();
    useDispatchMock.mockClear();
  });

  it('should find company based on slug', () => {
    useSelectorMock.mockImplementation(() => {
      return mockSelectorValues;
    });

    const companyViewAsVisitorWrapper = mount(
      <CompanyViewAsVisitorPage companyIdOrSlug="a-z-project-management-8c503fl" />
    );
    const toggleModalButton = companyViewAsVisitorWrapper.find('.test-modal-toggle');
    toggleModalButton.simulate('click');

    const editCompanyWrapper = mount(
      <EditCompany
        id={mockCompanyProfileState.value?.id!}
        name={mockCompanyProfileState.value?.name!}
        logo={mockCompanyProfileState.value?.logo!}
        slug={mockCompanyProfileState.value?.slug!}
        tagline={mockCompanyProfileState.value?.tagline!}
        description={mockCompanyProfileState.value?.description!}
        industryId={mockCompanyProfileState.value?.industryId!}
        companySizeId={mockCompanyProfileState.value?.companySizeId!}
        website={mockCompanyProfileState.value?.website!}
        headquarters={mockCompanyProfileState.value?.headquarter!}
        companyAdmins={[]}
      />
    );
    setTimeout(() => {
      expect(editCompanyWrapper).toBeInTheDocument();
      expect(editCompanyWrapper.find('.company-name-field').text()).toEqual(
        'A-Z Project Management'
      );
    }, 1000);
  });

  it('should find company based on id', () => {
    useSelectorMock.mockImplementation(() => {
      return mockSelectorValues;
    });

    const companyViewAsVisitorWrapper = mount(
      <CompanyViewAsVisitorPage companyIdOrSlug="15933b75-52fa-404d-bfb1-cf2f08f23626" />
    );
    const toggleModalButton = companyViewAsVisitorWrapper.find('.test-modal-toggle');
    toggleModalButton.simulate('click');

    const editCompanyWrapper = mount(
      <EditCompany
        id={mockCompanyProfileState.value?.id!}
        name={mockCompanyProfileState.value?.name!}
        logo={mockCompanyProfileState.value?.logo!}
        slug={mockCompanyProfileState.value?.slug!}
        tagline={mockCompanyProfileState.value?.tagline!}
        description={mockCompanyProfileState.value?.description!}
        industryId={mockCompanyProfileState.value?.industryId!}
        companySizeId={mockCompanyProfileState.value?.companySizeId!}
        website={mockCompanyProfileState.value?.website!}
        headquarters={mockCompanyProfileState.value?.headquarter!}
        companyAdmins={[]}
      />
    );
    setTimeout(() => {
      expect(editCompanyWrapper).toBeInTheDocument();
      expect(editCompanyWrapper.find('.company-name-field').text()).toEqual(
        'A-Z Project Management'
      );
    }, 1000);
  });

  it.todo("it should error if the company id or slug doesn't exist");
});

describe('company name', () => {
  it('should render company names', () => {
    useSelectorMock.mockImplementation(() => {
      return mockSelectorValues;
    });

    const companyViewAsVisitorWrapper = mount(
      <CompanyViewAsVisitorPage companyIdOrSlug="15933b75-52fa-404d-bfb1-cf2f08f23626" />
    );
    const toggleModalButton = companyViewAsVisitorWrapper.find('.test-modal-toggle');
    toggleModalButton.simulate('click');

    const editCompanyWrapper = mount(
      <EditCompany
        id={mockCompanyProfileState.value?.id!}
        name={mockCompanyProfileState.value?.name!}
        logo={mockCompanyProfileState.value?.logo!}
        slug={mockCompanyProfileState.value?.slug!}
        tagline={mockCompanyProfileState.value?.tagline!}
        description={mockCompanyProfileState.value?.description!}
        industryId={mockCompanyProfileState.value?.industryId!}
        companySizeId={mockCompanyProfileState.value?.companySizeId!}
        website={mockCompanyProfileState.value?.website!}
        headquarters={mockCompanyProfileState.value?.headquarter!}
        companyAdmins={[]}
      />
    );
    setTimeout(() => {
      expect(editCompanyWrapper).toBeInTheDocument();
      expect(editCompanyWrapper.find('.company-name-field').text()).toEqual(
        'A-Z Project Management'
      );
    }, 1000);
  });

  it.todo('it should render errors if no company name');
});

describe('logo', () => {
  it.todo('it should render company logo');
});

describe('panelist url', () => {
  it('should render company url', () => {
    useSelectorMock.mockImplementation(() => {
      return mockSelectorValues;
    });

    const companyViewAsVisitorWrapper = mount(
      <CompanyViewAsVisitorPage companyIdOrSlug="15933b75-52fa-404d-bfb1-cf2f08f23626" />
    );
    const toggleModalButton = companyViewAsVisitorWrapper.find('.test-modal-toggle');
    toggleModalButton.simulate('click');

    const editCompanyWrapper = mount(
      <EditCompany
        id={mockCompanyProfileState.value?.id!}
        name={mockCompanyProfileState.value?.name!}
        logo={mockCompanyProfileState.value?.logo!}
        slug={mockCompanyProfileState.value?.slug!}
        tagline={mockCompanyProfileState.value?.tagline!}
        description={mockCompanyProfileState.value?.description!}
        industryId={mockCompanyProfileState.value?.industryId!}
        companySizeId={mockCompanyProfileState.value?.companySizeId!}
        website={mockCompanyProfileState.value?.website!}
        headquarters={mockCompanyProfileState.value?.headquarter!}
        companyAdmins={[]}
      />
    );
    setTimeout(() => {
      expect(editCompanyWrapper).toBeInTheDocument();
      expect(editCompanyWrapper.find('.slug-field')).toBeInTheDocument();
    }, 1000);
  });

  it.todo('it should render error if no company url');

  it('should prefill company url', () => {
    useSelectorMock.mockImplementation(() => {
      return mockSelectorValues;
    });

    const companyViewAsVisitorWrapper = mount(
      <CompanyViewAsVisitorPage companyIdOrSlug="15933b75-52fa-404d-bfb1-cf2f08f23626" />
    );
    const toggleModalButton = companyViewAsVisitorWrapper.find('.test-modal-toggle');
    toggleModalButton.simulate('click');

    const editCompanyWrapper = mount(
      <EditCompany
        id={mockCompanyProfileState.value?.id!}
        name={mockCompanyProfileState.value?.name!}
        logo={mockCompanyProfileState.value?.logo!}
        slug={mockCompanyProfileState.value?.slug!}
        tagline={mockCompanyProfileState.value?.tagline!}
        description={mockCompanyProfileState.value?.description!}
        industryId={mockCompanyProfileState.value?.industryId!}
        companySizeId={mockCompanyProfileState.value?.companySizeId!}
        website={mockCompanyProfileState.value?.website!}
        headquarters={mockCompanyProfileState.value?.headquarter!}
        companyAdmins={[]}
      />
    );
    setTimeout(() => {
      expect(editCompanyWrapper).toBeInTheDocument();
      expect(editCompanyWrapper.find('.slug-field').text()).toEqual(
        'a-z-project-management-8c503fl'
      );
    }, 1000);
  });
});

describe('tagline', () => {
  it('should render tagline', () => {
    useSelectorMock.mockImplementation(() => {
      return mockSelectorValues;
    });

    const companyViewAsVisitorWrapper = mount(
      <CompanyViewAsVisitorPage companyIdOrSlug="15933b75-52fa-404d-bfb1-cf2f08f23626" />
    );
    const toggleModalButton = companyViewAsVisitorWrapper.find('.test-modal-toggle');
    toggleModalButton.simulate('click');

    const editCompanyWrapper = mount(
      <EditCompany
        id={mockCompanyProfileState.value?.id!}
        name={mockCompanyProfileState.value?.name!}
        logo={mockCompanyProfileState.value?.logo!}
        slug={mockCompanyProfileState.value?.slug!}
        tagline={mockCompanyProfileState.value?.tagline!}
        description={mockCompanyProfileState.value?.description!}
        industryId={mockCompanyProfileState.value?.industryId!}
        companySizeId={mockCompanyProfileState.value?.companySizeId!}
        website={mockCompanyProfileState.value?.website!}
        headquarters={mockCompanyProfileState.value?.headquarter!}
        companyAdmins={[]}
      />
    );
    setTimeout(() => {
      expect(editCompanyWrapper).toBeInTheDocument();
      expect(editCompanyWrapper.find('.tagline-field')).toBeInTheDocument();
    }, 1000);
  });
});

describe('description', () => {
  it('should render company description', () => {
    useSelectorMock.mockImplementation(() => {
      return mockSelectorValues;
    });

    const companyViewAsVisitorWrapper = mount(
      <CompanyViewAsVisitorPage companyIdOrSlug="15933b75-52fa-404d-bfb1-cf2f08f23626" />
    );
    const toggleModalButton = companyViewAsVisitorWrapper.find('.test-modal-toggle');
    toggleModalButton.simulate('click');

    const editCompanyWrapper = mount(
      <EditCompany
        id={mockCompanyProfileState.value?.id!}
        name={mockCompanyProfileState.value?.name!}
        logo={mockCompanyProfileState.value?.logo!}
        slug={mockCompanyProfileState.value?.slug!}
        tagline={mockCompanyProfileState.value?.tagline!}
        description={mockCompanyProfileState.value?.description!}
        industryId={mockCompanyProfileState.value?.industryId!}
        companySizeId={mockCompanyProfileState.value?.companySizeId!}
        website={mockCompanyProfileState.value?.website!}
        headquarters={mockCompanyProfileState.value?.headquarter!}
        companyAdmins={[]}
      />
    );
    setTimeout(() => {
      expect(editCompanyWrapper).toBeInTheDocument();
      expect(editCompanyWrapper.find('.description-field')).toBeInTheDocument();
    }, 1000);
  });
});

describe('industry', () => {
  it('should render industry', () => {
    useSelectorMock.mockImplementation(() => {
      return mockSelectorValues;
    });

    const companyViewAsVisitorWrapper = mount(
      <CompanyViewAsVisitorPage companyIdOrSlug="15933b75-52fa-404d-bfb1-cf2f08f23626" />
    );
    const toggleModalButton = companyViewAsVisitorWrapper.find('.test-modal-toggle');
    toggleModalButton.simulate('click');

    const editCompanyWrapper = mount(
      <EditCompany
        id={mockCompanyProfileState.value?.id!}
        name={mockCompanyProfileState.value?.name!}
        logo={mockCompanyProfileState.value?.logo!}
        slug={mockCompanyProfileState.value?.slug!}
        tagline={mockCompanyProfileState.value?.tagline!}
        description={mockCompanyProfileState.value?.description!}
        industryId={mockCompanyProfileState.value?.industryId!}
        companySizeId={mockCompanyProfileState.value?.companySizeId!}
        website={mockCompanyProfileState.value?.website!}
        headquarters={mockCompanyProfileState.value?.headquarter!}
        companyAdmins={[]}
      />
    );
    setTimeout(() => {
      expect(editCompanyWrapper).toBeInTheDocument();
      expect(editCompanyWrapper.find('.industry-field')).toBeInTheDocument();
    }, 1000);
  });

  it.todo('it should error on no company industry');
});

describe('size', () => {
  it('should render company size', () => {
    useSelectorMock.mockImplementation(() => {
      return mockSelectorValues;
    });

    const companyViewAsVisitorWrapper = mount(
      <CompanyViewAsVisitorPage companyIdOrSlug="15933b75-52fa-404d-bfb1-cf2f08f23626" />
    );
    const toggleModalButton = companyViewAsVisitorWrapper.find('.test-modal-toggle');
    toggleModalButton.simulate('click');

    const editCompanyWrapper = mount(
      <EditCompany
        id={mockCompanyProfileState.value?.id!}
        name={mockCompanyProfileState.value?.name!}
        logo={mockCompanyProfileState.value?.logo!}
        slug={mockCompanyProfileState.value?.slug!}
        tagline={mockCompanyProfileState.value?.tagline!}
        description={mockCompanyProfileState.value?.description!}
        industryId={mockCompanyProfileState.value?.industryId!}
        companySizeId={mockCompanyProfileState.value?.companySizeId!}
        website={mockCompanyProfileState.value?.website!}
        headquarters={mockCompanyProfileState.value?.headquarter!}
        companyAdmins={[]}
      />
    );
    setTimeout(() => {
      expect(editCompanyWrapper).toBeInTheDocument();
      expect(editCompanyWrapper.find('.company-size-field')).toBeInTheDocument();
    }, 1000);
  });

  it.todo('it should error on no company size');
});

describe('website', () => {
  it('should render company website', () => {
    useSelectorMock.mockImplementation(() => {
      return mockSelectorValues;
    });

    const companyViewAsVisitorWrapper = mount(
      <CompanyViewAsVisitorPage companyIdOrSlug="15933b75-52fa-404d-bfb1-cf2f08f23626" />
    );
    const toggleModalButton = companyViewAsVisitorWrapper.find('.test-modal-toggle');
    toggleModalButton.simulate('click');

    const editCompanyWrapper = mount(
      <EditCompany
        id={mockCompanyProfileState.value?.id!}
        name={mockCompanyProfileState.value?.name!}
        logo={mockCompanyProfileState.value?.logo!}
        slug={mockCompanyProfileState.value?.slug!}
        tagline={mockCompanyProfileState.value?.tagline!}
        description={mockCompanyProfileState.value?.description!}
        industryId={mockCompanyProfileState.value?.industryId!}
        companySizeId={mockCompanyProfileState.value?.companySizeId!}
        website={mockCompanyProfileState.value?.website!}
        headquarters={mockCompanyProfileState.value?.headquarter!}
        companyAdmins={[]}
      />
    );
    setTimeout(() => {
      expect(editCompanyWrapper).toBeInTheDocument();
      expect(editCompanyWrapper.find('.website-field')).toBeInTheDocument();
    }, 1000);
  });

  it.todo('it should error if no company website');
});

describe('headquarters', () => {
  it('should render company headquarters', () => {
    useSelectorMock.mockImplementation(() => {
      return mockSelectorValues;
    });

    const companyViewAsVisitorWrapper = mount(
      <CompanyViewAsVisitorPage companyIdOrSlug="15933b75-52fa-404d-bfb1-cf2f08f23626" />
    );
    const toggleModalButton = companyViewAsVisitorWrapper.find('.test-modal-toggle');
    toggleModalButton.simulate('click');

    const editCompanyWrapper = mount(
      <EditCompany
        id={mockCompanyProfileState.value?.id!}
        name={mockCompanyProfileState.value?.name!}
        logo={mockCompanyProfileState.value?.logo!}
        slug={mockCompanyProfileState.value?.slug!}
        tagline={mockCompanyProfileState.value?.tagline!}
        description={mockCompanyProfileState.value?.description!}
        industryId={mockCompanyProfileState.value?.industryId!}
        companySizeId={mockCompanyProfileState.value?.companySizeId!}
        website={mockCompanyProfileState.value?.website!}
        headquarters={mockCompanyProfileState.value?.headquarter!}
        companyAdmins={[]}
      />
    );
    setTimeout(() => {
      expect(editCompanyWrapper).toBeInTheDocument();
      expect(editCompanyWrapper.find('.headquarters-field')).toBeInTheDocument();
    }, 1000);
  });

  it.todo('it should error on no headquarters');
});

describe('admin', () => {
  it('should render company admin', () => {
    useSelectorMock.mockImplementation(() => {
      return mockSelectorValues;
    });

    const companyViewAsVisitorWrapper = mount(
      <CompanyViewAsVisitorPage companyIdOrSlug="15933b75-52fa-404d-bfb1-cf2f08f23626" />
    );
    const toggleModalButton = companyViewAsVisitorWrapper.find('.test-modal-toggle');
    toggleModalButton.simulate('click');

    const editCompanyWrapper = mount(
      <EditCompany
        id={mockCompanyProfileState.value?.id!}
        name={mockCompanyProfileState.value?.name!}
        logo={mockCompanyProfileState.value?.logo!}
        slug={mockCompanyProfileState.value?.slug!}
        tagline={mockCompanyProfileState.value?.tagline!}
        description={mockCompanyProfileState.value?.description!}
        industryId={mockCompanyProfileState.value?.industryId!}
        companySizeId={mockCompanyProfileState.value?.companySizeId!}
        website={mockCompanyProfileState.value?.website!}
        headquarters={mockCompanyProfileState.value?.headquarter!}
        companyAdmins={[]}
      />
    );
    setTimeout(() => {
      expect(editCompanyWrapper).toBeInTheDocument();
      expect(editCompanyWrapper.find('.company-admins-field')).toBeInTheDocument();
    }, 1000);
  });

  it.todo('it should error on no company admin');
});

describe('verification', () => {
  it('should render verification', () => {
    useSelectorMock.mockImplementation(() => {
      return mockSelectorValues;
    });

    const companyViewAsVisitorWrapper = mount(
      <CompanyViewAsVisitorPage companyIdOrSlug="15933b75-52fa-404d-bfb1-cf2f08f23626" />
    );
    const toggleModalButton = companyViewAsVisitorWrapper.find('.test-modal-toggle');
    toggleModalButton.simulate('click');

    const editCompanyWrapper = mount(
      <EditCompany
        id={mockCompanyProfileState.value?.id!}
        name={mockCompanyProfileState.value?.name!}
        logo={mockCompanyProfileState.value?.logo!}
        slug={mockCompanyProfileState.value?.slug!}
        tagline={mockCompanyProfileState.value?.tagline!}
        description={mockCompanyProfileState.value?.description!}
        industryId={mockCompanyProfileState.value?.industryId!}
        companySizeId={mockCompanyProfileState.value?.companySizeId!}
        website={mockCompanyProfileState.value?.website!}
        headquarters={mockCompanyProfileState.value?.headquarter!}
        companyAdmins={[]}
      />
    );
    setTimeout(() => {
      expect(editCompanyWrapper).toBeInTheDocument();
      expect(editCompanyWrapper.find('.verification-checkbox')).toBeInTheDocument();
    }, 1000);
  });
});
