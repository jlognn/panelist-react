import { useEffect, useState } from 'react';

import { useDispatch, useSelector } from 'react-redux';

import editButton from '../../../assets/images/edit-icon.png';
import { Modal } from '@panelist/panelist-components-beta';
import companyEventsIcon from '../../../assets/images/events-big.svg';
import optionsButton from '../../../assets/images/options-button.svg';
import companyPosts from '../../../assets/images/posts-big.svg';
import companyResourcesIcon from '../../../assets/images/resources-big.svg';
import shareButton from '../../../assets/images/share-button.svg';
import techShow from '../../../assets/images/tech-show.svg';
import verifiedIcon from '../../../assets/images/verified-icon.png';
import { ModalDialog } from '../../../components/modal-dialog';
import { ICompanyHeader } from '../../../models/company/company-header';
import { Connection } from '../../../models/connection';
import { IAppState } from '../../../store';
import { getCompanyEmployees } from '../../../store/company/company-employees/actions';
import { getCompanyProfilePreview } from '../../../store/company/profile-preview/actions';
import { getUserProfile } from '../../../store/user/user-profile/actions';
import { EditCompany } from '../edit-company';
import { CompanyAdminsDialog } from '../company-as-visitor/company-admins-dialog';

export const CompanyHeader = (props: ICompanyHeader) => {
  const imageUrlPrefix = process.env.REACT_APP_IMAGE_URL_PREFIX_STAGING;
  const noCompanyLogoUrl = process.env.REACT_APP_NO_COMPANY_LOGO_URL;

  const [isMember, setIsMember] = useState<boolean>(false);
  const [adminsCount, setAdminsCount] = useState<number>(0);

  const { userProfile, companyProfilePreview, companyEmployees } = useSelector(
    (state: IAppState) => state
  );
  const dispatch = useDispatch();

  const [companyEmployeesData, setCompanyEmployeesData] = useState<Connection[]>();

  useEffect(() => {
    dispatch(getUserProfile());
    dispatch(getCompanyEmployees(props.id));
    dispatch(getCompanyProfilePreview(props.id));

    if (userProfile.value && userProfile.value.companyId === props.id) setIsMember(true);

    if (companyProfilePreview.value && companyProfilePreview.value.admins)
      setAdminsCount(companyProfilePreview.value.admins.length);

    const connections: Connection[] = [];
    companyEmployees.value &&
      companyEmployees.value?.data.map((emp) =>
        connections.push({
          fullName: emp.fullName || `${emp.firstName} ${emp.lastName}`,
          email: '',
          id: emp.id,
          firstName: emp.firstName,
          lastName: emp.lastName,
          slug: emp.slug,
          avatar: emp.avatar,
          jobTitle: emp.jobTitle,
          companyId: emp.companyId,
          createdAt: emp.createdAt,
          connectionId: ''
        })
      );

    setCompanyEmployeesData(connections);
  }, [dispatch, userProfile, companyProfilePreview, companyEmployees, props]);

  return (
    <div
      className="bg-white rounded-lg w-1124 mt-4 ml-10"
      style={{
        boxShadow: '1px 1px 4px 0 rgba(0, 0, 0, 0.2)'
      }}
    >
      <div className="flex flex-col">
        <div className="mx-auto">
          {props.cover ? (
            <img src={`${imageUrlPrefix}/${props.cover}`} alt="" />
          ) : (
            <div className="w-1069 h-238 bg-gray-7 rounded-lg">&nbsp;</div>
          )}
        </div>
        <div className={`${props.logo ? ' -mt-12' : ''} w-full`}>
          <div className="flex flex-row justify-between">
            {isMember && (
              <div className="ml-4 mt-16 bg-blue-0 p-2 rounded-xl h-35 text-sm text-blue-2 font-light">
                View as a Member
              </div>
            )}
            <div className="-ml-20">
              <img
                src={props.logo ? `${imageUrlPrefix}/${props.logo}` : noCompanyLogoUrl}
                alt=""
                className="h-130 w-130 ml-24 -mt-8"
              />
            </div>
            <div className="mt-12 py-4">
              <div className="flex flex-row">
                {isMember && (
                  <Modal
                    OpenModalbutton="bg-blue-0 rounded-xl h-35 px-4 py-1.5 mr-2"
                    openModalText={`${adminsCount} Admins`}
                    children={
                      <CompanyAdminsDialog
                        currentAdmins={companyProfilePreview.value?.admins ?? []}
                        adminRequests={[]}
                        companyEmployees={companyEmployeesData!}
                      />
                    }
                    modalWidth="630px"
                    modalHeight="710px"
                  />
                )}
                <div className="bg-blue-0 rounded-xl h-35 px-4 py-1.5">
                  <span className="text-md15 text-blue-7 font-semibold">Follow</span>
                </div>
                <div className="mt-2 ml-2">
                  {/* <img src={editButton} alt="Share" /> */}
                  <ModalDialog
                    buttonImageUrl={editButton}
                    children={
                      <EditCompany
                        id={companyProfilePreview.value?.id ?? ''}
                        name={companyProfilePreview.value?.name ?? ''}
                        logo={companyProfilePreview.value?.logo ?? ''}
                        slug={companyProfilePreview.value?.slug ?? ''}
                        tagline={companyProfilePreview.value?.tagline ?? ''}
                        description={companyProfilePreview.value?.description ?? ''}
                        industryId={companyProfilePreview.value?.industryId ?? ''}
                        companySizeId={companyProfilePreview.value?.companySizeId ?? ''}
                        website={companyProfilePreview.value?.website ?? ''}
                        headquarters={companyProfilePreview.value?.headquarter ?? ''}
                        companyAdmins={[]}
                      />
                    }
                    modalWidth="605px"
                    modalHeight="625px"
                    classes="test-modal-toggle"
                  />
                </div>
                <div className="mt-1 ml-6">
                  <img src={shareButton} alt="Share" />
                </div>
                <div className="mt-1 ml-1">
                  <img src={optionsButton} alt="Share" />
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="mx-auto flex flex-row">
          <div className="text-lg text-black font-semibold leading-1">{props.name}</div>
          <div className="ml-0.5">
            <img src={verifiedIcon} alt="" />
          </div>
        </div>
        <div className="flex flex-row justify-around px-16 mt-4 mb-2">
          {props.showPosts && (
            <div className="flex flex-col">
              <div>
                <img src={companyPosts} alt="" />
              </div>
              <div className="text-sm font-medium  text-gray-6">Posts</div>
            </div>
          )}
          {props.showTechShows && (
            <div className="flex flex-col">
              <div>
                <img src={techShow} alt="" />
              </div>
              <div className="text-sm font-medium  text-gray-6">Techshows</div>
            </div>
          )}

          {props.showResources && (
            <div className="flex flex-col">
              <div>
                <img src={companyResourcesIcon} alt="" />
              </div>
              <div className="text-sm font-medium  text-gray-6">Resources</div>
            </div>
          )}

          {props.showEvents && (
            <div className="flex flex-col">
              <div>
                <img src={companyEventsIcon} alt="" />
              </div>
              <div className="text-sm font-medium text-gray-6">Events</div>
            </div>
          )}
        </div>
      </div>
    </div>
  );
};

export default CompanyHeader;
