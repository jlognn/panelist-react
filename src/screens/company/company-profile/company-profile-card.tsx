import companySizeIcon from '../../../assets/images/company-size-icon.svg';
import descriptionIcon from '../../../assets/images/description-icon.svg';
import specailityIcon from '../../../assets/images/specialties-icon.svg';
import websiteIcon from '../../../assets/images/website-icon.svg';
import { ICompanyProfileCardProps } from '../../../models/company/company-profile-card';

export const CompanyProfileCard = (props: ICompanyProfileCardProps) => {
  const imageUrlPrefix = process.env.REACT_APP_IMAGE_URL_PREFIX_STAGING;
  const noCompanyLogoUrl = process.env.REACT_APP_NO_COMPANY_LOGO_URL;

  return (
    <div className="flex flex-col px-4 py-4 mb-2">
      <div className="flex flex-row ml-3">
        <div>
          <img
            src={props.logoUrl ? `${imageUrlPrefix}/${props.logoUrl}` : noCompanyLogoUrl}
            alt=""
            className="w-55 h-55"
          />
        </div>
        <div className="flex flex-col mt-2 ml-2">
          <div className="text-lg text-black leading-5">{props.companyName}</div>
          <div className="text-sm text-gray-1 leading-6">{props.industryName}</div>
          <div className="text-h3 text-gray-1 leading-5">{props.headquarter}</div>
        </div>
      </div>
      <div className="flex flex-col mt-4 ml-3">
        <div className="flex flex-row">
          <div>
            <img src={descriptionIcon} alt="" />
          </div>
          <div className="text-md15 text-gray-1 leading-4 font-semibold mt-1 ml-1">About</div>
        </div>
        <div className="text-x-sm text-gray-1 leading-4 font-light ml-6 mt-1">
          {props.about}{' '}
          <a href="/" className="text-blue-1 font-semibold">
            See more
          </a>
        </div>
      </div>
      <div className="flex flex-col mt-4 ml-3">
        <div className="flex flex-row">
          <div>
            <img src={specailityIcon} alt="" />
          </div>
          <div className="text-md15 text-gray-1 leading-4 font-semibold mt-1 ml-1">
            Specialities
          </div>
        </div>
        <div className="text-x-sm text-gray-1 leading-4 font-light ml-6 mt-1">
          <ul className="list-disc ml-5">
            {props.specialities &&
              props.specialities.map((speciality, index) => (
                <li key={`speciality-${index}`}>{speciality.name}</li>
              ))}
          </ul>
          <a href="/" className="text-blue-1 font-semibold ml-1">
            See more
          </a>
        </div>
      </div>
      <div className="flex flex-col mt-4 ml-3">
        <div className="flex flex-row">
          <div>
            <img src={companySizeIcon} alt="" />
          </div>
          <div className="text-md15 text-gray-1 leading-4 font-semibold mt-1 ml-1">Size</div>
        </div>
        <div className="text-x-sm text-gray-1 leading-4 font-light ml-6 mt-1">{props.size}</div>
        <div className="text-x-sm text-blue-1 leading-4 font-semibold ml-6 mt-1">
          47 employees on Panelist
        </div>
      </div>
      <div className="flex flex-col mt-4 ml-3">
        <div className="flex flex-row">
          <div>
            <img src={websiteIcon} alt="" />
          </div>
          <div className="text-md15 text-gray-1 leading-4 font-semibold mt-1 ml-1">Website</div>
        </div>
        <div className="text-x-sm text-blue-1 leading-4 font-semibold ml-6 mt-1">
          {props.website}
        </div>
      </div>
    </div>
  );
};

export default CompanyProfileCard;
