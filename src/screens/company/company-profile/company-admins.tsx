import Select from 'react-select';

export const CompanyAdmins = () => (
  <div className="bg-white w-483 h-200 rounded-xl mt-4">
    <div className="flex flex-col px-4 py-3">
      <div className="flex flex-col">
        <div className="text-md15 text-gray-1 leading-4 font-semibold mt-1 ml-1">Admins*</div>
        <div>
          <Select options={['Josh Heid', 'Frazer Irvine', 'Slavi Svec']} isMulti />
        </div>
        <div className="text-sm font-light text-gray-1">
          Add a name or another admins email address if they are not a member on Panelist
        </div>
      </div>
      <div className="flex flex-row mt-1.5">
        <div className="bg-white border-2 rounded border-blue-0 w-6 h-6 flex flex-shrink-0 justify-center items-center mr-2 focus-within:border-blue-0">
          <input type="checkbox" className="opacity-0 absolute" />
          <svg
            className="fill-current hidden w-4 h-4 text-blue-3 pointer-events-none"
            viewBox="0 0 20 20"
          >
            <path d="M0 11l2-2 5 5L18 3l2 2L7 18z" />
          </svg>
        </div>
        <div className="text-sm text-blue-2 font-light leading-5 w-full ml-1">
          I verify that I am an authorised representative of this organisation and have the right to
          act on its behalf in the creation and management of this page.
        </div>
      </div>
    </div>
  </div>
);
