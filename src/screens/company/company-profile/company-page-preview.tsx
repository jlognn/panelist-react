import { useEffect } from 'react';

import { useDispatch, useSelector } from 'react-redux';

import companyEvents from '../../../assets/images/company-events.svg';
import companyPosts from '../../../assets/images/company-posts.svg';
import companyProfileCover from '../../../assets/images/company-profile-cover.svg';
import companyResources from '../../../assets/images/company-resources.svg';
import companySizeIcon from '../../../assets/images/company-size-icon.svg';
import descriptionIcon from '../../../assets/images/description-icon.svg';
import micrsoftLogo from '../../../assets/images/microsoft-logo-big.svg';
import microsoftLogoPreview from '../../../assets/images/microsoft-logo-preview.svg';
import specailityIcon from '../../../assets/images/specialties-icon.svg';
import verifiedIcon from '../../../assets/images/verified-icon.png';
import websiteIcon from '../../../assets/images/website-icon.svg';
import { IAppState } from '../../../store';
import { getCompanyProfilePreview } from '../../../store/company/profile-preview/actions';

export const CompanyPagePreview = (companyIdOrSlug: any) => {
  const companyId = String(Object.values(companyIdOrSlug)[0]);

  const dispatch = useDispatch();
  const { companyProfilePreview } = useSelector((state: IAppState) => state);

  useEffect(() => {
    dispatch(getCompanyProfilePreview(companyId));
  }, [dispatch, companyId]);

  return (
    <div className="bg-white w-570 rounded-xl mt-4 px-4 py-4">
      <div
        className="bg-white rounded-lg w-539"
        style={{
          boxShadow: '1px 1px 4px 0 rgba(0, 0, 0, 0.2)',
          backgroundImage: 'linear-gradient(to bottom, #6da43d -92%, #fff 68%)'
        }}
      >
        <div className="flex flex-col">
          <div className="mx-auto mt-4 z-10 mb-4">
            <img src={companyProfileCover} alt="" />
          </div>
          <div className="mx-auto z-20 -mt-12">
            <img src={micrsoftLogo} alt="" />
          </div>
          <div className="mx-auto flex flex-row">
            <div className="text-2x-sm text-black font-semibold leading-1">
              {companyProfilePreview.value?.name}
            </div>
            <div className="ml-0.5">
              <img src={verifiedIcon} alt="" />
            </div>
          </div>
          <div className="mx-auto mt-2 mb-2">
            <div className="text-2x-sm text-gray-1">{companyProfilePreview.value?.tagline}</div>
          </div>
          <div className="flex flex-row justify-around px-24 mt-1 mb-2">
            <div className="flex flex-col">
              <div>
                <img src={companyPosts} alt="" />
              </div>
              <div className="text-3x-sm text-gray-6">Posts</div>
            </div>
            <div className="flex flex-col">
              <div>
                <img src={companyResources} alt="" />
              </div>
              <div className="text-3x-sm text-gray-6">Resources</div>
            </div>
            <div className="flex flex-col">
              <div>
                <img src={companyEvents} alt="" />
              </div>
              <div className="text-3x-sm text-gray-6">Events</div>
            </div>
          </div>
        </div>
      </div>
      <div
        className="bg-white rounded-lg w-539 mt-4"
        style={{ boxShadow: '1px 1px 4px 0 rgba(0, 0, 0, 0.22)' }}
      >
        <div className="flex flex-col px-4 py-4 mb-2">
          <div className="flex flex-row">
            <div>
              <img src={microsoftLogoPreview} alt="" />
            </div>
            <div className="flex flex-col mt-2">
              <div className="text-lg text-black leading-5">
                {companyProfilePreview.value?.name}
              </div>
              <div className="text-sm text-gray-1 leading-5">
                {companyProfilePreview.value?.industry.name}
              </div>
              <div className="text-h3 text-gray-1 leading-5">
                {companyProfilePreview.value?.headquarter}
              </div>
            </div>
          </div>
          <div className="flex flex-col mt-4 ml-3">
            <div className="flex flex-row">
              <div>
                <img src={descriptionIcon} alt="" />
              </div>
              <div className="text-md15 text-gray-1 leading-4 font-semibold mt-1 ml-1">About</div>
            </div>
            <div className="text-x-sm text-gray-1 leading-4 font-light ml-6 mt-1">
              {companyProfilePreview.value?.description}{' '}
              <a href="/" className="text-blue-1 font-semibold">
                See more
              </a>
            </div>
          </div>
          <div className="flex flex-col mt-4 ml-3">
            <div className="flex flex-row">
              <div>
                <img src={specailityIcon} alt="" />
              </div>
              <div className="text-md15 text-gray-1 leading-4 font-semibold mt-1 ml-1">
                Specialities
              </div>
            </div>
            <div className="text-x-sm text-gray-1 leading-4 font-light ml-6 mt-1">
              <ul className="list-disc ml-5">
                {companyProfilePreview.value?.companySpecialities.map((speciality, index) => (
                  <li key={`speciality-${index}`}>{speciality.name}</li>
                ))}
              </ul>
              <a href="/" className="text-blue-1 font-semibold ml-1">
                See more
              </a>
            </div>
          </div>
          <div className="flex flex-col mt-4 ml-3">
            <div className="flex flex-row">
              <div>
                <img src={companySizeIcon} alt="" />
              </div>
              <div className="text-md15 text-gray-1 leading-4 font-semibold mt-1 ml-1">Size</div>
            </div>
            <div className="text-x-sm text-gray-1 leading-4 font-light ml-6 mt-1">
              {companyProfilePreview.value?.companySize.name}
            </div>
            <div className="text-x-sm text-blue-1 leading-4 font-semibold ml-6 mt-1">
              47 employees on Panelist
            </div>
          </div>
          <div className="flex flex-col mt-4 ml-3">
            <div className="flex flex-row">
              <div>
                <img src={websiteIcon} alt="" />
              </div>
              <div className="text-md15 text-gray-1 leading-4 font-semibold mt-1 ml-1">Website</div>
            </div>
            <div className="text-x-sm text-blue-1 leading-4 font-semibold ml-6 mt-1">
              {companyProfilePreview.value?.website}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
export default CompanyPagePreview;
