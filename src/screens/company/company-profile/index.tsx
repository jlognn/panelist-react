import { useParams } from 'react-router';

import { LinkButton } from '@panelist/panelist-components-beta';

import { UserNavbar } from '../../../components';
import CompanyDetails from './company-details';
import CompanyPagePreview from './company-page-preview';
import CompanyProfileCover from './company-profile-cover';

export const Company = () => {
  const params = useParams();

  return (
    <div className="bg-gray-1">
      <UserNavbar />
      <div className="container mx-auto">
        <div className="flex flex-row bg-gray-6 mt-4 ml-12 mb-16 px-4 py-2 shadow-md rounded-lg w-1124">
          <div className="flex flex-col">
            <div>
              <div className="text-left p-6 pl-6">
                <div className="text-gray-1 text-base font-semibold">Create a company page</div>
                <div className="text-gray-1 text-sm pt-2">
                  This is a view to your company profile page on Panelist, you can add yourself and
                  others in your company as an admin to this page.
                  <br />
                  You can also edit your company at a later time on Panelist.
                </div>
              </div>
              <div className="flex justify-end">
                <LinkButton
                  href="#"
                  backgroundColor="#203c6e"
                  textColor="#fff"
                  text="Save & Publish"
                  width="142px"
                  height="31px"
                  classes="pt-1.5 mt-3 mr-6"
                />
              </div>
            </div>
            <div className="flex mt-4 justify-between border-t-1 border-blue-0 px-1 py-1 ml-5 mr-5 w-1069">
              <div className="flex flex-col border-b-1 border-blue-0">
                <div className="flex flex-row text-gray-1 text-lg18 pb-1 my-1 mr-4 w-464">
                  Company Information
                </div>
              </div>
              <div className="flex flex-col border-b-1 border-blue-0 w-539">
                <div className="flex flex-row mr-60 text-gray-1 text-lg18 -ml-8 mt-1">Preview</div>
              </div>
            </div>
            <div className="flex flex-row">
              <div className="ml-6 mt-4">
                <CompanyProfileCover />
                <CompanyDetails companyIdOrSlug={Object.values(params)[0]} />
              </div>
              <div className="ml-4">
                <CompanyPagePreview companyIdOrSlug={Object.values(params)[0]} />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Company;
