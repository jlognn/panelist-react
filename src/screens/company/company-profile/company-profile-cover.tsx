import { useRef } from 'react';

import { useDispatch, useSelector } from 'react-redux';

import changeProfilePicture from '../../../assets/images/change-profile-picture.png';
import coverPhotoDefault from '../../../assets/images/cover-photo-default-md.png';
import { IAppState } from '../../../store';
import { setCoverPhoto } from '../../../store/company/cover-photo/actions';
import { setProfilePicture } from '../../../store/company/profile-picture/actions';
import { uploadImage } from '../../../store/fileupload/imageupload/actions';

export const CompanyProfileCover = () => {
  const coverPhotoRef = useRef<HTMLInputElement | null>(null);
  const profilePictureRef = useRef<HTMLInputElement | null>(null);

  const { imageUpload } = useSelector((state: IAppState) => state);

  const dispatch = useDispatch();

  const handleCoverPhotoSelected = (files: FileList) => {
    const data = new FormData();
    data.append(files[0].name, files[0]);
    dispatch(uploadImage(data));

    const uploadedFile = imageUpload.value?.data;
    dispatch(setCoverPhoto(uploadedFile?.key));
  };

  const handleProfilePictureSelected = (files: FileList) => {
    const data = new FormData();
    data.append(files[0].name, files[0]);
    dispatch(uploadImage(data));

    const uploadedFile = imageUpload.value?.data;
    dispatch(setProfilePicture(uploadedFile?.key));
  };

  return (
    <div
      className="bg-white rounded-lg w-483"
      style={{
        boxShadow: '1px 1px 4px 0 rgba(0, 0, 0, 0.1)'
      }}
    >
      <div className="flex flex-col">
        <div className="mx-auto mt-4 z-10">
          <img src={coverPhotoDefault} alt="" />
        </div>
        <div className="flex flex-row justify-end z-20">
          <div className="-mt-10 mr-6">
            <div
              className="bg-gray-1 text-blue-2 text-h3 rounded-2.5xl w-136 h-35 px-3 py-2 cursor-pointer"
              onClick={() => coverPhotoRef?.current?.click()}
            >
              Edit Cover Photo
            </div>
            <input
              type="file"
              id="coverPhoto"
              ref={coverPhotoRef}
              className="hidden"
              onChange={(e) => handleCoverPhotoSelected(e.target?.files!)}
            />
          </div>
        </div>
        <div className="flex flex-row">
          <div className="flex flex-row ml-4 mt-2">
            <div className="text-sm text-gray-1 font-light mr-8">Company Logo</div>
          </div>
        </div>
        <div className="flex flex-row justify-start z-20 mt-2">
          <div className="mb-2 mr-6">
            <div className="bg-gray-1 text-blue-2 text-h3 rounded-lg w-464 h-38 pl-10 py-2 ml-2">
              300px x 300px recommended
            </div>
          </div>
        </div>
        <div className="z-30 -mt-12 pl-1">
          <img
            src={changeProfilePicture}
            alt=""
            className="cursor-pointer"
            onClick={() => profilePictureRef?.current?.click()}
          />
          <input
            type="file"
            id="profilePicture"
            ref={profilePictureRef}
            className="hidden"
            onChange={(e) => handleProfilePictureSelected(e.target?.files!)}
          />
        </div>
      </div>
    </div>
  );
};

export default CompanyProfileCover;
