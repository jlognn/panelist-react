import { useEffect, useState } from 'react';

import { useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import Select from 'react-select';

import { Button, TextInput } from '@panelist/panelist-components-beta';

import companySizeIcon from '../../../assets/images/company-size-icon.svg';
import descriptionIcon from '../../../assets/images/description-icon.svg';
import headquartersIcon from '../../../assets/images/headquarters-icon.svg';
import industryIcon from '../../../assets/images/industry-icon.svg';
import panelistIcon from '../../../assets/images/panelist-icon.svg';
import specailityIcon from '../../../assets/images/specialties-icon.svg';
import websiteIcon from '../../../assets/images/website-icon.svg';
import { IEditCompanyProfile } from '../../../models/company/company-profile-edit';
import { IReactSelectOption } from '../../../models/react-select-option';
import { IAppState } from '../../../store';
import { getCompanyEmployees } from '../../../store/company/company-employees/actions';
import { getCompanySizes } from '../../../store/company/company-sizes/actions';
import { getCompanySpecialities } from '../../../store/company/company-specialities/actions';
import { getIndustries } from '../../../store/company/industries/actions';
import { getCompanyProfilePreview } from '../../../store/company/profile-preview/actions';
import { updateProfile } from '../../../store/company/profile-update/actions';

export const CompanyDetails = (companyIdOrSlug: any) => {
  const { register, setValue, handleSubmit } = useForm<IEditCompanyProfile>();

  const companyId = String(Object.values(companyIdOrSlug)[0]);
  const [specialities, setSpecialities] = useState<IReactSelectOption[]>();
  const [selectedSpecialities, setSelectedSpecialities] = useState<IReactSelectOption[]>([]);
  const [admins, setAdmins] = useState<IReactSelectOption[]>();
  const [selectedAdmins, setSelectedAdmins] = useState<IReactSelectOption[]>();

  const dispatch = useDispatch();
  const {
    companySpecialities,
    companySizes,
    industries,
    companyEmployees,
    companyProfilePreview,
    companyCoverPhoto,
    companyProfilePicture
  } = useSelector((state: IAppState) => state);

  const handleSpecialitiesSelected = (selectedOptions: any) => {
    selectedOptions.map(
      (option: IReactSelectOption) =>
        selectedSpecialities?.indexOf(option) === -1 &&
        setSelectedSpecialities([...selectedSpecialities, option])
    );
  };

  const handleAdminsSelected = (selectedOptions: any) => {
    selectedOptions.map(
      (option: IReactSelectOption) =>
        selectedAdmins?.indexOf(option) === -1 && setSelectedAdmins([...selectedAdmins, option])
    );
  };

  const onSubmit = (data: IEditCompanyProfile) => {
    const cover = companyCoverPhoto.value;
    const logo = companyProfilePicture.value;
    const specialitiesToSubmit: string[] = [];
    selectedSpecialities.map((speciality: IReactSelectOption) =>
      specialitiesToSubmit.push(speciality.value)
    );

    const dataToSubmit = { ...data, cover, logo, specialities: specialitiesToSubmit };

    dispatch(updateProfile(companyProfilePreview.value?.id, dataToSubmit));
  };

  useEffect(() => {
    const populateSpecialities = async () => {
      const specialitiesData: IReactSelectOption[] = [];
      dispatch(getCompanySpecialities());

      companySpecialities &&
        companySpecialities.value?.data.map((item, index) =>
          specialitiesData.push({
            key: `specialities-option-${index}`,
            value: item.name,
            label: item.name
          })
        );

      setSpecialities(specialitiesData);
    };
    populateSpecialities();
  }, [companySpecialities, dispatch]);

  useEffect(() => {
    const populateCompanySizes = async () => dispatch(getCompanySizes());
    populateCompanySizes();
  }, [dispatch]);

  useEffect(() => {
    const populateIndustries = async () => dispatch(getIndustries());
    populateIndustries();
  }, [dispatch]);

  useEffect(() => {
    const populateAdmins = async () => {
      const employeesData: IReactSelectOption[] = [];
      dispatch(getCompanyEmployees(companyId));

      companyEmployees.value?.data &&
        companyEmployees.value?.data.map((employee, index) =>
          employeesData.push({
            key: `employee-option-${index}`,
            value: employee.id,
            label: employee.fullName ?? `${employee.firstName} ${employee.lastName}`
          })
        );

      setAdmins(employeesData);
    };
    populateAdmins();
  }, [dispatch, companyEmployees, companyId]);

  useEffect(() => {
    const populateFormFields = async () => {
      dispatch(getCompanyProfilePreview(companyId));

      setValue('name', companyProfilePreview.value?.name!, { shouldDirty: true });
      setValue('tagline', companyProfilePreview.value?.tagline!, { shouldDirty: true });
      setValue('website', companyProfilePreview.value?.website!, { shouldDirty: true });
      setValue('description', companyProfilePreview.value?.description!, { shouldDirty: true });

      const existingSpecialities: IReactSelectOption[] = [];
      companyProfilePreview.value?.companySpecialities.map((item) =>
        existingSpecialities.push({
          key: item.name,
          label: item.name,
          value: item.name
        })
      );

      setSelectedSpecialities(existingSpecialities);

      setValue('website', companyProfilePreview.value?.website!, { shouldDirty: true });
      setValue('industryId', companyProfilePreview.value?.industryId!, { shouldDirty: true });
      setValue('companySizeId', companyProfilePreview.value?.companySizeId!, { shouldDirty: true });
      setValue('headquarter', companyProfilePreview.value?.headquarter!, { shouldDirty: true });
    };
    populateFormFields();
  }, [dispatch, setValue, companyProfilePreview, companyId]);

  return (
    <div>
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className="bg-white w-483 rounded-xl mt-4 py-2">
          <div className="flex flex-col px-4 py-3">
            <div className="flex flex-row">
              <div className="text-md15 text-gray-1 leading-4 font-semibold mt-2">
                Company name<span className="text-happening">*</span>
              </div>
              <div className="ml-2">
                <TextInput
                  textinputsize="small"
                  type="text"
                  style={{ border: '1px solid #e3e3e3' }}
                  {...register('name', {
                    required: { value: true, message: 'Company name is required' }
                  })}
                />
              </div>
            </div>
            <div className="flex flex-col mt-3">
              <div className="text-sm text-gray-1 leading-4 font-semibold">Tagline</div>
              <div className="mt-2">
                <TextInput
                  textinputsize="extraLarge"
                  type="text"
                  style={{ border: '1px solid #e3e3e3' }}
                  {...register('tagline')}
                />
              </div>
              <div className="text-right text-x-sm text-blue-2 font-light mt-1 mr-1.5">0/120</div>
            </div>
            <div className="flex flex-col mt-3">
              <div className="flex flex-row">
                <div>
                  <img src={panelistIcon} alt="" />
                </div>
                <div className="text-md15 text-gray-1 leading-4 font-semibold mt-1 ml-1">
                  Panelist URL<span className="text-happening">*</span>
                </div>
              </div>
              <div className="flex flex-row ml-6 mt-1">
                <div className="text-sm text-gray-1 font-light mt-2">panelist.com/company/</div>
                <div className="ml-1">
                  <TextInput
                    type="text"
                    textinputsize="small"
                    placeholder="Type here..."
                    style={{ width: '262px', border: '1px solid #e3e3e3' }}
                    {...register('website', {
                      required: { value: true, message: 'Company name is required' }
                    })}
                  />
                </div>
              </div>
              <div className="flex flex-col mt-4">
                <div className="flex flex-row">
                  <div>
                    <img src={descriptionIcon} alt="" />
                  </div>
                  <div className="text-md15 text-gray-1 leading-4 font-semibold mt-1 ml-1">
                    Company description
                  </div>
                  <div className="text-x-sm text-blue-2 font-light leading-2 ml-2 mt-1">
                    Tell people more about your organisation
                  </div>
                </div>
                <div className="mt-2 ml-6">
                  <textarea
                    className="text-sm text-gray-1 px-2 py-2"
                    style={{
                      width: '420px',
                      height: '187px',
                      border: '1px solid #e3e3e3',
                      borderRadius: '5px'
                    }}
                    {...register('description')}
                  ></textarea>
                </div>
                <div className="text-right text-x-sm text-blue-2 font-light mt-1 mr-1.5">
                  0/2000
                </div>
              </div>
            </div>
            <div className="flex flex-col mt-3">
              <div className="flex flex-row">
                <div>
                  <img src={specailityIcon} alt="" />
                </div>
                <div className="text-md15 text-gray-1 leading-4 font-semibold mt-1 ml-1">
                  Specialities
                </div>
              </div>
              <div className="ml-6">
                <Select
                  {...register('specialities')}
                  options={specialities}
                  isMulti
                  onChange={(e) => handleSpecialitiesSelected(e)}
                  value={selectedSpecialities}
                />
              </div>
              <div className="text-sm text-blue-2 font-light ml-6 mt-1">
                Type your keywords here and press enter to confirm
              </div>
            </div>
            <div className="flex flex-col mt-4">
              <div className="flex flex-row">
                <div>
                  <img src={websiteIcon} alt="" />
                </div>
                <div className="text-md15 text-gray-1 leading-4 font-semibold mt-1 ml-1">
                  Website
                </div>
              </div>
              <div className="mt-2 ml-6">
                <TextInput
                  textinputsize="large"
                  type="text"
                  placeholder={"Link to your company's external website"}
                  style={{ width: '420px', border: '1px solid #e3e3e3' }}
                  {...register('website')}
                />
              </div>
            </div>
            <div className="flex flex-col mt-4">
              <div className="flex flex-row">
                <div>
                  <img src={industryIcon} alt="" />
                </div>
                <div className="text-md15 text-gray-1 leading-4 font-semibold mt-1 ml-1">
                  Industry<span className="text-happening">*</span>
                </div>
              </div>
              <div className="mt-2 ml-6">
                <select
                  className="z-10 border border-gray-1 rounded-lg text-h2 text-gray-1 h-8 pl-3 pr-10 bg-white hover:border-gray-400 focus:outline-none appearance-none w-420"
                  {...register('industryId')}
                >
                  {industries &&
                    industries.value?.data.map((industry, index) => (
                      <option key={`size-${index}`} value={industry.id}>
                        {industry.name}
                      </option>
                    ))}
                </select>
              </div>
            </div>
            <div className="flex flex-col mt-4">
              <div className="flex flex-row">
                <div>
                  <img src={companySizeIcon} alt="" />
                </div>
                <div className="text-md15 text-gray-1 leading-4 font-semibold mt-1 ml-1">
                  Size<span className="text-happening">*</span>
                </div>
              </div>
              <div className="mt-2 ml-6">
                <select
                  className="border border-gray-1 rounded-lg text-h2 text-gray-1 h-8 pl-3 pr-10 bg-white hover:border-gray-400 focus:outline-none appearance-none"
                  style={{ width: '420px' }}
                  {...register('companySizeId')}
                >
                  {companySizes &&
                    companySizes.value?.data.map((size, index) => (
                      <option key={`size-${index}`} value={size.id}>
                        {size.name}
                      </option>
                    ))}
                </select>
              </div>
            </div>
            <div className="flex flex-col mt-4">
              <div className="flex flex-row">
                <div>
                  <img src={headquartersIcon} alt="" />
                </div>
                <div className="text-md15 text-gray-1 leading-4 font-semibold mt-1 ml-1">
                  Headquarters<span className="text-happening">*</span>
                </div>
              </div>
              <div className="mt-2 ml-6">
                <TextInput
                  textinputsize="large"
                  type="text"
                  style={{ width: '420px', border: '1px solid #e3e3e3' }}
                  {...register('headquarter', {
                    required: { value: true, message: 'Headquarter is required' }
                  })}
                />
              </div>
            </div>
          </div>
        </div>
        <div className="bg-white w-483 h-200 rounded-xl mt-4">
          <div className="flex flex-col px-4 py-3">
            <div className="text-md15 text-gray-1 leading-4 font-semibold mt-1 ml-1">
              Admins<span className="text-happening">*</span>
            </div>
            <div className="mt-2">
              <Select options={admins} isMulti onChange={handleAdminsSelected} />
            </div>
            <div className="flex flex-row mt-4">
              <div className="bg-white border-2 rounded border-blue-0 w-6 h-6 flex flex-shrink-0 justify-center items-center mr-2 focus-within:border-blue-0">
                <input type="checkbox" className="opacity-0 absolute" />
                <svg
                  className="fill-current hidden w-4 h-4 text-blue-3 pointer-events-none"
                  viewBox="0 0 20 20"
                >
                  <path d="M0 11l2-2 5 5L18 3l2 2L7 18z" />
                </svg>
              </div>
              <div className="text-sm text-blue-2 font-light leading-5 w-full ml-1">
                I verify that I am an authorised representative of this organisation and have the
                right to act on its behalf in the creation and management of this page.
              </div>
            </div>
          </div>
        </div>
        <div className="flex justify-end mt-2">
          <Button size="x-large" text="Save &amp; Publish" classes="bg-blue-1 text-white" />
        </div>
      </form>
    </div>
  );
};

export default CompanyDetails;
