import { LinkButton } from '@panelist/panelist-components-beta';

import attendindustryevents from '../../assets/images/homepage/attend-industry-events.svg';

export const AttendIndustryEvents = () => {
  const IndustryOptions = [
    {
      id: 1,
      name: 'Select Industry'
    },
    {
      id: 2,
      name: 'Marketing'
    },
    {
      id: 3,
      name: 'Engineering'
    }
  ];

  const FunctionOptions = [
    {
      id: 1,
      name: 'Select Function'
    },
    {
      id: 2,
      name: 'IT'
    },
    {
      id: 3,
      name: 'Business'
    }
  ];
  return (
    <div className="w-full bg-gray-1 xl:-mt-8">
      <div className="flex container mx-auto mt-32 mb-16 w-10/12">
        <div className="block md:flex justify-between">
          <div>
            <img src={attendindustryevents} alt="" className="lg:pl-3 md:max-w-xs xl:max-w-none" />
          </div>
          <div className="flex flex-col text-left 2xl:ml-28">
            <div className="flex flex-col md:ml-5">
              <p className="lg:text-xl3 font-extralight text-xl text-blue-4 leading-snug mt-10 md:mt-6">
                Attend industry events and <br /> grow your network
              </p>

              <p className="text-md font-extralight text-gray-1 mt-5 mr-40 text-left w-11/12">
                Attend industry events, collect competitive intelligence, find more about technology
                providers and hear industry experts share advice and insights into topics that you
                can benefit from.
              </p>
            </div>
            <div className="block lg:flex mt-7">
              <div className="flex md:flex-col md:ml-5">
                <span className="text-x-sm font-medium w-32 mb-1">Your industry</span>
                <div className="relative inline-flex md:w-6/12 lg:w-full ">
                  <svg
                    className="dropdown-icon"
                    xmlns="http://www.w3.org/2000/svg"
                    width="15"
                    height="7.5"
                    viewBox="0 0 15 7.5"
                  >
                    <path
                      id="Polygon_40"
                      data-name="Polygon 40"
                      d="M6.793.707a1,1,0,0,1,1.414,0l5.086,5.086A1,1,0,0,1,12.586,7.5H2.414a1,1,0,0,1-.707-1.707Z"
                      transform="translate(15 7.5) rotate(180)"
                      fill="#203c6e"
                    />
                  </svg>
                  <select className="dropdown-btn border border-gray-300 rounded-full text-gray-1 h-10 pl-5 pr-10 bg-white hover:border-gray-400 focus:outline-none appearance-none">
                    {IndustryOptions &&
                      IndustryOptions.map((item, index) => (
                        <option key={`industry-${index}`} value={item.id}>
                          {item.name}
                        </option>
                      ))}
                  </select>
                </div>
              </div>
              <div className="flex md:flex-col md:ml-8 mt-5 lg:ml-9 lg:mt-0">
                <span className="text-x-sm font-medium w-32 mb-1">Your job function</span>
                <div className="relative inline-flex md:w-6/12 lg:w-full  ">
                  <svg
                    className="dropdown-icon"
                    xmlns="http://www.w3.org/2000/svg"
                    width="15"
                    height="7.5"
                    viewBox="0 0 15 7.5"
                  >
                    <path
                      id="Polygon_40"
                      data-name="Polygon 40"
                      d="M6.793.707a1,1,0,0,1,1.414,0l5.086,5.086A1,1,0,0,1,12.586,7.5H2.414a1,1,0,0,1-.707-1.707Z"
                      transform="translate(15 7.5) rotate(180)"
                      fill="#203c6e"
                    />
                  </svg>
                  <select className="dropdown-btn border border-gray-300 rounded-full text-gray-1 h-10 pl-5 pr-10 bg-white hover:border-gray-400 focus:outline-none appearance-none">
                    {FunctionOptions &&
                      FunctionOptions.map((item, index) => (
                        <option key={`function-${index}`} value={item.id}>
                          {item.name}
                        </option>
                      ))}
                  </select>
                </div>
              </div>
            </div>

            <div className="mt-4 ml-3 2xl:ml-0 lg:ml-10 flex  justify-left lg:justify-end lg:w-3/4 xl:w-9/12 2xl:w-3/5 ">
              <div className="">
                <LinkButton
                  text="See results"
                  backgroundColor=""
                  textColor="#fff"
                  classes="bg-blue ml-5 text-sm2 font-extralight hover:bg-blue-gray-2"
                  width="108px"
                  height="30px"
                  href="/connect"
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AttendIndustryEvents;
