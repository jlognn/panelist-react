import { ButtonWithTextAndImage } from '@panelist/panelist-components-beta';
import createandhostevents from '../../assets/images/homepage/create-and-host-events.svg';
import Background from '../../assets/images/homepage/create-and-host-events2.svg';
import polyicon from '../../assets/images/homepage/polygon-icon.svg';

export const CreateAndHostEvents = () => (
  <div
    className="w-full min-h-1182"
    style={{
      background: `url(${Background})`,
      backgroundPosition: 'center',
      backgroundSize: 'cover',
      backgroundRepeat: 'repeat-x'
    }}
  >
    <div className="container mx-auto">
      <div className="block lg:flex flex-row mb-24">
        <div className=" -mt-9 w-full -ml-4">
          <img src={createandhostevents} alt="" />
        </div>
        <div className="md:mt-20 w-full -mt-20">
          <div className="block lg:flex flex-col mb-24 ml-20">
            <div className="text-left lg:text-xl3 font-extralight text-xl text-blue-4 mt-10 md:mt-6">
              Create and host your own events
            </div>
            <p className="text-gray-1 font-light text-md mt-4 text-left w-4/5  text-left w-3/5">
              Deliver a live or on-demand fully customisable events from multiple presenters and
              panelists with a full-scale online events platform. Facilitate your events with
              audience engagement tools, breakout rooms for peer networking and roundtable
              discussions.
            </p>

            <div className="mt-4 ml-0 w-1/5">
              <ButtonWithTextAndImage
                buttonClasses="bg-transparent hover:bg-gray-7 active:bg-gray-7 border-1 border-blue-4 w-242 h-43 rounded-full"
                imageUrl={polyicon}
                text="Create an event"
                textClasses="text-dark-blue font-extralight text-sm3 ml-1 pl-1"
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
);

export default CreateAndHostEvents;
