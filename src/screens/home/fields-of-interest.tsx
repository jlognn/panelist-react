import { LinkButton } from '@panelist/panelist-components-beta';

const fieldsOfInterest = [
  { text: 'Insurance', url: '/' },
  { text: 'Finance', url: '/' },
  { text: 'Healthcare', url: '/' },
  { text: 'Real Estate', url: '/' },
  { text: 'Field Services', url: '/' },
  { text: 'Data & Analytics', url: '/' },
  { text: 'IT & Cloud Infrastructure', url: '/' },
  { text: 'Banking', url: '/' },
  { text: 'Retail', url: '/' },
  { text: 'Oil & Energy', url: '/' },
  { text: 'Hospitality', url: '/' },
  { text: 'Entertainment', url: '/' },
  { text: 'Construction', url: '/' },
  { text: 'Computer Software', url: '/' }
];

export const FieldsOfInterest = () => {
  return (
    <div className="mt-24 w-full bg-white">
      <div className="flex container mx-auto mb-10">
        <div className="flex justify-between">
          <div className="flex flex-col items-start -mt-5 ml-16 pl-3 mr-20 w-3/4">
            <p className="text-xl3 text-blue-4 font-medium">Stay caught up in your field</p>
            <p className="text-xl3 text-blue-4 font-medium">of interest</p>
          </div>
          <div className="-mt-12 ml-22 pl-3 w-7/12 2xl:ml-24 2xl:w-full">
            <div className="flex flex-col items-start w-11/12 -ml-28 pl-6">
              <div className="flex flex-wrap">
                {fieldsOfInterest.map((field, i) => (
                  <div className="mb-2.5 mr-2.5" key={`interest-${i}`}>
                    <LinkButton
                      text={field.text}
                      backgroundColor="#cddcf2"
                      textColor="#0b1221"
                      classes="ml-5 field-btn"
                      width="100%"
                      height="30px"
                      href={field.url}
                    />
                  </div>
                ))}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default FieldsOfInterest;
