import { createRef } from 'react';

import { Card } from '@panelist/panelist-components-beta';

import avatarimage from '../../assets/images/card-avatar.png';
import cardimage from '../../assets/images/card-image.png';
import leftPrev from '../../assets/images/left-prev.svg';
import rightNext from '../../assets/images/right-next.svg';

interface CardProps {
  imageUrl: string;
  avatarUrl: string;
  title: string;
  subTitle: string;
  dateAndTime: string;
  eventType: string;
  detailsHref: string;
}

const eventsData: CardProps[] = [
  {
    imageUrl: cardimage,
    avatarUrl: avatarimage,
    title: 'Finance Transformation Summit',
    subTitle: 'Melbourne, AUS Apple Inc.',
    dateAndTime: '6 June 2021 12 PM GMT+10',
    eventType: 'Virtual Event',
    detailsHref: '#'
  },
  {
    imageUrl: cardimage,
    avatarUrl: avatarimage,
    title: 'Finance Transformation Summit',
    subTitle: 'Melbourne, AUS Apple Inc.',
    dateAndTime: '6 June 2021 12 PM GMT+10',
    eventType: 'Virtual Event',
    detailsHref: '#'
  },
  {
    imageUrl: cardimage,
    avatarUrl: avatarimage,
    title: 'Finance Transformation Summit',
    subTitle: 'Melbourne, AUS Apple Inc.',
    dateAndTime: '6 June 2021 12 PM GMT+10',
    eventType: 'Virtual Event',
    detailsHref: '#'
  },
  {
    imageUrl: cardimage,
    avatarUrl: avatarimage,
    title: 'Finance Transformation Summit',
    subTitle: 'Melbourne, AUS Apple Inc.',
    dateAndTime: '6 June 2021 12 PM GMT+10',
    eventType: 'Virtual Event',
    detailsHref: '#'
  },
  {
    imageUrl: cardimage,
    avatarUrl: avatarimage,
    title: 'Finance Transformation Summit',
    subTitle: 'Melbourne, AUS Apple Inc.',
    dateAndTime: '6 June 2021 12 PM GMT+10',
    eventType: 'Virtual Event',
    detailsHref: '#'
  },
  {
    imageUrl: cardimage,
    avatarUrl: avatarimage,
    title: 'Finance Transformation Summit',
    subTitle: 'Melbourne, AUS Apple Inc.',
    dateAndTime: '6 June 2021 12 PM GMT+10',
    eventType: 'Virtual Event',
    detailsHref: '#'
  },
  {
    imageUrl: cardimage,
    avatarUrl: avatarimage,
    title: 'Finance Transformation Summit',
    subTitle: 'Melbourne, AUS Apple Inc.',
    dateAndTime: '6 June 2021 12 PM GMT+10',
    eventType: 'Virtual Event',
    detailsHref: '#'
  }
];

export const UpcomingEvents = () => {
  const carouselRef = createRef<HTMLDivElement>();

  const scroll = (direction: number) => {
    const carouselDiv = carouselRef.current;

    if (carouselDiv) {
      const far = (+carouselDiv.offsetWidth / 2) * direction;
      const position = carouselDiv.scrollLeft + far;
      carouselDiv.scrollTo({ left: position, behavior: 'smooth' });
    }
  };

  return (
    <div className="w-full bg-gray-1">
      <div className="container mx-auto mt-8 mb-24">
        <div className="flex flex-col container mx-auto">
          <div className="text-xl mb-10 mt-5">Upcoming events</div>
          <div className="flex flex-row justify-center ">
            <div className="mt-32" onClick={() => scroll(-1)}>
              <img src={leftPrev} alt="" />
            </div>
            <div className="flex flex-row overflow-x-auto w-full" ref={carouselRef}>
              {eventsData &&
                eventsData.map((cardInfo, i) => (
                  <div className="w-64 ml-2" key={`card-${i}`}>
                    <Card
                      imageUrl={cardInfo.imageUrl}
                      avatarUrl={cardInfo.avatarUrl}
                      title={cardInfo.title}
                      subTitle={cardInfo.subTitle}
                      dateAndTime={cardInfo.dateAndTime}
                      eventType={cardInfo.eventType}
                      detailsHref={cardInfo.detailsHref}
                    />
                  </div>
                ))}
            </div>
            <div className="mt-32 ml-2" onClick={() => scroll(1)}>
              <img src={rightNext} alt="" />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default UpcomingEvents;
