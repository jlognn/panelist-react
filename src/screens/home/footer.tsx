import { Typography } from '@panelist/panelist-components-beta';

import logo from '../../assets/images/homepage/footer-logo.svg';

export const Footer = () => (
  <div className="bg-white">
    <div className="w-screen h-52 container mx-auto">
      <div className="flex flex-row 2xl:justify-center">
        <div className="mt-8 lg:ml-44 2xl:ml-0">
          <img src={logo} alt="" />
        </div>
        <div className="flex flex-col text-left xl:ml-20 mt-8 ">
          <div className="ml-16 text-md2">
            <Typography variant="FormFieldCaption" text="General" />
          </div>
          <div className="flex flex-col items-start ml-16 mt-3 text-sm font-light text-gray-1">
            <button className="text-sm font-light text-gray-1">Sign up</button>
            <button className="text-sm mt-1 font-light text-gray-1">About us </button>
            <button className="text-sm mt-1 font-light text-gray-1">Help Centre</button>
            <button className="text-sm mt-1 font-light text-gray-1">Branding</button>
            {/* <HyperLink variant='FooterLink' text='Sign up' href='#' />
                        <HyperLink variant='FooterLink' text='About us' href='#' />
                        <HyperLink variant='FooterLink' text='Help Centre' href='#' />
                        <HyperLink variant='FooterLink' text='Branding' href='#' />*/}
          </div>
        </div>
        <div className="flex flex-col text-left mt-8">
          <div className="ml-20 text-md2">
            <Typography variant="FormFieldCaption" text="Explore" />
          </div>
          <div className="flex flex-col items-start ml-20 mt-3">
            <button className="text-sm font-light text-gray-1">Library</button>
            <button className="text-sm mt-1 font-light text-gray-1">Events </button>
            <button className="text-sm mt-1 font-light text-gray-1">Network</button>

            {/* <HyperLink variant='FooterLink' text='Library' href='#' />
                        <HyperLink variant='FooterLink' text='Events' href='#' />
                        <HyperLink variant='FooterLink' text='Network' href='#' />*/}
          </div>
        </div>
        <div className="flex flex-col text-left mt-8">
          <div className="ml-20 text-md2">
            <Typography variant="FormFieldCaption" text="Business Solutions" />
          </div>
          <div className="flex flex-col items-start ml-20 mt-3">
            {/*  <HyperLink variant='FooterLink' text='Hybrid &amp; Virtual Events Platform' href='#' />
                        <HyperLink variant='FooterLink' text='Professional Community' href='#' />
                        <HyperLink variant='FooterLink' text='Product Demo Platforms' href='#' />*/}
            <button className="text-sm font-light text-gray-1">
              Hybrid &amp; Virtual Events Platform
            </button>
            <button className="text-sm mt-1 font-light text-gray-1">Professional Community</button>
            <button className="text-sm mt-1 font-light text-gray-1">Product Demo Platforms</button>
          </div>
        </div>
      </div>
    </div>
  </div>
);

export default Footer;
