import bottom from '../../assets/images/bottom.png';

export const BottomSection = () => (
  <div style={{ backgroundColor: '#edf2f5' }}>
    <div className="w-screen container mx-auto">
      <img src={bottom} alt="" />
    </div>
  </div>
);

export default BottomSection;
