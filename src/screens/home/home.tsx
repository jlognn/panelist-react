import { Navbar } from '../../components';
import AttendIndustryEvents from './attend-industry-events';
import ConnectAndInteract from './connect-and-interact';
import ConnectingProfessionals from './connecting-professionals';
import CreateAndHostEvents from './create-and-host-events';
import FieldsOfInterest from './fields-of-interest';
import { Footer } from './footer';
import UpcomingEvents from './upcoming-events';

export const Home = () => {
  return (
    <>
      <Navbar />
      <div className="mx-auto">
        <div className=" mx-auto">
          <div className="flex flex-col">
            <ConnectingProfessionals />
            <ConnectAndInteract />
            <AttendIndustryEvents />
            <FieldsOfInterest />
            <UpcomingEvents />
            <CreateAndHostEvents />
            <Footer />
          </div>
        </div>
      </div>
    </>
  );
};

export default Home;
