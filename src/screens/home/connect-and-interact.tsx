import connectandinteract from '../../assets/images/homepage/connectandinteract.svg';

export const ConnectAndInteract = () => (
  <div className="mt-24 w-full bg-white ">
    <div className="flex container mx-auto w-10/12">
      <div className="md:flex justify-between">
        <div className="flex flex-col items-start mt-53 ml-8 mr-12">
          <p className="max-w-lg text-xl3 font-extralight text-blue-4 text-left leading-tight md:leading-snug">
            Connect and interact with professionals in your industry
          </p>
          <p className="max-w-xl text-md font-extralight text-gray-1 mt-7 md:mt-5 text-left mb-5">
            Gain competitive advantage by connecting to <br />
            professionals in your industry, hear about their experience, <br />
            learn more about your field and share your insights.
          </p>
        </div>
        <div className="max-w-sm lg:max-w-none lg:-mt-12 2xl:ml-20 lg:-ml-18 md:mt-10  z-40 mx-auto">
          <img src={connectandinteract} alt="" />
        </div>
      </div>
    </div>
  </div>
);

export default ConnectAndInteract;
