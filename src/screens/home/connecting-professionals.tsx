import { LinkButton } from '@panelist/panelist-components-beta';
import connectingprofessionals from '../../assets/images/homepage/topbanner.svg';

export const ConnectingProfessionals = () => (
  <div className="bg-gray-1">
    <div className=" container">
      <div className="block md:flex">
        <div className="md:-mt-16 sm:mt-5 md:-ml-72 2xl:ml-0">
          <img className="" src={connectingprofessionals} alt="" />
        </div>
        <div className="md:ml-10 lg:ml-18 xl:ml-100 2xl:ml-10 block mt-9 sm:mt-20 md:mt-32 ml-11 ">
          <div className="text-xl2 block text-left ">
            <p className="text-blue-1 font-semibold">Connecting</p>
            <p className="text-blue-3 font-light ">professionals around</p>
            <p className="text-blue-1 font-semibold ">the world</p>
          </div>
          <div className="flex flex-col items-start mt-10 mt-0 md:mt-5 -ml-5">
            <div className="sm:ml-20 md:ml-0">
              <div className="mb-3">
                <LinkButton
                  text="Attend Events"
                  backgroundColor=""
                  textColor="#0b1221"
                  classes="bg-orange ml-5 text-sm2 font-extralight hover:bg-orange-2"
                  width="246px"
                  height="30px"
                  href="/attend-events"
                />
              </div>
              <div className="mb-3">
                <LinkButton
                  text="Host events"
                  backgroundColor=""
                  textColor="#fff"
                  classes="bg-blue-1 ml-5 text-sm2 font-extralight hover:bg-blue-4"
                  width="246px"
                  height="30px"
                  href="/host-events"
                />
              </div>
              <div className="mb-3">
                <LinkButton
                  text="Join social rooms"
                  backgroundColor=""
                  textColor="#fff"
                  classes="bg-blue-gray-1 ml-5 text-sm2 font-extralight hover:bg-gray-5"
                  width="246px"
                  height="30px"
                  href="/social-rooms"
                />
              </div>
              <div className="mb-3">
                <LinkButton
                  text="Connect"
                  backgroundColor=""
                  textColor="#0b1221"
                  classes="bg-white hover:bg-white-1 ml-5"
                  width="246px"
                  height="30px"
                  href="/connect"
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
);

export default ConnectingProfessionals;
