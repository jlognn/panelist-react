import { useEffect } from 'react';

import { routeTo } from '../../utils';

const Logout = () => {
  useEffect(() => {
    const logoutUser = async () => {
      localStorage.clear();
      sessionStorage.clear();
      routeTo('/login');
    };
    logoutUser();
  }, []);

  return <div className="container mx-auto">Logging out...</div>;
};

export default Logout;
