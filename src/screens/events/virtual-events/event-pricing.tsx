import bottom from '../../../assets/images/bottom.png';

export const EventPricing = () => (
  <div style={{ backgroundColor: '#edf2f5' }}>
    <div className="w-full container mx-auto">
      <img src={bottom} alt="" />
    </div>
  </div>
);

export default EventPricing;
