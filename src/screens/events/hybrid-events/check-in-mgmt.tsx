import checkinmgmtbg from '../../../assets/images/hybrid/check-mg.svg';
export const CheckInManagement = () => (
  <div>
    <div
      className="w-full -mt-24 "
      style={{
        background: `url(${checkinmgmtbg})`,
        backgroundPosition: 'center',
        backgroundSize: 'cover',
        backgroundRepeat: 'no-repeat',
        height: '487px'
      }}
    >
      <div className="2xl:container 2xl:mx-auto block md:flex w-11/12 justify-left ">
        <div className="2xl:pl-6 ml-32 w-4/12 pl-5">
          <div className="lg:text-xl3 font-extralight text-xl text-dark-blue mt-32 pt-2">
            Check-in Management
          </div>
          <div className="text-lg font-extralight text-dark-blue mt-4">
            Manage attendance with an automated <br />
            badge management system
          </div>

          <p className="text-md font-extralight text-gray-1 text-left mt-4">
            Manage your attendance and on-site check in with auto-generated customizable name tags
          </p>
        </div>
      </div>
    </div>
  </div>
);

export default CheckInManagement;
