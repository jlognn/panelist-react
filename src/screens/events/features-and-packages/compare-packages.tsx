import { LinkButton } from '@panelist/panelist-components-beta';
import compareplans from '../../../assets/images/feature/compareplan.png';
import compareplans2 from '../../../assets/images/feature/compareplan@2x.png';

export const ComparePackages = () => (
  <div className="container flex justify-center mx-auto">
    <div className="mt-4 flex flex-col items-center">
      <div className="text-lg text-gray-5 mt-5">Compare</div>
      <div className="text-md text-gray-4 font-light">Compare features by plan</div>
      <div className="mt-4 w-1124 2xl:w-full">
        <img
          className=""
          srcSet={`${compareplans} 300w, ${compareplans2} 1000w`}
          sizes="(max-width: 590px) 30vw, (max-width: 590px) 30vw, 508px"
          alt=""
          src={compareplans2}
        />
      </div>
      <div className="z-20 absolute ml-2" style={{ marginTop: '574px', marginLeft: '935px' }}>
        <LinkButton
          text="Contact Us"
          href="/contact-us"
          backgroundColor="#ffae39"
          textColor="#172746"
          width="146px"
          height="30px"
        />
      </div>
    </div>
  </div>
);

export default ComparePackages;
