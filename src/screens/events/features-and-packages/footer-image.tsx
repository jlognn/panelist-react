import footerimage from '../../../assets/images/feature/create-and-host-events2.svg';

export const FooterImage = () => (
  <div className="" style={{ marginTop: '' }}>
    <img className="w-full" src={footerimage} alt="" />
  </div>
);

export default FooterImage;
