import { useEffect, useRef } from 'react';

import { useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';

import { ImageButton, TextInput, Typography } from '@panelist/panelist-components-beta';

import arrowbutton from '../../assets/images/arrowbutton.png';
import { Navbar, PageHeader, PageTitle } from '../../components';
import { IAppState } from '../../store';
import { checkUserEmail, loadCheckUserEmail } from '../../store/user/loginorjoin/actions';
import { routeTo, emailRegex } from '../../utils';

interface ILoginOrSignup {
  email: string;
}

export const LoginOrJoin = () => {
  const {
    register,
    handleSubmit,
    formState: { errors }
  } = useForm<ILoginOrSignup>();

  const dispatch = useDispatch();
  const selector = useSelector((state: IAppState) => state.checkUserEmail);

  const onSubmit = (data: ILoginOrSignup) => {
    dispatch(loadCheckUserEmail(true));
    dispatch(checkUserEmail(data));
  };

  const initialRender = useRef(true);
  useEffect(() => {
    if (!initialRender.current) {
      const userExists = selector.value;

      if (userExists) routeTo('/login');
      else routeTo('/join');
    } else {
      initialRender.current = false;
    }
  }, [selector.value]);

  return (
    <>
      <Navbar />
      <div className="bg-gray-1 flex text-center h-screen">
        <div className="mx-auto">
          <div className="flex items-center justify-between h-16">
            <div className="flex w-full flex-wrap mt-72 mb-8 content-center">
              <div className="flex flex-col">
                <PageHeader />
                <PageTitle titleText={'Login or Join Now'} />
                <div className="flex justify-start ml-5 mt-5">
                  <form onSubmit={handleSubmit(onSubmit)}>
                    <div className="flex">
                      <div className="flex flex-col">
                        <TextInput
                          textinputsize="medium"
                          type="text"
                          caption="Email Address*"
                          placeholder="Enter your email"
                          {...register('email', {
                            required: { value: true, message: 'Email address is required' },
                            pattern: emailRegex
                          })}
                        />
                        <div>
                          {errors.email && (
                            <Typography
                              variant="ValidationMessage"
                              text={errors.email?.message!.toString()}
                              classes="flex content-start ml-2 mt-2"
                            />
                          )}
                        </div>
                      </div>
                      <div className="mt-5 -ml-14">
                        <ImageButton imageUrl={arrowbutton} />
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default LoginOrJoin;
