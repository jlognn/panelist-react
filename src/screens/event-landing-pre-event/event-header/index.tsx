import { IEventHeader } from '../../../models/upcoming-event/event-header';

export const EventHeader = (props: IEventHeader) => {
  return <div className="w-576 h-327 rounded-lg bg-white">{props.eventName}</div>;
};
