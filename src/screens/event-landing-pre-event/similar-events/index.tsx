export const SimilarEvents = () => {
  return (
    <div className="bg-white w-576 rounded-lg">
      <div className="flex flex-col px-2 py-2">
        <div className="text-h2 text-gray5a font-semibold">Similar Events</div>
        <div>{/* Map will appear here */}</div>
      </div>
    </div>
  );
};

export default SimilarEvents;
