import { IEventSponsor } from '../../../models/upcoming-event/event-sponsor';

export interface IEventPartnerProps {
  imageUrl: string;
  companyName: string;
}

const PartnerItem = (props: IEventPartnerProps) => (
  <div className="flex flex-col">
    <div className="px-2 py-2 mx-auto align-middle">
      <div>
        <img className="rounded-lg shadow-md p-2" src={props.imageUrl} alt="" />
      </div>
      <div className="text-md font-medium mt-2 ml-3">{props.companyName}</div>
    </div>
  </div>
);

export const EventPartners = (props: { eventPartners: IEventSponsor[] }) => {
  const defaultAvatarUrl = process.env.REACT_APP_DEFAULT_AVATAR_IMAGE_URL;
  const imageUrlPrefix = process.env.REACT_APP_IMAGE_URL_PREFIX_STAGING;

  return (
    <div className="grid gap-x-8 gap-y-4 grid-cols-4 px-2 py-2">
      {props.eventPartners &&
        props.eventPartners.length > 0 &&
        props.eventPartners.map((sponsor, index) => (
          <div key={`sponsor-${index}`}>
            <PartnerItem
              imageUrl={
                (sponsor.company.logo
                  ? `${imageUrlPrefix}/${sponsor.company.logo}`
                  : defaultAvatarUrl) ?? ''
              }
              companyName={sponsor.company.name}
            />
          </div>
        ))}
    </div>
  );
};

export default EventPartners;
