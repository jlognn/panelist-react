import { EVENT_ABOUT_CARD } from '../../../constants';
import { IAboutEventProps } from '../../../models/upcoming-event/event-about';

interface IEventItemProps {
  title: string;
  detail: string;
}

const EventItem = (props: IEventItemProps) => (
  <div className="flex flex-col">
    <div className="text-blue-1 text-h2">{props.title}</div>
    <div className="text-xs">{props.detail}</div>
  </div>
);

export const AboutEvent = (props: IAboutEventProps) => (
  <div className="text-blue-3 flex flex-col px-2 py-2">
    <div className="mb-1">
      <EventItem title={EVENT_ABOUT_CARD.EVENT_NAME} detail={props.eventName} />
    </div>
    <div className="mb-1">
      <EventItem title={EVENT_ABOUT_CARD.EVENT_THEME} detail={props.eventTheme} />
    </div>
    <div className="mb-1">
      <EventItem title={EVENT_ABOUT_CARD.EVENT_OVERVIEW} detail={props.overview} />
    </div>
    <div className="mb-1">
      <div className="flex flex-col">
        <div className="text-blue-1 text-h2">{EVENT_ABOUT_CARD.EVENT_KEY_DISCUSSION_POINTS}</div>
        <div className="text-xs">
          <ul className="list-outside list-disc">
            {props.keyDiscussionPoints &&
              props.keyDiscussionPoints.length > 0 &&
              props.keyDiscussionPoints.map((item, index) => (
                <li key={`discussion-point-${index}`}>{item.summary}</li>
              ))}
          </ul>
        </div>
      </div>
    </div>
    <div className="mb-1">
      <div className="flex flex row justify-between">
        <div className="flex flex-col">
          <div className="text-blue-1 text-h2">{EVENT_ABOUT_CARD.EVENT_ATTENDING_JOB_TITLES}</div>
          <div className="text-xs">
            <ul className="list-outside list-disc">
              {props.attendingJobTitles &&
                props.attendingJobTitles.length > 0 &&
                props.attendingJobTitles.map((item, index) => (
                  <li key={`job-title-${index}`}>{item}</li>
                ))}
            </ul>
          </div>
        </div>
        <div className="flex flex-col">
          <div className="text-blue-1 text-h2">{EVENT_ABOUT_CARD.EVENT_ATTENDING_INSUSTRIES}</div>
          <div className="text-xs">
            <ul className="list-outside list-disc">
              {props.attendingJobIndustries &&
                props.attendingJobIndustries.length > 0 &&
                props.attendingJobIndustries.map((item, index) => (
                  <li key={`industry-${index}`}>{item}</li>
                ))}
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
);

export default AboutEvent;
