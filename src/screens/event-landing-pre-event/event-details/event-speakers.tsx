import { useEffect } from 'react';

import { useDispatch, useSelector } from 'react-redux';

import { CardCompanyAdmins } from '@panelist/panelist-components-beta';

import { IAppState } from '../../../store';
import { getUpcomingEventSpeakers } from '../../../store/events/upcominn-event-speakers/actions';

interface IEventSpeakersProps {
  eventIdOrSlug: string;
}

export const EventSpeakers = (props: IEventSpeakersProps) => {
  const dispatch = useDispatch();
  const { upcomingEventSpeakers } = useSelector((state: IAppState) => state);

  const defaultAvatarUrl = process.env.REACT_APP_DEFAULT_AVATAR_IMAGE_URL;
  const imageUrlPrefix = process.env.REACT_APP_IMAGE_URL_PREFIX_STAGING;

  useEffect(() => {
    dispatch(getUpcomingEventSpeakers(props.eventIdOrSlug));
  }, [dispatch, props.eventIdOrSlug]);

  return (
    <>
      <div className="">
        {upcomingEventSpeakers.value &&
          upcomingEventSpeakers.value.data.length > 0 &&
          upcomingEventSpeakers.value.data.map((speaker, i) => (
            <div key={`speaker-${i}`} className="mr-4">
              <CardCompanyAdmins
                containerClass="h-57 mt-4"
                avatarUrl={
                  (speaker.avatar ? `${imageUrlPrefix}/${speaker.avatar}` : defaultAvatarUrl) + ''
                }
                title={`${speaker.firstName} ${speaker.lastName}`}
                subTitle={speaker.jobTitle}
                location={speaker.location}
                titleClass="blue-3 text-md"
                subTitleClass="gray-1 text-sm font-light"
                locationClass="text-s-10 gray-1 text-md mb-3 -mt-1"
                detailsHref="#"
                primaryButton="rounded-2xl h-8 w-24 px-5 py-1.5 bg-gray-2 text-blue-2  text-sm absolute  mt-4 right-28"
                primaryButtonText="Message"
                secondaryButton="rounded-2xl h-8 w-24 px-5 py-1.5 bg-blue text-gray-2  font-semibold text-sm absolute mt-4 right-0"
                secondaryButtonText="Connect"
                avatarClass={'w-12 h-12 rounded-full'}
              />
              <div className="border-blue border-b mt-3"></div>
            </div>
          ))}
      </div>
    </>
  );
};

export default EventSpeakers;
