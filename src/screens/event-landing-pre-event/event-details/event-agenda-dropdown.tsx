import { FC, useState } from 'react';

import triangleDown from '../../../assets/images/event-landing-pre-event/triangle-down.svg';
import trangleUp from '../../../assets/images/event-landing-pre-event/triangle-up.svg';

export interface ICardCompanyAdminsProps {
  eventTime?: string;
  title?: string;
  description?: string;
  speakerName?: string;
  speakerRole?: string;
  speakerCompany?: string;
  speakerAvatar?: string;
}

const EventAgendaDropdown: FC<ICardCompanyAdminsProps> = ({
  eventTime,
  title,
  description,
  speakerName,
  speakerRole,
  speakerCompany,
  speakerAvatar
}) => {
  const [menuOpen, setMenuOpen] = useState(false);
  const handleToggle = () => setMenuOpen(!menuOpen);

  return (
    <>
      <div className="shadow-md rounded-lg w-full h-ful bg-gray-200 px-4 py-2 relative">
        <button onClick={handleToggle} className="absolute top-1 right-1">
          {menuOpen && menuOpen ? (
            <img src={triangleDown} alt="open menu" />
          ) : (
            <img src={trangleUp} alt="close menu" />
          )}
        </button>
        <div className="text-left text-sm font-semibold">{eventTime}</div>
        <div className="text-left text-sm font-semibold">{title}</div>
        {!menuOpen && (
          <div className="flex flex-row">
            <div className="text-xs">{speakerName}</div>
            <span className={speakerName && speakerCompany ? '-mt-1 px-0.5' : 'hidden'}>●</span>
            <div className="text-xs">{speakerCompany}</div>
          </div>
        )}
        {menuOpen && (
          <>
            <div className="w-full h-full flex flex-col justify-center items-center">
              <p className="text-left text-xs font-light">{description}</p>
            </div>
            <div className={speakerName ? 'mt-12' : 'hidden'}>Speakers</div>
            <div className="px-5 pt-1">
              <div className="flex flex-row mt-4">
                <div>
                  <img className="w-12 h-12 rounded-full" src={speakerAvatar} alt="" />
                </div>
                <div className="flex flex-col ml-2 text-sm">
                  <div className="text-base text-blue-3">{speakerName}</div>
                  <div className="">{speakerRole}</div>
                  <div className="">{speakerCompany}</div>
                </div>
              </div>
            </div>
          </>
        )}
      </div>
    </>
  );
};

export default EventAgendaDropdown;
