import { useEffect } from 'react';

import { useDispatch, useSelector } from 'react-redux';

import { IAppState } from '../../../store';
import { getUpcomingEventAgenda } from '../../../store/events/upcoming-event-agenda/actions';
import EventAgendaDropdown from './event-agenda-dropdown';

interface IEventAgendaProps {
  eventIdOrSlug: string;
}

const EventAgenda = (props: IEventAgendaProps) => {
  const dispatch = useDispatch();
  const { upcomingEventAgenda } = useSelector((state: IAppState) => state);

  const defaultAvatarUrl = process.env.REACT_APP_DEFAULT_AVATAR_IMAGE_URL;
  const imageUrlPrefix = process.env.REACT_APP_IMAGE_URL_PREFIX_STAGING;

  useEffect(() => {
    dispatch(getUpcomingEventAgenda(props.eventIdOrSlug));
  }, [dispatch, props.eventIdOrSlug]);

  return (
    <div className="">
      {upcomingEventAgenda.value?.data &&
        upcomingEventAgenda.value?.data.length > 0 &&
        upcomingEventAgenda.value?.data.map((item) =>
          item.sessions.map((session, index) =>
            session.eventSessionSpeakers.map((speaker) => (
              <div key={`session-${index}`} className="my-2">
                <EventAgendaDropdown
                  eventTime={session.startTime}
                  title={session.title}
                  description={session.summary}
                  speakerName={`${speaker.eventSpeaker.firstName} ${speaker.eventSpeaker.lastName}`}
                  speakerRole={speaker.eventSpeaker.jobTitle}
                  speakerCompany={speaker.eventSpeaker.company?.name}
                  speakerAvatar={
                    speaker.eventSpeaker.user?.avatar
                      ? `${imageUrlPrefix}/${speaker.eventSpeaker.user?.avatar}`
                      : defaultAvatarUrl
                  }
                />
              </div>
            ))
          )
        )}
    </div>
  );
};

export default EventAgenda;
