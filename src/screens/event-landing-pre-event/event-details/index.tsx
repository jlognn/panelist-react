import { useEffect, useState } from 'react';

import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';

import { IAppState } from '../../../store';
import { getUpcomingEvent } from '../../../store/events/upcoming-event/actions';
import AboutEvent from './about-event';
import EventAgenda from './event-agenda';
import EventPartners from './event-partners';
import { EventSpeakers } from './event-speakers';

export const EventDetails = () => {
  const [openTab, setOpenTab] = useState(1);
  const tabNames = [
    [1, 'About'],
    [2, 'Speakers'],
    [3, 'Agenda'],
    [4, 'Event Partners']
  ];

  const params = useParams();
  const eventIdOrSlug: string = String(Object.values(params)[0]);

  const dispatch = useDispatch();
  const { upcomingEvent } = useSelector((state: IAppState) => state);

  useEffect(() => {
    dispatch(getUpcomingEvent(eventIdOrSlug));
  }, [dispatch, eventIdOrSlug]);

  return (
    <>
      <div className="flex flex-wrap">
        <div className="w-576">
          <ul className="flex list-none flex-row bg-white rounded-t-lg pt-4">
            {tabNames.map(([idx, name]) => {
              return (
                <li className="-mb-px mr-2 last:mr-0 flex-auto text-center cursor-pointer">
                  <button
                    className={openTab === idx ? 'underline' : ''}
                    onClick={(e) => {
                      e.preventDefault();
                      setOpenTab(idx as number);
                    }}
                  >
                    {name}
                  </button>
                </li>
              );
            })}
          </ul>
          <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-lg rounded-b-lg">
            <div className="px-4 py-5 overflow-auto">
              <div className="border-red border-b mt-2"></div>
              <div className="">
                <div className={openTab === 1 ? 'block' : 'hidden'}>
                  <AboutEvent
                    eventName={upcomingEvent.value?.data.event.name!}
                    eventTheme={upcomingEvent.value?.data.event.theme!}
                    overview={upcomingEvent.value?.data.event.overview!}
                    keyDiscussionPoints={[]}
                  />
                </div>
                <div className={openTab === 2 ? 'block' : 'hidden'}>
                  {/* Event Speakers*/}
                  <EventSpeakers eventIdOrSlug={eventIdOrSlug} />
                </div>
                <div className={openTab === 3 ? 'block' : 'hidden'}>
                  <EventAgenda eventIdOrSlug={eventIdOrSlug} />
                </div>
                <div className={openTab === 4 ? 'block' : 'hidden'}>
                  <EventPartners eventPartners={upcomingEvent.value?.data.event.eventSponsors!} />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default EventDetails;
