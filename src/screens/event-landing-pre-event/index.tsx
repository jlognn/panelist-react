import { UserNavbar } from '../../components';
import EventDetails from './event-details';
import { EventHeader } from './event-header';
import EventLocation from './event-location';
import SimilarEvents from './similar-events';

export const PreEventLandingPage = () => {
  return (
    <div className="bg-gray-1">
      <UserNavbar />
      <div className="container mx-auto mt-4">
        <div className="flex flex-row">
          <div className="flex flex-col ml-12">
            <div>
              <EventHeader
                id={''}
                eventName={'Chief Data Officer Live Series'}
                theme={''}
                companyName={''}
                industryName={''}
                startDateTime={''}
                endDateTime={''}
                format={''}
                type={''}
                location={''}
              />
            </div>
            <div className="mt-4">
              <EventDetails />
            </div>
            <div className="">
              <EventLocation
                latitude={''}
                longitude={''}
                title={''}
                description={''}
                address={''}
                notes={''}
              />
            </div>
            <div className="mt-4">
              <SimilarEvents />
            </div>
          </div>
          <div className="flex flex-col">
            {/* Put create post component over here */}
            <div className="w-527 h-295 bg-white rounded-lg ml-4">&nbsp;</div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default PreEventLandingPage;
