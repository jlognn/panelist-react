import { Navbar } from '../../components';
import { ButtonWithTextAndImage, HyperLink } from '@panelist/panelist-components-beta';
import logo from '../../assets/images/homepage/logo-panelist.svg';
import ScooterImage from '../../assets/images/homepage/scooter-image.svg';
import { ONE_TIME_LINK } from '../../constants';

export const OneTimeLink = () => (
  <>
    <Navbar />
    <div className="bg-gray-1  flex h-screen justify-center items-center">
      <div className="w-281 h-371">
        <div className="flex flex-col">
          <div className="">
            <img className="h-31" src={logo} alt="Panelist" />
          </div>
          <div className="flex justify-start ml-5">
            <div className="text-sm2 text-gray-1 font-semibold mt-3 -ml-5">
              {ONE_TIME_LINK.WE_JUST_EMAILED}
            </div>
          </div>
          <div className="text-sm2 text-gray-1 mt-2 ml-11">{ONE_TIME_LINK.CLICK_ON_THE_LINK}</div>
          <div className="text-sm2 text-gray-1 ml-20">{ONE_TIME_LINK.TO_YOUR_PANELIST_ACCOUNT}</div>
          <img className="w-83 ml-28 mt-4 mb-4" src={ScooterImage} alt="Panelist" />
        </div>
        <div className="ml-24">
          <ButtonWithTextAndImage
            imageUrl=""
            text="Back"
            textClasses="text-sm font-semibold text-blue-1 py-1.5"
            buttonClasses="bg-blue-0 login-button rounded-2xl inline-block h-8 w-20 mr-1"
          />
          <ButtonWithTextAndImage
            imageUrl=""
            text="Resend"
            textClasses="text-sm font-semibold text-blue-0 py-1.5"
            buttonClasses="bg-blue-1 login-button rounded-2xl inline-block h-8 w-24 ml-1"
          />
        </div>
        <div className="mt-10 ml-20">
          New member? &nbsp;{' '}
          <HyperLink variant="Signup" text="Signup" textColor="#285cb2" href="/join" />
        </div>
      </div>
    </div>
  </>
);
