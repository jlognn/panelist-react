import { useEffect, useRef } from 'react';
import 'react-toastify/dist/ReactToastify.css';

import { useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { toast } from 'react-toastify';

import { Button, HyperLink, TextInput, Typography } from '@panelist/panelist-components-beta';

import { Navbar, PageHeader, PageTitle } from '../../components';
import { IAppState } from '../../store';
import { loggingInUser, login } from '../../store/user/login/actions';
import { routeTo, emailRegex } from '../../utils';

interface ILogin {
  username: string;
  password: string;
}

interface ILoginProps {
  onSubmit: (data: ILogin) => void;
}

export function LoginPure({ onSubmit }: ILoginProps) {
  const {
    register,
    handleSubmit,
    formState: { errors }
  } = useForm<ILogin>();
  return (
    <>
      <Navbar />
      <div className="bg-gray-1 flex text-center h-screen login-screen">
        <div className="mx-auto">
          <div className="flex h-48">
            <div className="flex w-full flex-wrap mt-72 mb-8 content-center">
              <div className="flex flex-col">
                <PageHeader />
                <PageTitle titleText="Log in" />
                <form onSubmit={handleSubmit(onSubmit)}>
                  <div className="flex justify-start ml-5 mt-5">
                    <TextInput
                      textinputsize="medium"
                      type="text"
                      caption="Email Address*"
                      placeholder="Enter your email"
                      {...register('username', {
                        required: { value: true, message: 'Email address is required' },
                        pattern: {
                          value: emailRegex,
                          message: 'Email is invalid'
                        }
                      })}
                      id="emailAddress"
                    />
                  </div>
                  <div>
                    {errors.username && (
                      <Typography
                        variant="ValidationMessage"
                        text={errors.username?.message!.toString()}
                        classes="flex content-start ml-6 email-validation-message"
                      />
                    )}
                  </div>

                  <div className="flex justify-start ml-5 mt-5">
                    <TextInput
                      textinputsize="medium"
                      type="password"
                      caption="Password*"
                      {...register('password', {
                        required: { value: true, message: 'Password is required' }
                      })}
                      id="password"
                    />
                  </div>
                  <div>
                    {errors.password && (
                      <Typography
                        variant="ValidationMessage"
                        text={errors.password?.message!.toString()}
                        classes="flex content-start ml-6 password-validation-message"
                      />
                    )}
                  </div>

                  <div className="flex justify-start ml-5">
                    <HyperLink
                      variant="ForgotPassword"
                      text="Forgot your password?"
                      textColor="#285cb2"
                    />
                  </div>

                  <div className="flex justify-start mt-5 ml-28">
                    <Button
                      type="submit"
                      size="medium"
                      text="Login"
                      classes="bg-blue-1 text-white login-button"
                    />
                  </div>
                </form>
                <div>
                  <div className="flex justify-center mt-5 mr-12 font-sans">
                    New member? &nbsp;{' '}
                    <HyperLink variant="Signup" text="Signup" textColor="#285cb2" href="/join" />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export function Login() {
  const dispatch = useDispatch();
  const selector = useSelector((state: IAppState) => state.authenticateUser);

  const onSubmit = (data: ILogin) => {
    dispatch(loggingInUser(true));
    dispatch(login(data));
  };

  const initialRender = useRef(true);
  useEffect(() => {
    if (!initialRender.current) {
      const loggedIn = selector.isLoggedIn;

      if (loggedIn) {
        sessionStorage.setItem('isAuthenticated', loggedIn.toString());
        const auth = JSON.stringify(selector.value);
        sessionStorage.setItem('auth', auth);
        toast.success('Redirecting...');
        routeTo('/home');
      } else toast.error(selector.errorMessage);
    } else {
      initialRender.current = false;
    }
  }, [selector.isLoggedIn, selector.errorMessage, selector.value]);

  return <LoginPure onSubmit={onSubmit} />;
}
