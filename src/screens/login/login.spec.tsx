import React from 'react';
import { render, screen, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { LoginPure } from './login';

test('it renders the login screen', async () => {
  const onSubmit = jest.fn();

  render(<LoginPure onSubmit={onSubmit} />);

  expect(screen.getByRole('textbox', { name: 'Email Address*' })).toBeTruthy();
  expect(screen.getByLabelText('Password*')).toBeTruthy();
  expect(screen.getByRole('button', { name: 'Login' })).toBeTruthy();
  expect(onSubmit).not.toHaveBeenCalled();
});

test('it validates missing values', async () => {
  const onSubmit = jest.fn();
  render(<LoginPure onSubmit={onSubmit} />);
  const submit = screen.getByRole('button', { name: 'Login' });

  await waitFor(() => userEvent.click(submit));

  expect(await screen.findByText('Email address is required')).toBeTruthy();
  expect(await screen.findByText('Password is required')).toBeTruthy();
  expect(onSubmit).not.toHaveBeenCalled();
});

test('it validates invalid values', async () => {
  const onSubmit = jest.fn();
  render(<LoginPure onSubmit={onSubmit} />);
  const email = screen.getByRole('textbox', { name: 'Email Address*' });
  const submit = screen.getByRole('button', { name: 'Login' });

  await waitFor(() => {
    userEvent.type(email, 'notanemail');
    userEvent.click(submit);
  });

  expect(await screen.findByText('Email is invalid')).toBeTruthy();
  expect(onSubmit).not.toHaveBeenCalled();
});

test('it submits valid values', async () => {
  const onSubmit = jest.fn();
  render(<LoginPure onSubmit={onSubmit} />);
  const email = screen.getByRole('textbox', { name: 'Email Address*' });
  const password = screen.getByLabelText('Password*');
  const submit = screen.getByRole('button', { name: 'Login' });

  await waitFor(() => {
    userEvent.type(email, 'frazer.irving@team.panelist.com');
    userEvent.type(password, 'ImNotActuallyGoingToPutMyPasswordHereGuys!!!');
    userEvent.click(submit);
  });

  await expect(screen.findByText('Email is invalid')).rejects.toBeTruthy();
  await expect(screen.findByText('Email address is required')).rejects.toBeTruthy();
  await expect(screen.findByText('Password is required')).rejects.toBeTruthy();
  expect(onSubmit).toHaveBeenCalled();
});
