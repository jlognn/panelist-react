import { Connection } from '../../../models/connection';

export const ConnectionItem = (props: { item: Connection }) => (
  <div className="flex flex-col items-center">
    <div>
      <img
        src={`${process.env.REACT_APP_IMAGE_URL_PREFIX_STAGING}/${
          props.item.avatar ?? 'images/20211129/blank-profile-picture-g7b0695023640-udq004b.png'
        }`}
        alt=""
        className="rounded-full"
      />
    </div>
    <div
      className="text-h3 text-gray-1 font-light text-center mt-1"
      style={{ width: '46px', height: '31px' }}
    >
      {props.item.fullName}
    </div>
  </div>
);
