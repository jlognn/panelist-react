import { useEffect } from 'react';

import { useDispatch, useSelector } from 'react-redux';

import { CardEventsInterested, CardYourEvents } from '@panelist/panelist-components-beta';

import arrowDown from '../../../assets/images/arrow-down-icon.png';
import avatarDefault from '../../../assets/images/avatar-john-doe.png';
import coverPhotoDefault from '../../../assets/images/cover-photo-default.png';
import eventsAvatar from '../../../assets/images/events-avatar.png';
import eventBg from '../../../assets/images/events-background.png';
import groupIconSmall from '../../../assets/images/events-icon.png';
import microsoftLogoSmall from '../../../assets/images/microsoft-logo-small.png';
import { UserNavbar } from '../../../components';
import { IExperienceItem } from '../../../models/experience-item';
import { IAppState } from '../../../store';
import {
  getConnectSuggestions,
  loadConnectSuggestions
} from '../../../store/user/connect-suggestions/actions';
import { getOwnConnections, loadOwnConnections } from '../../../store/user/own-connections/actions';
import { getOwnExperience, loadOwnExperiences } from '../../../store/user/own-experience/actions';
import {
  getRecommendedEvents,
  loadRecommendedEvents
} from '../../../store/user/recommended-events/actions';
import { getUserEvents, loadUserEvents } from '../../../store/user/user-events/actions';
import { getUserProfile, loadUserProfile } from '../../../store/user/user-profile/actions';
import { calculatePeriod } from '../../../utils';
import AboutMe from './about-me';
import Connections from './connections';
import CurrentExperience from './current-experience';
import PastExperience from './past-experience';
import PeopleInYourNetwork from './people-in-your-network';
import ProfileCover from './profile-cover';

export const UserProfile = () => {
  const dispatch = useDispatch();

  const selector = useSelector((state: IAppState) => state);
  const profileSelector = selector.userProfile;
  const profile = profileSelector.value;

  const experienceSelector = selector.ownExperience;
  const experience = experienceSelector.value;
  const currentExperience = experience?.true[0];
  const pastExperience = experience?.false;

  const pastExperienceProps: IExperienceItem[] = [];

  pastExperience?.map((exp) => {
    return pastExperienceProps.push({
      logoUrl: exp.company.logo!,
      jobTitle: exp.jobTitle!,
      companyName: exp.company.name!,
      period: calculatePeriod(exp.startDate!, exp?.endDate!)!,
      country: exp.location!
    });
  });

  const connectionsSelector = selector.connections;
  const connections = connectionsSelector.value?.data;

  const userEventsSelector = selector.userEvents;
  const userEvents = userEventsSelector.value?.data;

  let attendees = '';
  userEvents?.map((event) =>
    event?.relatedAttendees?.map(
      (attendee) => (attendees += `${attendee.firstName} ${attendee.lastName}`)
    )
  );

  const connectSuggestionsSelector = selector.connectSuggestions;
  const connectSuggestions = connectSuggestionsSelector.value?.data;

  useEffect(() => {
    dispatch(loadUserProfile(true));
    dispatch(getUserProfile());

    dispatch(loadOwnExperiences(true));
    dispatch(getOwnExperience());

    dispatch(loadOwnConnections(true));
    dispatch(getOwnConnections());
    dispatch(loadRecommendedEvents(true));
    dispatch(getRecommendedEvents());

    dispatch(loadUserEvents(true));
    dispatch(getUserEvents());

    dispatch(loadConnectSuggestions(true));
    dispatch(getConnectSuggestions());
  }, [dispatch]);

  return (
    <div className="bg-gray-1">
      <UserNavbar />
      <div className="ml-20 h-screen">
        <div className="container mx-auto mt-4">
          <div className="flex flex-row">
            {/* Left column */}
            <div className="flex flex-col">
              {/* Profile banner */}
              <ProfileCover
                name={profile?.fullName!}
                coverPhotoUrl={coverPhotoDefault}
                avatarUrl={avatarDefault}
                jobTitle={profile?.jobTitle!}
                companyName={profile?.company.name!}
                companyLogoUrl={microsoftLogoSmall}
                location={profile?.location!}
              />
              {/* Experience & About me */}
              <div className="flex flex-row">
                {/* Current and past experience */}
                <div className="flex flex-col mr-4">
                  {/* Current experience */}
                  <CurrentExperience
                    logoUrl={currentExperience?.company.logo!}
                    jobTitle={currentExperience?.jobTitle!}
                    companyName={currentExperience?.company.name!}
                    period={
                      calculatePeriod(currentExperience?.startDate!, currentExperience?.endDate!)!
                    }
                    country={currentExperience?.location!}
                  />
                  {/* Past experience */}
                  <PastExperience items={pastExperienceProps!} />
                  {/* Connections */}
                  <Connections connections={connections!} title="Connections" />
                </div>
                {/* About me */}
                <AboutMe
                  location={profile?.location!}
                  industry={profile?.industry.name!}
                  offers={profile?.interestJobFunctions!}
                  interests={profile?.interestIndustries!}
                />
              </div>
            </div>
            {/* Right column */}
            <div className="flex flex-col">
              {/* Events you might be interested in */}
              <div className="bg-white rounded-lg w-315 px-2 py-1">
                <div className="text-h2 text-blue-3 px-2 py-1">
                  Events you might be intereseted in
                </div>
                <div className="bg-white px-2 py-2">
                  <CardEventsInterested
                    events={[
                      {
                        imageUrl: '',
                        groupDotsIconUrl: groupIconSmall,
                        groupIconSmallUrl: groupIconSmall,
                        arrowDownUrl: arrowDown,
                        heading: 'Events you might be interested in',
                        title: 'Pivoting From In-Person Events To Virtual Experiences - Melbourne',
                        subTitle: 'Melbourne, AUS Apple Inc.',
                        date: '6 Sep 2021',
                        time: '12 PM GMT+10',
                        eventConnections: 'Katarinasiala & 2 others',
                        detailsHref: '#',
                        border: '1px solid #203c6e',
                        backgroundUrl: eventBg,
                        avatarUrl: eventsAvatar,
                        url: '/virtual-event-two'
                      },
                      {
                        imageUrl: '',
                        groupDotsIconUrl: groupIconSmall,
                        groupIconSmallUrl: groupIconSmall,
                        arrowDownUrl: arrowDown,
                        heading: 'Events you might be interested in',
                        title: 'Pivoting From In-Person Events To Virtual Experiences - Melbourne',
                        subTitle: 'Melbourne, AUS Apple Inc.',
                        date: '6 Sep 2021',
                        time: '12 PM GMT+10',
                        eventConnections: 'Katarinasiala & 2 others',
                        detailsHref: '#',
                        border: '1px solid #203c6e',
                        backgroundUrl: eventBg,
                        avatarUrl: eventsAvatar,
                        url: '/virtual-event-two'
                      },
                      {
                        imageUrl: '',
                        groupDotsIconUrl: groupIconSmall,
                        groupIconSmallUrl: groupIconSmall,
                        arrowDownUrl: arrowDown,
                        heading: 'Events you might be interested in',
                        title: 'Pivoting From In-Person Events To Virtual Experiences - Melbourne',
                        subTitle: 'Melbourne, AUS Apple Inc.',
                        date: '6 Sep 2021',
                        time: '12 PM GMT+10',
                        eventConnections: 'Katarinasiala & 2 others',
                        detailsHref: '#',
                        border: '1px solid #203c6e',
                        backgroundUrl: eventBg,
                        avatarUrl: eventsAvatar,
                        url: '/virtual-event-two'
                      }
                    ]}
                  />
                </div>
              </div>
              {/* Your events */}
              <div className="bg-white rounded-lg w-315 mt-4 px-2 py-2">
                <div className="text-h2 text-blue-3 px-2 py-1">{`${profile?.firstName}'s events`}</div>
                <div className="bg-white mt-1">
                  {userEvents &&
                    userEvents.map((event, index) => (
                      <div key={`your-event-${index}`} className="mb-2">
                        <CardYourEvents
                          avatarUrl={event.logo!}
                          dotIconUrl={''}
                          groupIconSmallUrl={event.logo!}
                          title={event.name!}
                          subTitle={''}
                          date={event.startTime!}
                          time={event.startTime!}
                          contactsGoing={attendees}
                          eventType={event.type!}
                          detailsHref={`/event-details/${event.slug!}`}
                        />
                      </div>
                    ))}
                </div>
              </div>
              {/* People in your network */}
              <PeopleInYourNetwork people={connectSuggestions!} />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default UserProfile;
