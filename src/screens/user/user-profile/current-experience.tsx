import plusIcon from '../../../assets/images/plus-icon.png';
import { IExperienceItem } from '../../../models/experience-item';
import ExperienceItem from './experience-item';

export const CurrentExperience = (experience: IExperienceItem) => (
  <div className="bg-white rounded-lg w-315 h-143 mt-4">
    <div className="flex flex-col mt-4">
      <div className="flex flex-row justify-between">
        <div className="text-md15 text-blue-3 ml-4">Current experience</div>
        <div className="mr-4 mt-1">
          <img src={plusIcon} alt="" />
        </div>
      </div>
      <div className="mt-4">
        <ExperienceItem
          logoUrl={experience.logoUrl}
          jobTitle={experience.jobTitle}
          companyName={experience.companyName}
          period={experience.period}
          country={experience.country}
        />
      </div>
    </div>
  </div>
);

export default CurrentExperience;
