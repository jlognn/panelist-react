import connectIcon from '../../../assets/images/connect-icon.png';
import { IPeopleInYourNetworkProp } from '../../../models/people-in-your-network';

export const PeopleInYourNetworkItem = (props: IPeopleInYourNetworkProp) => (
  <div className="flex flex-row justify-between">
    <div className="">
      <img src={props.avatarImage} alt="" />
    </div>
    <div className="flex flex-col">
      <div className="text-h2 text-blue-3 font-semibold">{props.name}</div>
      <div className="text-h2 text-blue-3">
        {props.jobTitle} at {props.companyName}
      </div>
    </div>
    <div className="">
      <img src={connectIcon} alt="" />
    </div>
  </div>
);

export default PeopleInYourNetworkItem;
