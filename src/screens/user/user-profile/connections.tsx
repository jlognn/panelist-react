import { Connection } from '../../../models/connection';
import { ConnectionItem } from './connection-item';

export const Connections = (props: { connections: Connection[]; title: string }) => (
  <div className="bg-white rounded-lg w-315">
    <div className="flex flex-col">
      <div
        className="text-md15 text-blue-3 w-full"
        style={{ boxShadow: '0 3px 6px 0 rgba(0, 0, 0, 0.1)' }}
      >
        <div className="px-4 py-3">
          <div className="flex flex-row justify-between">
            <div className="">{props.title}</div>
            <div className="font-semibold">{props.connections && props.connections.length}</div>
          </div>
        </div>
      </div>
      <div className="grid grid-cols-3 gap-4 px-5 py-4">
        {props.connections &&
          props.connections.map((connection, index) => (
            <div key={`connection-${index}`}>
              <ConnectionItem item={connection} />
            </div>
          ))}
      </div>
      {props.connections && (
        <div className="rounded-bl-lg rounded-br-lg border-t-1 border-blue-0 text-center mb-1">
          <div className="text-sm text-blue-1 font-semibold mt-2">See all</div>
        </div>
      )}
    </div>
  </div>
);

export default Connections;
