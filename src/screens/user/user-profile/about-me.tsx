import editIcon from '../../../assets/images/edit-icon.png';
import industryIcon from '../../../assets/images/industry-icon.png';
import interestsIcon from '../../../assets/images/interests-icon.svg';
import locationIcon from '../../../assets/images/location-icon.png';
import offersIcon from '../../../assets/images/offers-icon.svg';
import { IAboutMeProps } from '../../../models/about-me';
import AttributeTag from './attribute-tag';

export const AboutMe = (props: IAboutMeProps) => (
  <div className="bg-white rounded-lg w-463 mt-4 mr-4">
    <div className="flex flex-col px-1 py-3">
      <div className="flex flex-row justify-between">
        <div className="text-md15 text-blue-3 ml-4">About Me</div>
        <div className="mr-4 mt-2">
          <img src={editIcon} alt="Edit profile" />
        </div>
      </div>
      <div className="flex flex-row ml-4 mt-1">
        <div className="mr-2">
          <img src={locationIcon} alt="" />
        </div>
        <div className="text-md15 text-gray-1 font-semibold mr-2">Location</div>
        <div className="text-md15 text-gray-1 font-light">{props.location}</div>
      </div>
      <div className="flex flex-row ml-4 mt-2">
        <div className="mr-2">
          <img src={industryIcon} alt="" />
        </div>
        <div className="text-md15 text-gray-1 font-semibold mr-2">Industry</div>
        <div className="">
          <AttributeTag value={props.industry} showCheckmark={false} />
        </div>
      </div>
      <div className="flex flex-row justify-between border-t-1 border-b-1 border-blue-0 px-2 py-2 mt-2">
        <div className="ml-4">
          <img src={offersIcon} alt="" />
        </div>
        <div className="mr-4">
          <img src={editIcon} alt="" />
        </div>
      </div>
      <div className="flex flex-row flex-wrap px-2 py-2">
        {props.offers?.map((offer, i) => (
          <div key={`attribute-offers-${i}`} className="px-1 py-1">
            <AttributeTag value={offer.name} showCheckmark={true} />
          </div>
        ))}
      </div>
      <div className="flex flex-row justify-between border-t-1 border-b-1 border-blue-0 px-2 py-2 mt-2">
        <div className="ml-4">
          <img src={interestsIcon} alt="" />
        </div>
        <div className="mr-4">
          <img src={editIcon} alt="" />
        </div>
      </div>
      <div className="flex flex-row flex-wrap px-2 py-2">
        {props.interests?.map((interest, i) => (
          <div key={`attribute-interests-${i}`} className="px-1 py-1">
            <AttributeTag value={interest.name} showCheckmark={true} />
          </div>
        ))}
      </div>
    </div>
  </div>
);

export default AboutMe;
