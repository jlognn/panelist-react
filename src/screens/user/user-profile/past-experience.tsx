import dividerSmall from '../../../assets/images/divider-small.png';
import { IExperienceItem } from '../../../models/experience-item';
import { ExperienceItem } from './experience-item';

export const PastExperience = (props: { items: IExperienceItem[] }) => (
  <div className="bg-white rounded-lg w-315 mt-4">
    <div className="flex flex-col">
      <div
        className="text-md15 text-blue-3 w-full"
        style={{ boxShadow: '0 3px 6px 0 rgba(0, 0, 0, 0.1)' }}
      >
        <div className="px-4 py-3">Past experience</div>
      </div>
      <div className="mt-4">
        {props.items.map((item: IExperienceItem, index) => (
          <div key={`experience-${index}`} className="mb-4">
            <ExperienceItem
              logoUrl={item.logoUrl}
              jobTitle={item.jobTitle}
              companyName={item.companyName}
              period={item.period}
              country={item.country}
            />
            {props.items.length > 1 && index < props.items.length - 1 && (
              <div className="ml-4 mt-2">
                <img src={dividerSmall} alt="" />
              </div>
            )}
          </div>
        ))}
      </div>
    </div>
  </div>
);

export default PastExperience;
