import avatarDefault from '../../../assets/images/avatar-john-doe.png';
import changeProfilePicture from '../../../assets/images/change-profile-picture.png';
import coverPhotoDefault from '../../../assets/images/cover-photo-default.png';
import divider from '../../../assets/images/divider.png';
import editIcon from '../../../assets/images/edit-icon.png';
import microsoftLogoSmall from '../../../assets/images/microsoft-logo-small.png';
import { IProfileCoverProps } from '../../../models/profile-cover';

export const ProfileCover = (props: IProfileCoverProps) => (
  <div
    className="bg-white rounded-lg w-793 h-327"
    style={{
      boxShadow: '1px 1px 4px 0 rgba(0, 0, 0, 0.1)',
      backgroundImage: 'linear-gradient(to bottom, #bcbfc7 -92%, #fff 44%)'
    }}
  >
    <div className="flex flex-col">
      <div className="mx-auto mt-4 z-10">
        <img src={props.coverPhotoUrl ?? coverPhotoDefault} alt="" />
      </div>
      <div className="flex flex-row justify-between z-20">
        <div className="ml-6 -mt-32">
          <img src={props.avatarUrl ?? avatarDefault} alt="" />
        </div>
        <div className="-mt-10 mr-6">
          <div className="bg-gray-1 text-blue-2 text-h3 rounded-lg w-136 h-35 px-2 py-2">
            Edit Cover Photo
          </div>
        </div>
      </div>
      <div className="flex flex-row justify-between">
        <div className="z-30 -mt-12 ml-32">
          <img src={changeProfilePicture} alt="" />
        </div>
        <div className="flex flex-row">
          <div className="-mt-4">
            <img
              src={props.companyLogoUrl ?? microsoftLogoSmall}
              alt=""
              style={{ width: '40px', height: '40px' }}
            />
          </div>
          <div className="-mt-2 mr-4 text-md15 font-semibold">{props.companyName}</div>
        </div>
      </div>
      <div className="flex flex-col ml-4">
        <div className="text-xl font-semibold">{props.name}</div>
        <div className="text-md15 text-gray-1">{props.jobTitle}</div>
        <div className="text-md15 text-gray-1 font-light">{props.location}</div>
      </div>
      <div className="flex mt-1 ml-4">
        <img src={divider} alt="" />
      </div>
      <div className="flex flex-row justify-between">
        <div className="flex flex-row ml-4 mt-2">
          <div className="text-md15 text-blue-2 font-light mr-8">About</div>
          <div className="text-md15 text-blue-2 font-light mr-8">Posts</div>
          <div className="text-md15 text-blue-2 font-light">Content</div>
        </div>
        <div className="mr-4 mt-2">
          <img src={editIcon} alt="Edit profile" />
        </div>
      </div>
    </div>
  </div>
);

export default ProfileCover;
