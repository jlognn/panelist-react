import avatarDaniella from '../../../assets/images/daniella-avatar.png';
import avatarDaniella2 from '../../../assets/images/daniella-avatar2.png';
import eventsIcon from '../../../assets/images/events-icon.png';
import avatarJohn from '../../../assets/images/john-avatar.png';
import microsoftLogo from '../../../assets/images/microsoft-logo-experience.png';
import avatarMo from '../../../assets/images/mo-avatar.png';
import yourEventsCardAvatar from '../../../assets/images/your-events-card-avatar.png';

export const experienceItems = [
  {
    logoUrl: microsoftLogo,
    jobTitle: 'Marketing Advisor',
    companyName: 'Microsoft',
    period: 'May 2019 - June 2020 ・ 1 year',
    country: 'Singapore'
  },
  {
    logoUrl: microsoftLogo,
    jobTitle: 'Marketing Advisor',
    companyName: 'Microsoft',
    period: 'May 2019 - June 2020 ・ 1 year',
    country: 'Singapore'
  },
  {
    logoUrl: microsoftLogo,
    jobTitle: 'Marketing Advisor',
    companyName: 'Microsoft',
    period: 'May 2019 - June 2020 ・ 1 year',
    country: 'Singapore'
  }
];

export const connections = [
  {
    avatarUrl: avatarDaniella,
    name: 'Daniella Abril'
  },
  {
    avatarUrl: avatarMo,
    name: 'Mo Hamdouna'
  },
  {
    avatarUrl: avatarJohn,
    name: 'John Doe'
  },
  {
    avatarUrl: avatarJohn,
    name: 'John Doe'
  },
  {
    avatarUrl: avatarDaniella,
    name: 'Daniella Abril'
  },
  {
    avatarUrl: avatarMo,
    name: 'Mo Hamdouna'
  }
];

export const offers = [
  {
    id: '1',
    name: 'Software development'
  },
  {
    id: '2',
    name: 'Working with first home buyers'
  },
  {
    id: '3',
    name: 'Fast-moving consumer goods (FMCG)'
  },
  {
    id: '4',
    name: 'Corporate Management Software'
  },
  {
    id: '5',
    name: 'Mentorship'
  }
];

export const events = [
  {
    avatarUrl: yourEventsCardAvatar,
    dotIconUrl: '',
    groupIconSmallUrl: eventsIcon,
    title: 'Chief Data Officer ANZ-E Live 2021 - Volume II',
    date: '6 Sep 2021',
    time: '12 PM GMT+10',
    contactsGoing: 'Katarinasiala & 2 others',
    eventType: 'Partnering',
    detailsHref: '#'
  },
  {
    avatarUrl: yourEventsCardAvatar,
    dotIconUrl: '',
    groupIconSmallUrl: eventsIcon,
    title: 'Chief Data Officer ANZ-E Live 2021 - Volume II',
    date: '6 Sep 2021',
    time: '12 PM GMT+10',
    contactsGoing: 'Katarinasiala & 2 others',
    eventType: 'Partnering',
    detailsHref: '#'
  },
  {
    avatarUrl: yourEventsCardAvatar,
    dotIconUrl: '',
    groupIconSmallUrl: eventsIcon,
    title: 'Chief Data Officer ANZ-E Live 2021 - Volume II',
    date: '6 Sep 2021',
    time: '12 PM GMT+10',
    contactsGoing: 'Katarinasiala & 2 others',
    eventType: 'Partnering',
    detailsHref: '#'
  }
];

export const people = [
  {
    name: 'Daniella Abril',
    jobTitle: 'Project Manager',
    companyName: 'IBM',
    avatarImage: avatarDaniella2
  },
  {
    name: 'Daniella Abril',
    jobTitle: 'Project Manager',
    companyName: 'IBM',
    avatarImage: avatarDaniella2
  },
  {
    name: 'Daniella Abril',
    jobTitle: 'Project Manager',
    companyName: 'IBM',
    avatarImage: avatarDaniella2
  },
  {
    name: 'Daniella Abril',
    jobTitle: 'Project Manager',
    companyName: 'IBM',
    avatarImage: avatarDaniella2
  },
  {
    name: 'Daniella Abril',
    jobTitle: 'Project Manager',
    companyName: 'IBM',
    avatarImage: avatarDaniella2
  }
];
