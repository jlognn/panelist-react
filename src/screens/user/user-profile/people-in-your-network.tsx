import { ConnectSuggestion } from '../../../models/connect-suggestions';
import PeopleInYourNetworkItem from './people-in-your-network-item';

export const PeopleInYourNetwork = (props: { people: ConnectSuggestion[] }) => (
  <div className="bg-white rounded-lg w-315 px-2 py-2 mt-4">
    <div className="text-h2 text-blue-3 px-2 py-2">People in your network</div>
    <div className="bg-white mt-1 mb-2">
      {props.people &&
        props.people.map((person, index) => (
          <div key={`persion-${index}`} className="mt-2">
            <PeopleInYourNetworkItem
              name={`${person.firstName} ${person.lastName}`}
              jobTitle={person.jobTitle}
              companyName={person.company.name}
              avatarImage={person.avatar}
            />
          </div>
        ))}
    </div>
    <div className="rounded-bl-lg rounded-br-lg border-t-1 border-blue-0 text-center mb-1">
      <div className="text-sm text-blue-1 font-semibold mt-2">See all</div>
    </div>
  </div>
);

export default PeopleInYourNetwork;
