import editIcon from '../../../assets/images/edit-icon.png';
import noCompanyLogo from '../../../assets/images/no-company.svg';
import { IExperienceItem } from '../../../models/experience-item';

export const ExperienceItem = (item: IExperienceItem) => (
  <div className="flex flex-row justify-between">
    <div className="flex flex-row">
      <div className="ml-4 -mt-1 mr-2">
        <img src={item.logoUrl ?? noCompanyLogo} alt="" />
      </div>
      <div className="flex flex-col">
        <div className="text-h3 text-blue-3 font-semibold">{item.jobTitle}</div>
        <div className="text-h3 text-blue-3">{item.companyName}</div>
        <div className="text-h3 text-gray-1 font-light">{item.period}</div>
        <div className="text-h3 text-gray-1 font-light">{item.country}</div>
      </div>
    </div>
    <div className="mr-4">
      <img src={editIcon} alt="Edit experience" />
    </div>
  </div>
);

export default ExperienceItem;
