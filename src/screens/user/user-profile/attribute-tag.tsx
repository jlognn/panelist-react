import checkmarkIcon from '../../../assets/images/checkmark-icon.svg';

interface IAttributeTagProps {
  value: string;
  showCheckmark: boolean;
}
export const AttributeTag = (props: IAttributeTagProps) => (
  <div className="text-x-sm text-blue-2 font-light bg-blue-0 rounded-xl px-2 py-1.5 -mt-0.5 flex flex-row">
    <div>{props.value} &nbsp;</div>
    {props.showCheckmark && <img src={checkmarkIcon} alt="" />}
  </div>
);

export default AttributeTag;
