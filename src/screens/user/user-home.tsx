import {
  AdvertisingCard,
  CardContainer,
  CardEventsInterested,
  CardMainPost,
  CardProfile,
  CreatePostCard,
  NavMenuCard
} from '@panelist/panelist-components-beta';

import advertising from '../../assets/images/advertising.png';
import arrowDown from '../../assets/images/arrow-down-icon.png';
import avatarOne from '../../assets/images/avatar-one.png';
import avatarSmall from '../../assets/images/avatar-small.png';
import avatarImage from '../../assets/images/cardProfile-avatar.png';
import backgroundImage from '../../assets/images/cardProfile-background.png';
import commentIcon from '../../assets/images/comment-icon.png';
import contentIcon from '../../assets/images/content-icon.png';
import documentIcon from '../../assets/images/document-icon.png';
import emojiIcon from '../../assets/images/emoji-smile-small-icon.png';
import eventsAvatar from '../../assets/images/events-avatar.png';
import eventBg from '../../assets/images/events-background.png';
import groupIconSmall from '../../assets/images/events-icon.png';
import groupIcon from '../../assets/images/group-icon.png';
import imageVideoIcon from '../../assets/images/image-video-icon.png';
import likeIcon from '../../assets/images/like-icon.png';
import pollIcon from '../../assets/images/poll-icon.png';
import imageIcon from '../../assets/images/post-small-icon.png';
import profileAvatar from '../../assets/images/profile-avatar.png';
import profileBg from '../../assets/images/profile-background.png';
import rectangleIcon from '../../assets/images/rectangle-icon.png';
import shareIcon from '../../assets/images/share-icon.png';
import { UserNavbar } from '../../components';

export const UserHome = () => {
  const userProfile = {
    imageUrl: backgroundImage,
    avatarUrl: avatarImage,
    name: 'John Barleycorn',
    occupation: 'IT Manager at Microsoft',
    location: 'Melbourne, Australia'
  };

  const cardItems = [
    { text: 'Pending invitations', url: '/pending-invitations' },
    { text: 'My upcoming events', url: '/upcoming-events' },
    { text: 'Past events', url: '/past-events' },
    { text: "Events I'm hosting", url: '/my-hosted-events' }
  ];

  const events = [
    {
      imageUrl: rectangleIcon,
      avatarUrl: avatarOne,
      groupIcons: groupIcon,
      likeIcon,
      commentIcon,
      shareIcon,
      title: 'Phillip Stubbs',
      subTitle: 'Project Manager at IBM',
      dateAndTime: 'Sep 23, 2020 at 02:35 pm',
      placeHolder:
        'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected…see more',
      commentsTally: '244 24 comments',
      like: 'Like',
      comment: 'Comment',
      share: 'Share',
      eventType: 'Virtual Event',
      detailsHref: '#',
      comments: []
    },
    {
      imageUrl: rectangleIcon,
      avatarUrl: avatarOne,
      groupIcons: groupIcon,
      likeIcon,
      commentIcon,
      shareIcon,
      title: 'Phillip Stubbs',
      subTitle: 'Project Manager at IBM',
      dateAndTime: 'Sep 23, 2020 at 02:35 pm',
      placeHolder:
        'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected…see more',
      commentsTally: '244 24 comments',
      like: 'Like',
      comment: 'Comment',
      share: 'Share',
      eventType: 'Virtual Event',
      detailsHref: '#',
      comments: [
        {
          avatarUrl: avatarImage,
          name: 'Phillip Stubbs',
          jobTitle: 'Project Manager',
          companyName: 'IBM',
          timeAgo: '6d',
          commentText:
            'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour.',
          groupIcons: groupIcon,
          reactionsCount: 14
        }
      ]
    },
    {
      imageUrl: rectangleIcon,
      avatarUrl: avatarOne,
      groupIcons: groupIcon,
      likeIcon,
      commentIcon,
      shareIcon,
      title: 'Phillip Stubbs',
      subTitle: 'Project Manager at IBM',
      dateAndTime: 'Sep 23, 2020 at 02:35 pm',
      placeHolder:
        'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected…see more',
      commentsTally: '244 24 comments',
      like: 'Like',
      comment: 'Comment',
      share: 'Share',
      eventType: 'Virtual Event',
      detailsHref: '#',
      comments: []
    }
  ];

  const peopleYouManyKnow = [
    {
      imageUrl: profileBg,
      avatarUrl: profileAvatar,
      name: 'John Barleycorn',
      occupation: 'IT Manager at Microsoft',
      location: 'Melbourne, Australia',
      detailsHref: ''
    },
    {
      imageUrl: profileBg,
      avatarUrl: profileAvatar,
      name: 'John Barleycorn',
      occupation: 'IT Manager at Microsoft',
      location: 'Melbourne, Australia',
      detailsHref: ''
    },
    {
      imageUrl: profileBg,
      avatarUrl: profileAvatar,
      name: 'John Barleycorn',
      occupation: 'IT Manager at Microsoft',
      location: 'Melbourne, Australia',
      detailsHref: ''
    }
  ];

  return (
    <div className="bg-gray-1 h-full">
      <UserNavbar />
      <div>
        <div className="container mx-auto mt-4">
          <div className="flex flex-row">
            <div className="flex flex-col">
              <div className="bg-white w-250 ml-12">
                <CardProfile
                  imageUrl={userProfile.imageUrl}
                  avatarUrl={userProfile.avatarUrl}
                  name={userProfile.name}
                  occupation={userProfile.occupation}
                  location={userProfile.location}
                />
              </div>
              <div className="mt-2 ml-12">
                <NavMenuCard cardItems={cardItems} />
              </div>
            </div>
            <div className="ml-4 mb-8">
              <div className="flex flex-col">
                <div>
                  <CreatePostCard
                    avatarUrl={avatarSmall}
                    pollIconUrl={pollIcon}
                    contentIconUrl={contentIcon}
                    documentIconUrl={documentIcon}
                    imageVideoIconUrl={imageVideoIcon}
                    imageVideo={'image/video'}
                    document={'Document'}
                    content={'Content'}
                    poll={'Poll'}
                    detailsHref={'#'}
                    textInput={''}
                    backgroundColor="#edf2f5"
                    placeHolder="Create a post"
                    borderRadius="20px"
                  />
                </div>
                <div className="mt-4 bg-white rounded-lg">
                  <CardContainer title="People you may know" cards={peopleYouManyKnow} />
                </div>
                <div className="mt-4">
                  {events &&
                    events.map((event, index) => (
                      <div key={`event-${index}`} className="mb-4 bg-white rounded-lg">
                        <CardMainPost
                          imageUrl={event.imageUrl}
                          avatarUrl={event.avatarUrl}
                          groupIcons={event.groupIcons}
                          likeIcon={event.likeIcon}
                          commentIcon={event.commentIcon}
                          shareIcon={event.shareIcon}
                          title={event.title}
                          subTitle={event.subTitle}
                          dateAndTime={event.dateAndTime}
                          eventType={event.eventType}
                          placeHolder={event.placeHolder}
                          commentsTally={event.commentsTally}
                          detailsHref={event.detailsHref}
                          comments={event.comments}
                          emojiIcon={emojiIcon}
                          imageIcon={imageIcon}
                        />
                      </div>
                    ))}
                </div>
              </div>
            </div>
            <div className="flex flex-col ml-4">
              <div className="bg-white rounded-lg w-315 px-2 py-1">
                <div className="text-h2 text-blue-3 px-2 py-1">
                  Events you might be intereseted in
                </div>
                <div className="bg-white px-2 py-2">
                  <CardEventsInterested
                    events={[
                      {
                        imageUrl: '',
                        groupDotsIconUrl: groupIconSmall,
                        groupIconSmallUrl: groupIconSmall,
                        arrowDownUrl: arrowDown,
                        heading: 'Events you might be interested in',
                        title: 'Pivoting From In-Person Events To Virtual Experiences - Melbourne',
                        subTitle: 'Melbourne, AUS Apple Inc.',
                        date: '6 Sep 2021',
                        time: '12 PM GMT+10',
                        eventConnections: 'Katarinasiala & 2 others',
                        detailsHref: '#',
                        border: '1px solid #203c6e',
                        backgroundUrl: eventBg,
                        avatarUrl: eventsAvatar,
                        url: '/virtual-event-two'
                      },
                      {
                        imageUrl: '',
                        groupDotsIconUrl: groupIconSmall,
                        groupIconSmallUrl: groupIconSmall,
                        arrowDownUrl: arrowDown,
                        heading: 'Events you might be interested in',
                        title: 'Pivoting From In-Person Events To Virtual Experiences - Sydney',
                        subTitle: 'Melbourne, AUS Apple Inc.',
                        date: '6 Sep 2021',
                        time: '12 PM GMT+10',
                        eventConnections: 'Katarinasiala & 2 others',
                        detailsHref: '#',
                        border: '1px solid #203c6e',
                        backgroundUrl: eventBg,
                        avatarUrl: eventsAvatar,
                        url: '/virtual-event-two'
                      },
                      {
                        imageUrl: '',
                        groupDotsIconUrl: groupIconSmall,
                        groupIconSmallUrl: groupIconSmall,
                        arrowDownUrl: arrowDown,
                        heading: 'Events you might be interested in',
                        title: 'Pivoting From In-Person Events To Virtual Experiences - Adelaide',
                        subTitle: 'Melbourne, AUS Apple Inc.',
                        date: '6 Sep 2021',
                        time: '12 PM GMT+10',
                        eventConnections: 'Katarinasiala & 2 others',
                        detailsHref: '#',
                        border: '1px solid #203c6e',
                        backgroundUrl: eventBg,
                        avatarUrl: eventsAvatar,
                        url: '/virtual-event-two'
                      }
                    ]}
                  />
                </div>
              </div>
              <div className="mt-4">
                <AdvertisingCard imageUrl={advertising} />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default UserHome;
