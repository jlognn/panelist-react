import React from 'react';

import { Button, TextInput, Typography } from '@panelist/panelist-components-beta';

import { Navbar, PageHeader, PageTitle } from '../../components';

export const Signup = () => (
  <>
    <Navbar />
    <div className="bg-gray-1 flex text-center h-screen">
      <div className="mx-auto">
        <div className="flex items-center justify-between h-48">
          <div className="flex w-full flex-wrap mt-96 mb-8 content-center">
            <div className="flex flex-col ml-20">
              <PageHeader />
              <PageTitle titleText="Join now" />
              <div>
                <div className="flex justify-start ml-5 mt-5">
                  <TextInput
                    textinputsize="medium"
                    type="text"
                    caption="Email Address*"
                    placeholder="Enter your email"
                  />
                </div>
              </div>
              <div>
                <div className="flex justify-start ml-5 mt-5">
                  <TextInput textinputsize="medium" type="password" caption="Password*" />
                </div>
              </div>
              <div>
                <div className="flex justify-start ml-5 mt-5">
                  <TextInput textinputsize="medium" type="password" caption="Confirm password*" />
                </div>
              </div>
              <div>
                <div className="flex justify-start mt-5 ml-28">
                  <Button
                    size="large"
                    text="Agree &amp; Join"
                    backgroundColor="#203c6e"
                    textColor="#fff"
                  />
                </div>
              </div>
              <div>
                <div className="flex justify-center mt-5 mr-12">
                  <Typography
                    variant="TextWithLinks"
                    text="By clicking Agree & Join, you agree to Panelist {0},"
                    links={[{ text: 'User Agreement', url: '/user-agreement' }]}
                  />
                  <br />
                </div>
                <div className="flex justify-center mt-1 mr-12">
                  <Typography
                    variant="TextWithLinks"
                    text={'{0} and {1}'}
                    links={[
                      { text: 'Privacy Policy', url: '/privacy-policy' },
                      { text: 'Cookie Policy', url: '/cookie-policy' }
                    ]}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </>
);

export default Signup;
