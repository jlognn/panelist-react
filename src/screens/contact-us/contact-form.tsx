import { LinkButton, TextInput } from '@panelist/panelist-components-beta';

export const ContactForm = () => (
  <div className="w-full">
    <div className=" mx-auto bg-blue-0 py-8">
      <div className="xl:w-10/12 xl:flex xl:flex-row mx-auto">
        <div className="block 2xl:ml-48 xl:ml-2 pl-6  xl:mt-52 pt-1 justify-center mt-5 xl:mb-0 mb-10">
          <div className="text-xl font-extralight text-blue-3">
            Contact our sales team today <br /> to build the right event for you
          </div>
          <div className="text-md font-extralight text-blue-3 mt-4">Using Panelist?</div>
          <div className="text-md2 text-blue-6 navlink font-semibold mt-2">Login</div>
          <div className="text-md2 text-blue-6 navlink font-semibold mt-2">Contact Support</div>
        </div>
        <div className="lg:ml-8">
          <div className=" w-11/12 mx-auto lg:w-10/12 lg:ml-6 xl:w-535 xl:flex flex-col bg-white rounded-xl px-6 pt-7 pb-5 -mt-2 xl:ml-60 w-5/12">
            <div className="xl:flex ">
              <div className="text-xl7 font-extralight text-blue-3 w-full xl:w-1/2 xl:mb-0 mb-10 pt-1">
                Contact Us
              </div>
              <div className="xl:justify-end xl:ml-2 pl-2 -mt-1 mx-auto">
                <LinkButton
                  text="Auto-fill with your Panelist account"
                  backgroundColor=""
                  textColor="#fff"
                  classes="bg-blue-1 ml-5 text-sm2 font-extralight hover:bg-blue-4"
                  width="283px"
                  height="30px"
                  href="/auto-fill"
                />
              </div>
            </div>
            <div className="text-md2 font-extralight text-blue-3 mt-4">
              Let us know how we can help you
            </div>
            <div className="flex flex-row mt-2">
              <div className="mr-2 w-full">
                <TextInput
                  textinputsize="small"
                  caption="First name*"
                  type="text"
                  classes="w-full"
                />
              </div>
              <div className="w-full">
                <TextInput
                  textinputsize="small"
                  caption="Last name*"
                  type="text"
                  classes="w-full"
                />
              </div>
            </div>
            <div className="mt-2">
              <TextInput
                textinputsize="medium"
                caption="Organization*"
                type="text"
                classes="w-full"
              />
            </div>
            <div className="mt-2">
              <TextInput textinputsize="medium" caption="Your Role*" type="text" classes="w-full" />
            </div>
            <div className="mt-2">
              <TextInput
                textinputsize="medium"
                caption="City, Country*"
                type="text"
                classes="w-full"
              />
            </div>
            <div className="mt-2">
              <TextInput
                textinputsize="medium"
                caption="Email Address*"
                type="text"
                classes="w-full"
              />
            </div>
            <div className="mt-2">
              <TextInput
                textinputsize="medium"
                caption="Phone Number*"
                type="text"
                classes="w-full"
              />
            </div>
            <div className="mt-3">
              <div className="text-sm5 font-extralight text-justify">
                By clicking <span className="font-semibold">submit</span> you agree that we use the
                information you provide to contact you with relevant information to address your
                request. If you are a member, you can control what communication you receive in your
                settings. If you are a guest, you can unsubscribe from Panelist emails at anytime by
                clicking the unsubscribe link in the email. For more information visit our
                {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
                <a className="text-link font-semibold ml-1" href="#">
                  Privacy Policy
                </a>
                .
              </div>
            </div>
            <div className="text-right mt-2 mb-3 pb-1">
              <LinkButton
                text="Submit"
                backgroundColor=""
                textColor="#fff"
                classes="bg-blue-1 ml-5 text-sm2 font-extralight hover:bg-blue-4"
                width="98px"
                height="30px"
                href="/auto-fill"
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
);

export default ContactForm;
