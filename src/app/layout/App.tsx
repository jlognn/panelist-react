import './App.css';

import { Route } from 'react-router-dom';

import { Footer } from '../../components';
import { Home, Login, LoginOrJoin, Signup } from '../../screens';
import CompanyViewAsVisitor from '../../screens/company/company-as-visitor';
import CompanyContent from '../../screens/company/company-content';
import { Company } from '../../screens/company/company-profile';
import { ContactUs } from '../../screens/contact-us';
import { EventLive } from '../../screens/event-live';
import PreEventLandingPage from '../../screens/event-landing-pre-event';
import { HybridEvents, Overview, VirtualEvents } from '../../screens/events';
import FeaturesAndPackages from '../../screens/events/features-and-packages';
import Logout from '../../screens/logout/logout';
import { OneTimeLink } from '../../screens/one-time-link/one-time-link';
import { UserHome, UserProfile } from '../../screens/user';

function App() {
  return (
    <div className="app">
      <div>
        <Route exact path="/" component={Home} />
        <Route exact path="/loginorjoin" component={LoginOrJoin} />
        <Route exact path="/login" component={Login} />
        <Route exact path="/one-time-link" component={OneTimeLink} />
        <Route exact path="/join" component={Signup} />
        <Route exact path="/overview" component={Overview} />
        <Route exact path="/hybrid-events" component={HybridEvents} />
        <Route exact path="/virtual-events" component={VirtualEvents} />
        <Route exact path="/features-and-packages" component={FeaturesAndPackages} />
        <Route exact path="/home" component={UserHome} />
        <Route exact path="/contact-us" component={ContactUs} />
        <Route exact path="/event-live" component={EventLive} />
        <Route exact path="/logout" component={Logout} />
        <Route exact path="/profile" component={UserProfile} />
        <Route exact path="/company-profile/:companyidorslug" component={Company} />
        <Route
          exact
          path="/event-landing-pre-event/:eventidorslug"
          component={PreEventLandingPage}
        />
        <Route
          exact
          path="/company-profile/view-as-visitor/:companyidorslug"
          component={CompanyViewAsVisitor}
        />
        <Route exact path="/company-content/:companyidorslug" component={CompanyContent} />
      </div>
      <Footer />
    </div>
  );
}

export default App;
