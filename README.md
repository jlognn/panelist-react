# Panelist App

This project is created using React, Redux and Typescript.

## Running the application

Before running the application please create a `.env` file in the root directory of this project and add following variable to it
```
REACT_APP_API_URL=<base url of the API>
```

Install the project dependancies by running
```
npm install
```

In the project directory, you can run:

```
npm start
```

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

## Executing tests

To run the tests you can run following command

```
npm test
```

To view the test coverage report run following command

```
npm run coverage
```

Launches the test runner in the interactive watch mode.\

## Deploying the application

`npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!
